﻿using System;
using System.Collections.Generic;
using System.Text;
using XinYiOffice.Common;
using System.Data;

namespace XinYiOffice.Basic
{
    public static class LocalTenantsServer
    {
        static LocalTenantsServer() { }

        /// <summary>
        /// 根据域名，获取本地租户
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Model.LocalTenants GetLocalTenantsByDname(string name)
        {
            Model.LocalTenants lt = new Model.LocalTenants();

            List<Model.LocalTenants> ltList = new BLL.LocalTenants().GetModelList(string.Format("Name='{0}'", name));
            if (ltList != null && ltList.Count > 0)
            {
                lt = ltList[0];
            }
            return lt;
        }

        /// <summary>
        /// 根据租户id，获取本地租户
        /// </summary>
        /// <param name="tid"></param>
        /// <returns></returns>
        public static Model.LocalTenants GetLocalTenantsByTenantId(int tid)
        {
            Model.LocalTenants lt = null;
            try
            {
                lt=new BLL.LocalTenants().GetModel(tid);
            }
            catch(Exception ex)
            {
                Common.EasyLog.WriteLog(ex);
            }

            return lt;
        }

        /// <summary>
        /// 获取本地租户信息,根据guid
        /// </summary>
        /// <param name="TenantGuid"></param>
        /// <returns></returns>
        public static Model.LocalTenants GetLocalTenantsByGuid(string TenantGuid)
        {
            Model.LocalTenants lt = new Model.LocalTenants();

            List<Model.LocalTenants> ltList = new BLL.LocalTenants().GetModelList(string.Format("TenantGuid='{0}'", TenantGuid));
            if (ltList != null && ltList.Count > 0)
            {
                lt = ltList[0];
            }
            return lt;
        }

        /// <summary>
        /// 判断是否存在此租户信息
        /// </summary>
        /// <param name="TenantGuid"></param>
        /// <param name="Name"></param>
        /// <returns></returns>
        public static bool IsTenantByGuidName(string TenantGuid,string Name)
        {
            string mysql = string.Format("select top 1 Id from LocalTenants where TenantGuid='{0}' and Name='{1}' ", TenantGuid,Name);
            bool t = false;

            DataSet ds = DbHelperSQL.Query(mysql);
            if (ds != null && ds.Tables.Count > 0)
            {
                t = true;
            }

            return t;
        }

        public static int GetLocalTenantIdByGuid(string TenantGuid)
        {
            string mysql = string.Format("select top 1 Id from LocalTenants where TenantGuid='{0}' ", TenantGuid);
            int tenantid = 0;

            DataSet ds = DbHelperSQL.Query(mysql);
            if (ds != null && ds.Tables[0] != null && ds.Tables[0] .Rows.Count>0&& ds.Tables.Count > 0)
            {
                tenantid = SafeConvert.ToInt(ds.Tables[0].Rows[0]["Id"]);
            }

            return tenantid;
        }
    }
}
