﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 帐号资金流转表
	/// </summary>
	[Serializable]
	public partial class BankAccountDetails
	{
		public BankAccountDetails()
		{}
		#region Model
		private int _id;
		private int? _bankaccountid;
		private int? _increaseorreduce;
		private string _specificamount;
		private string _remarks;
		private DateTime? _occurrencetime;
		private int? _makecollectionsid;
		private int? _advicepaymentid;
		private int? _createaccountid;
		private DateTime? _createtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 所属银行帐号Id
		/// </summary>
		public int? BankAccountId
		{
			set{ _bankaccountid=value;}
			get{return _bankaccountid;}
		}
		/// <summary>
		/// 增加or减少 1-为增加,0-为减少
		/// </summary>
		public int? IncreaseOrReduce
		{
			set{ _increaseorreduce=value;}
			get{return _increaseorreduce;}
		}
		/// <summary>
		/// 具体金额
		/// </summary>
		public string SpecificAmount
		{
			set{ _specificamount=value;}
			get{return _specificamount;}
		}
		/// <summary>
		/// 备注
		/// </summary>
		public string Remarks
		{
			set{ _remarks=value;}
			get{return _remarks;}
		}
		/// <summary>
		/// 发生时间
		/// </summary>
		public DateTime? OccurrenceTime
		{
			set{ _occurrencetime=value;}
			get{return _occurrencetime;}
		}
		/// <summary>
		/// 收款单id 若没有则为0
		/// </summary>
		public int? MakeCollectionsId
		{
			set{ _makecollectionsid=value;}
			get{return _makecollectionsid;}
		}
		/// <summary>
		/// 付款单id
		/// </summary>
		public int? AdvicePaymentId
		{
			set{ _advicepaymentid=value;}
			get{return _advicepaymentid;}
		}
		/// <summary>
		/// 录入人
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 录入时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

