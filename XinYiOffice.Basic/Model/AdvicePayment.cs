﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 付款单
	/// </summary>
	[Serializable]
	public partial class AdvicePayment
	{
		public AdvicePayment()
		{}
		#region Model
		private int _id;
		private string _documentnumber;
		private string _documentdescription;
		private int? _methodpayment;
		private string _suppliername;
		private string _clientname;
		private string _payeefullname;
		private int? _payeeaccountid;
		private DateTime? _paymentdate;
		private DateTime? _targetdate;
		private string _remark;
		private decimal? _paymentamount;
		private int? _paymentbankaccountid;
		private int? _sate;
		private int? _createaccountid;
		private DateTime? _createtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 单据编号
		/// </summary>
		public string DocumentNumber
		{
			set{ _documentnumber=value;}
			get{return _documentnumber;}
		}
		/// <summary>
		/// 单据描述
		/// </summary>
		public string DocumentDescription
		{
			set{ _documentdescription=value;}
			get{return _documentdescription;}
		}
		/// <summary>
		/// 付款方式 1-现金,2-银行转帐,3-支票,4-网银
		/// </summary>
		public int? MethodPayment
		{
			set{ _methodpayment=value;}
			get{return _methodpayment;}
		}
		/// <summary>
		/// 供应商
		/// </summary>
		public string SupplierName
		{
			set{ _suppliername=value;}
			get{return _suppliername;}
		}
		/// <summary>
		/// 客户名称 客户可能也是付款人
		/// </summary>
		public string ClientName
		{
			set{ _clientname=value;}
			get{return _clientname;}
		}
		/// <summary>
		/// 付款人
		/// </summary>
		public string PayeeFullName
		{
			set{ _payeefullname=value;}
			get{return _payeefullname;}
		}
		/// <summary>
		/// 付款人帐号 没有则为0
		/// </summary>
		public int? PayeeAccountId
		{
			set{ _payeeaccountid=value;}
			get{return _payeeaccountid;}
		}
		/// <summary>
		/// 实际付款日期
		/// </summary>
		public DateTime? PaymentDate
		{
			set{ _paymentdate=value;}
			get{return _paymentdate;}
		}
		/// <summary>
		/// 预定付款日期
		/// </summary>
		public DateTime? TargetDate
		{
			set{ _targetdate=value;}
			get{return _targetdate;}
		}
		/// <summary>
		/// 摘要
		/// </summary>
		public string Remark
		{
			set{ _remark=value;}
			get{return _remark;}
		}
		/// <summary>
		/// 付款金额
		/// </summary>
		public decimal? PaymentAmount
		{
			set{ _paymentamount=value;}
			get{return _paymentamount;}
		}
		/// <summary>
		/// 付款银行账户
		/// </summary>
		public int? PaymentBankAccountId
		{
			set{ _paymentbankaccountid=value;}
			get{return _paymentbankaccountid;}
		}
		/// <summary>
		/// 状态 0-应付,1-已付
		/// </summary>
		public int? Sate
		{
			set{ _sate=value;}
			get{return _sate;}
		}
		/// <summary>
		/// 创建账户
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 添加时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

