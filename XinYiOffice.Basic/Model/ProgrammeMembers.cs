﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 日程组人员
	/// </summary>
	[Serializable]
	public partial class ProgrammeMembers
	{
		public ProgrammeMembers()
		{}
		#region Model
		private int _id;
		private int? _accountid;
		private int? _programmeid;
		private int? _byaccountid;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 人员id
		/// </summary>
		public int? AccountId
		{
			set{ _accountid=value;}
			get{return _accountid;}
		}
		/// <summary>
		/// 日程id 日程id
		/// </summary>
		public int? ProgrammeId
		{
			set{ _programmeid=value;}
			get{return _programmeid;}
		}
		/// <summary>
		/// 邀请者ID 如果邀请者为0则表明是单一用户的日程
		/// </summary>
		public int? ByAccountId
		{
			set{ _byaccountid=value;}
			get{return _byaccountid;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

