﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 帐号 用于帐号登录
	/// </summary>
	[Serializable]
	public partial class Accounts
	{
		public Accounts()
		{}
		#region Model
		private int _id;
		private string _accountname;
		private string _accountpassword;
		private int? _sate;
		private string _nicename;
		private string _fullname;
		private string _enname;
		private int? _timezone;
		private string _email;
		private string _headportrait;
		private string _phone;
		private int? _accounttype;
		private int? _keyid;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 用户名
		/// </summary>
		public string AccountName
		{
			set{ _accountname=value;}
			get{return _accountname;}
		}
		/// <summary>
		/// 密码
		/// </summary>
		public string AccountPassword
		{
			set{ _accountpassword=value;}
			get{return _accountpassword;}
		}
		/// <summary>
		/// 状态 1-正常，2-未激活,3-异常
		/// </summary>
		public int? Sate
		{
			set{ _sate=value;}
			get{return _sate;}
		}
		/// <summary>
		/// 昵称
		/// </summary>
		public string NiceName
		{
			set{ _nicename=value;}
			get{return _nicename;}
		}
		/// <summary>
		/// 姓名
		/// </summary>
		public string FullName
		{
			set{ _fullname=value;}
			get{return _fullname;}
		}
		/// <summary>
		/// 英文名
		/// </summary>
		public string EnName
		{
			set{ _enname=value;}
			get{return _enname;}
		}
		/// <summary>
		/// 时区
		/// </summary>
		public int? TimeZone
		{
			set{ _timezone=value;}
			get{return _timezone;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Email
		{
			set{ _email=value;}
			get{return _email;}
		}
		/// <summary>
		/// 头像 为数字便为系统默认，为非空字符则读取地址，为空则默认一个头像
		/// </summary>
		public string HeadPortrait
		{
			set{ _headportrait=value;}
			get{return _headportrait;}
		}
		/// <summary>
		/// 手机号码
		/// </summary>
		public string Phone
		{
			set{ _phone=value;}
			get{return _phone;}
		}
		/// <summary>
		/// 所属帐号类型 1-公司内部人员,2-客户,3-第三方
		/// </summary>
		public int? AccountType
		{
			set{ _accounttype=value;}
			get{return _accounttype;}
		}
		/// <summary>
		/// 帐号关联ID 为公司,就是职员ID,为客户就为客户ID或客户联系人ID,第三方
		/// </summary>
		public int? KeyId
		{
			set{ _keyid=value;}
			get{return _keyid;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

