﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 客户信息表
	/// </summary>
	[Serializable]
	public partial class ClientInfo
	{
		public ClientInfo()
		{}
		#region Model
		private int _id;
		private int? _byaccountid;
		private string _name;
		private int? _heat;
		private int? _clienttype;
		private int? _industry;
		private int? _source;
		private int? _marketingplanid;
		private string _tag;
		private string _bankdeposit;
		private string _taxno;
		private string _intnumber;
		private int? _power;
		private int? _qualityrating;
		private int? _valueassessment;
		private int? _customerlevel;
		private int? _relbetweengrade;
		private int? _seedtime;
		private int? _supclientinfoid;
		private string _bankaccount;
		private string _mobilephone;
		private int? _countriesregions;
		private string _phone;
		private string _fax;
		private string _email;
		private string _zipcode;
		private int? _province;
		private int? _city;
		private string _county;
		private string _address;
		private string _companyprofile;
		private string _remarks;
		private string _generalmanager;
		private string _businessentity;
		private string _businesslicence;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _linkclientid;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 所属人 职员ID
		/// </summary>
		public int? ByAccountId
		{
			set{ _byaccountid=value;}
			get{return _byaccountid;}
		}
		/// <summary>
		/// 客户名称
		/// </summary>
		public string Name
		{
			set{ _name=value;}
			get{return _name;}
		}
		/// <summary>
		/// 热度
		/// </summary>
		public int? Heat
		{
			set{ _heat=value;}
			get{return _heat;}
		}
		/// <summary>
		/// 类型
		/// </summary>
		public int? ClientType
		{
			set{ _clienttype=value;}
			get{return _clienttype;}
		}
		/// <summary>
		/// 所属行业
		/// </summary>
		public int? Industry
		{
			set{ _industry=value;}
			get{return _industry;}
		}
		/// <summary>
		/// 来源
		/// </summary>
		public int? Source
		{
			set{ _source=value;}
			get{return _source;}
		}
		/// <summary>
		/// 来源营销计划ID
		/// </summary>
		public int? MarketingPlanId
		{
			set{ _marketingplanid=value;}
			get{return _marketingplanid;}
		}
		/// <summary>
		/// 标签
		/// </summary>
		public string Tag
		{
			set{ _tag=value;}
			get{return _tag;}
		}
		/// <summary>
		/// 开户行
		/// </summary>
		public string BankDeposit
		{
			set{ _bankdeposit=value;}
			get{return _bankdeposit;}
		}
		/// <summary>
		/// 纳税号
		/// </summary>
		public string TaxNo
		{
			set{ _taxno=value;}
			get{return _taxno;}
		}
		/// <summary>
		/// 客户内部编号
		/// </summary>
		public string IntNumber
		{
			set{ _intnumber=value;}
			get{return _intnumber;}
		}
		/// <summary>
		/// 权限属性
		/// </summary>
		public int? Power
		{
			set{ _power=value;}
			get{return _power;}
		}
		/// <summary>
		/// 信用等级
		/// </summary>
		public int? QualityRating
		{
			set{ _qualityrating=value;}
			get{return _qualityrating;}
		}
		/// <summary>
		/// 价值评估
		/// </summary>
		public int? ValueAssessment
		{
			set{ _valueassessment=value;}
			get{return _valueassessment;}
		}
		/// <summary>
		/// 客户级别
		/// </summary>
		public int? CustomerLevel
		{
			set{ _customerlevel=value;}
			get{return _customerlevel;}
		}
		/// <summary>
		/// 关系等级
		/// </summary>
		public int? RelBetweenGrade
		{
			set{ _relbetweengrade=value;}
			get{return _relbetweengrade;}
		}
		/// <summary>
		/// 发展阶段
		/// </summary>
		public int? Seedtime
		{
			set{ _seedtime=value;}
			get{return _seedtime;}
		}
		/// <summary>
		/// 上级客户
		/// </summary>
		public int? SupClientInfoId
		{
			set{ _supclientinfoid=value;}
			get{return _supclientinfoid;}
		}
		/// <summary>
		/// 银行帐号
		/// </summary>
		public string BankAccount
		{
			set{ _bankaccount=value;}
			get{return _bankaccount;}
		}
		/// <summary>
		/// 接受短信的手机号码
		/// </summary>
		public string MobilePhone
		{
			set{ _mobilephone=value;}
			get{return _mobilephone;}
		}
		/// <summary>
		/// 国家地区
		/// </summary>
		public int? CountriesRegions
		{
			set{ _countriesregions=value;}
			get{return _countriesregions;}
		}
		/// <summary>
		/// 电话
		/// </summary>
		public string Phone
		{
			set{ _phone=value;}
			get{return _phone;}
		}
		/// <summary>
		/// 传真
		/// </summary>
		public string Fax
		{
			set{ _fax=value;}
			get{return _fax;}
		}
		/// <summary>
		/// 邮件
		/// </summary>
		public string Email
		{
			set{ _email=value;}
			get{return _email;}
		}
		/// <summary>
		/// 邮编
		/// </summary>
		public string ZipCode
		{
			set{ _zipcode=value;}
			get{return _zipcode;}
		}
		/// <summary>
		/// 省份
		/// </summary>
		public int? Province
		{
			set{ _province=value;}
			get{return _province;}
		}
		/// <summary>
		/// 城市
		/// </summary>
		public int? City
		{
			set{ _city=value;}
			get{return _city;}
		}
		/// <summary>
		/// 区县
		/// </summary>
		public string County
		{
			set{ _county=value;}
			get{return _county;}
		}
		/// <summary>
		/// 地址
		/// </summary>
		public string Address
		{
			set{ _address=value;}
			get{return _address;}
		}
		/// <summary>
		/// 公司简介
		/// </summary>
		public string CompanyProfile
		{
			set{ _companyprofile=value;}
			get{return _companyprofile;}
		}
		/// <summary>
		/// 备注
		/// </summary>
		public string Remarks
		{
			set{ _remarks=value;}
			get{return _remarks;}
		}
		/// <summary>
		/// 负责人
		/// </summary>
		public string GeneralManager
		{
			set{ _generalmanager=value;}
			get{return _generalmanager;}
		}
		/// <summary>
		/// 企业法人
		/// </summary>
		public string BusinessEntity
		{
			set{ _businessentity=value;}
			get{return _businessentity;}
		}
		/// <summary>
		/// 营业执照
		/// </summary>
		public string BusinessLicence
		{
			set{ _businesslicence=value;}
			get{return _businesslicence;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 友情客户
		/// </summary>
		public int? LinkClientId
		{
			set{ _linkclientid=value;}
			get{return _linkclientid;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

