﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 内部信
	/// </summary>
	[Serializable]
	public partial class InternalLetter
	{
		public InternalLetter()
		{}
		#region Model
		private int _id;
		private int? _internalletterid;
		private string _title;
		private string _con;
		private int? _authoraccountid;
		private int? _impdegree;
		private int? _isdelete;
		private string _remarks;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 上级信息ID 若为回复的信息可能存在此必要
		/// </summary>
		public int? InternalLetterId
		{
			set{ _internalletterid=value;}
			get{return _internalletterid;}
		}
		/// <summary>
		/// 标题
		/// </summary>
		public string Title
		{
			set{ _title=value;}
			get{return _title;}
		}
		/// <summary>
		/// 内容
		/// </summary>
		public string Con
		{
			set{ _con=value;}
			get{return _con;}
		}
		/// <summary>
		/// 撰写人
		/// </summary>
		public int? AuthorAccountId
		{
			set{ _authoraccountid=value;}
			get{return _authoraccountid;}
		}
		/// <summary>
		/// 重要程度 1-普通,2-高,3-重要,4-非常重要
		/// </summary>
		public int? ImpDegree
		{
			set{ _impdegree=value;}
			get{return _impdegree;}
		}
		/// <summary>
		/// 是否删除 此字段只有作者可更改为删除,则不在发件箱里显示
		/// </summary>
		public int? IsDelete
		{
			set{ _isdelete=value;}
			get{return _isdelete;}
		}
		/// <summary>
		/// 备注
		/// </summary>
		public string Remarks
		{
			set{ _remarks=value;}
			get{return _remarks;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

