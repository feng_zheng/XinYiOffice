﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 客户_营销计划
	/// </summary>
	[Serializable]
	public partial class MarketingPlan
	{
		public MarketingPlan()
		{}
		#region Model
		private int _id;
		private string _programname;
		private string _programdescription;
		private int? _sate;
		private DateTime? _starttime;
		private DateTime? _endtime;
		private int? _type;
		private decimal? _anticipatedrevenue;
		private decimal? _budgetcost;
		private int? _schemeraccountid;
		private int? _executeaccountid;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 计划名称
		/// </summary>
		public string ProgramName
		{
			set{ _programname=value;}
			get{return _programname;}
		}
		/// <summary>
		/// 计划描述
		/// </summary>
		public string ProgramDescription
		{
			set{ _programdescription=value;}
			get{return _programdescription;}
		}
		/// <summary>
		/// 状态
		/// </summary>
		public int? Sate
		{
			set{ _sate=value;}
			get{return _sate;}
		}
		/// <summary>
		/// 开始时间
		/// </summary>
		public DateTime? StartTime
		{
			set{ _starttime=value;}
			get{return _starttime;}
		}
		/// <summary>
		/// 结束时间
		/// </summary>
		public DateTime? EndTime
		{
			set{ _endtime=value;}
			get{return _endtime;}
		}
		/// <summary>
		/// 类型
		/// </summary>
		public int? TYPE
		{
			set{ _type=value;}
			get{return _type;}
		}
		/// <summary>
		/// 预期收入
		/// </summary>
		public decimal? AnticipatedRevenue
		{
			set{ _anticipatedrevenue=value;}
			get{return _anticipatedrevenue;}
		}
		/// <summary>
		/// 预算成本
		/// </summary>
		public decimal? BudgetCost
		{
			set{ _budgetcost=value;}
			get{return _budgetcost;}
		}
		/// <summary>
		/// 计划人
		/// </summary>
		public int? SchemerAccountId
		{
			set{ _schemeraccountid=value;}
			get{return _schemeraccountid;}
		}
		/// <summary>
		/// 执行人
		/// </summary>
		public int? ExecuteAccountId
		{
			set{ _executeaccountid=value;}
			get{return _executeaccountid;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

