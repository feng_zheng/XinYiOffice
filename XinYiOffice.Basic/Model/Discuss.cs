﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 讨论表
	/// </summary>
	[Serializable]
	public partial class Discuss
	{
		public Discuss()
		{}
		#region Model
		private int _id;
		private int? _projectinfoid;
		private int? _discussclassid;
		private int? _parentdiscussid;
		private string _title;
		private string _con;
		private int? _istop;
		private int? _isshow;
		private int? _sate;
		private int? _sort;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 所属项目ID
		/// </summary>
		public int? ProjectInfoId
		{
			set{ _projectinfoid=value;}
			get{return _projectinfoid;}
		}
		/// <summary>
		/// 所属讨论分类ID
		/// </summary>
		public int? DiscussClassId
		{
			set{ _discussclassid=value;}
			get{return _discussclassid;}
		}
		/// <summary>
		/// 上级讨论
		/// </summary>
		public int? ParentDiscussId
		{
			set{ _parentdiscussid=value;}
			get{return _parentdiscussid;}
		}
		/// <summary>
		/// 标题
		/// </summary>
		public string Title
		{
			set{ _title=value;}
			get{return _title;}
		}
		/// <summary>
		/// 内容
		/// </summary>
		public string Con
		{
			set{ _con=value;}
			get{return _con;}
		}
		/// <summary>
		/// 是否置顶
		/// </summary>
		public int? IsTop
		{
			set{ _istop=value;}
			get{return _istop;}
		}
		/// <summary>
		/// 是否隐藏
		/// </summary>
		public int? IsShow
		{
			set{ _isshow=value;}
			get{return _isshow;}
		}
		/// <summary>
		/// 状态 是否开启讨论或可修改
		/// </summary>
		public int? Sate
		{
			set{ _sate=value;}
			get{return _sate;}
		}
		/// <summary>
		/// 排序
		/// </summary>
		public int? Sort
		{
			set{ _sort=value;}
			get{return _sort;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

