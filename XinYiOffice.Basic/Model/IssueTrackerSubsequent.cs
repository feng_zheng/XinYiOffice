﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 任务跟踪_后续处理
	/// </summary>
	[Serializable]
	public partial class IssueTrackerSubsequent
	{
		public IssueTrackerSubsequent()
		{}
		#region Model
		private int _id;
		private int? _issuetrackerid;
		private string _con;
		private int? _sate=0;
		private int? _assigneraccountid=0;
		private int? _createaccountid;
		private int? _refreshaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 所属追踪ID
		/// </summary>
		public int? IssueTrackerId
		{
			set{ _issuetrackerid=value;}
			get{return _issuetrackerid;}
		}
		/// <summary>
		/// 内容
		/// </summary>
		public string Con
		{
			set{ _con=value;}
			get{return _con;}
		}
		/// <summary>
		/// 后续处理的状态 继承IssueTracker的Sate
		/// </summary>
		public int? Sate
		{
			set{ _sate=value;}
			get{return _sate;}
		}
		/// <summary>
		/// 解决人
		/// </summary>
		public int? AssignerAccountId
		{
			set{ _assigneraccountid=value;}
			get{return _assigneraccountid;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

