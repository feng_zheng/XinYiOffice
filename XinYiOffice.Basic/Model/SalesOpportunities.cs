﻿using System;
namespace XinYiOffice.Model
{
	/// <summary>
	/// 销售机会
	/// </summary>
	[Serializable]
	public partial class SalesOpportunities
	{
		public SalesOpportunities()
		{}
		#region Model
		private int _id;
		private string _oppname;
		private int? _customerservice;
		private int? _personalaccountid;
		private int? _marketingplanid;
		private int? _source;
		private string _requirement;
		private decimal? _estamount;
		private string _currencynotes;
		private int? _feasibility;
		private int? _presentstage;
		private int? _nextstep;
		private int? _sate;
		private string _stageremarks;
		private DateTime? _occtime;
		private DateTime? _estdate;
		private int? _refreshaccountid;
		private int? _createaccountid;
		private DateTime? _createtime;
		private DateTime? _refreshtime;
		private int? _tenantid;
		/// <summary>
		/// 编号
		/// </summary>
		public int Id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 机会名称
		/// </summary>
		public string OppName
		{
			set{ _oppname=value;}
			get{return _oppname;}
		}
		/// <summary>
		/// 所属客户
		/// </summary>
		public int? CustomerService
		{
			set{ _customerservice=value;}
			get{return _customerservice;}
		}
		/// <summary>
		/// 所属人
		/// </summary>
		public int? PersonalAccountId
		{
			set{ _personalaccountid=value;}
			get{return _personalaccountid;}
		}
		/// <summary>
		/// 来自的营销计划
		/// </summary>
		public int? MarketingPlanId
		{
			set{ _marketingplanid=value;}
			get{return _marketingplanid;}
		}
		/// <summary>
		/// 来源
		/// </summary>
		public int? Source
		{
			set{ _source=value;}
			get{return _source;}
		}
		/// <summary>
		/// 客户需求描述
		/// </summary>
		public string Requirement
		{
			set{ _requirement=value;}
			get{return _requirement;}
		}
		/// <summary>
		/// 预计金额
		/// </summary>
		public decimal? EstAmount
		{
			set{ _estamount=value;}
			get{return _estamount;}
		}
		/// <summary>
		/// 外币备注
		/// </summary>
		public string CurrencyNotes
		{
			set{ _currencynotes=value;}
			get{return _currencynotes;}
		}
		/// <summary>
		/// 可能性
		/// </summary>
		public int? Feasibility
		{
			set{ _feasibility=value;}
			get{return _feasibility;}
		}
		/// <summary>
		/// 目前阶段
		/// </summary>
		public int? PresentStage
		{
			set{ _presentstage=value;}
			get{return _presentstage;}
		}
		/// <summary>
		/// 下一步
		/// </summary>
		public int? NextStep
		{
			set{ _nextstep=value;}
			get{return _nextstep;}
		}
		/// <summary>
		/// 状态
		/// </summary>
		public int? Sate
		{
			set{ _sate=value;}
			get{return _sate;}
		}
		/// <summary>
		/// 阶段备注
		/// </summary>
		public string StageRemarks
		{
			set{ _stageremarks=value;}
			get{return _stageremarks;}
		}
		/// <summary>
		/// 发生时间
		/// </summary>
		public DateTime? OccTime
		{
			set{ _occtime=value;}
			get{return _occtime;}
		}
		/// <summary>
		/// 预计确定日期
		/// </summary>
		public DateTime? EstDate
		{
			set{ _estdate=value;}
			get{return _estdate;}
		}
		/// <summary>
		/// 更新者id
		/// </summary>
		public int? RefreshAccountId
		{
			set{ _refreshaccountid=value;}
			get{return _refreshaccountid;}
		}
		/// <summary>
		/// 添加者id
		/// </summary>
		public int? CreateAccountId
		{
			set{ _createaccountid=value;}
			get{return _createaccountid;}
		}
		/// <summary>
		/// 创建时间
		/// </summary>
		public DateTime? CreateTime
		{
			set{ _createtime=value;}
			get{return _createtime;}
		}
		/// <summary>
		/// 更新时间 最近一次编辑的人员ID,添加既为添加
		/// </summary>
		public DateTime? RefreshTime
		{
			set{ _refreshtime=value;}
			get{return _refreshtime;}
		}
		/// <summary>
		/// 租户id
		/// </summary>
		public int? TenantId
		{
			set{ _tenantid=value;}
			get{return _tenantid;}
		}
		#endregion Model

	}
}

