﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using XinYiOffice.Common;

namespace XinYiOffice.Basic
{
    public class ClientServer
    {
        /// <summary>
        /// 获得 销售机会 视图
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public static DataSet GetSalesOpportunitiesList(string strWhere, int TenantId)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM vSalesOpportunities ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
                strSql.Append(string.Format(" and TenantId={0} ", TenantId));
            }
            strSql.Append(" order by CreateTime desc");
            return DbHelperSQL.Query(strSql.ToString());
        }

        public static string GetSalesOpportunitiesListSql(string strWhere, int TenantId)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM vSalesOpportunities ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
                strSql.Append(string.Format(" and TenantId={0} ", TenantId));
            }
            strSql.Append(" order by CreateTime desc");
            return strSql.ToString();
        }

        /// <summary>
        /// 获取 客户列表 视图
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public static DataSet GetClientInfoList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM.vClientInfo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by CreateTime desc");
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取 客户列表 视图
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public static string GetClientInfoListSql(string strWhere, int TenantId)
        {
            StringBuilder strSql = new StringBuilder();
            //strSql.Append("select [Id],[ByAccountId],[Name],[Heat],[ClientType],[Industry],[Source],[MarketingPlanId],[Tag],[BankDeposit],[TaxNo],[IntNumber],[Power],[QualityRating],[ValueAssessment],[CustomerLevel],[RelBetweenGrade],[Seedtime],[SupClientInfoId],[BankAccount],[MobilePhone],[CountriesRegions],[Phone],[Fax],[Email],[ZipCode],[Province],[City],[County],[Address],[CompanyProfile],[Remarks],[GeneralManager],[BusinessEntity],[BusinessLicence],[CreateAccountId],[RefreshAccountId],[CreateTime],[RefreshTime],[CityName],[ProvinceName],[ProgramName],[AccountName],[HeatName],[ClientTypeName],[IndustryName],[SourceName],[PowerName],[QualityRatingName],[CustomerLevelName],[RelBetweenGradeName],[ValueAssessmentName],[SeedtimeName],[SupClientInfoName],[CountriesRegionsName],[FullName],[TenantId] ");
            //strSql.Append(" FROM vClientInfo ");
            strSql.Append("select [Name],[Id],[ByAccountId],[CreateTime],[TenantId],[HeatName],[CustomerLevelName],[ClientTypeName],[SourceName],[QualityRatingName] ");
             strSql.Append(" FROM vClientInfoList ");

            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
                strSql.Append(string.Format(" and TenantId={0} ", TenantId));
            }
            strSql.Append(" order by Id desc");
            return strSql.ToString();
        }

        /// <summary>
        /// 获取营销计划视图
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public static DataSet GetMarketingPlan(string strWhere, int TenantId)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM vMarketingPlan ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
                strSql.Append(string.Format(" and TenantId={0} ", TenantId));
            }
            strSql.Append(" order by StartTime desc");
            return DbHelperSQL.Query(strSql.ToString());
        }

        public static string GetMarketingPlanSql(string strWhere, int TenantId)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM vMarketingPlan ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
                strSql.Append(string.Format(" and TenantId={0} ", TenantId));
            }
            strSql.Append(" order by Id desc ");
            return  strSql.ToString();
        }

        /// <summary>
        /// 客户跟进表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public static DataSet GetClientTracking(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM vClientTracking ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by CreateTime desc");
            return DbHelperSQL.Query(strSql.ToString());
        }


        /// <summary>
        /// 客户跟进表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public static DataSet GetClientTracking(string strWhere,int top)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select top {0} * ");
            strSql.Append(" FROM vClientTracking ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by CreateTime desc");
            string sql = string.Format(strSql.ToString(),top);
            return DbHelperSQL.Query(sql);
        }

        public static string GetClientTrackingSql(string strWhere, int TenantId)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  * ");
            strSql.Append(" FROM vClientTracking ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
                //strSql.Append(string.Format(" and TenantId={0} ", TenantId));
            }

            strSql.Append(" order by CreateTime desc");

            return strSql.ToString();
        }

        /// <summary>
        /// 获取客户联系人
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public static DataSet GetClientLiaisons(string strWhere, int TenantId)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM vClientLiaisons ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
                strSql.Append(string.Format(" and TenantId={0} ", TenantId));
            }
            
            strSql.Append(" order by CreateTime desc");

            return DbHelperSQL.Query(strSql.ToString());
        }

        public static string GetClientLiaisonsSql(string strWhere, int TenantId)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select [ClientInfoName],[Id],[ClientInfoId],[PersonNumber],[FullName],[Power],[Phone],[OtherPhone],[Fax],[Email],[QQ],[Birthday],[ByAccountId],[Department],[FunctionName],[DirectSuperior],[Source],[IfContact],[ClassiFication],[Business],[PrimaryContact],[ShortPhone],[Sex],[CountriesRegions],[Zip],[Address],[Province],[County],[Remarks],[CreateAccountId],[RefreshAccountId],[CreateTime],[RefreshTime],[ClassiFicationName],[PowerName],[SourceName],[PrimaryContactName],[IfContactName],[SexName],[ProvinceName],[CountyName],[ByAccountFullName],[TenantId] ");
            strSql.Append(" FROM vClientLiaisons ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
                strSql.Append(string.Format(" and TenantId={0} ", TenantId));
            }

            strSql.Append(" order by CreateTime desc");

            return strSql.ToString();
        }
    }


}
