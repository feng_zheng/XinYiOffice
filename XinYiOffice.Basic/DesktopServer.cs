﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using XinYiOffice.Common;

namespace XinYiOffice.Basic
{
    public class DesktopServer
    {
        /// <summary>
        /// 根据用户id获取我的桌面
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="topNum"></param>
        /// <returns></returns>
        public static DataTable GetMyDesktop(int accountId, int TenantId)
        {
            string mysql = string.Format("select * from vMyDesktop where AccountId={0} and TenantId={1} order by Sort asc", accountId, TenantId);

            return DbHelperSQL.Query(mysql).Tables[0];
        }

        /// <summary>
        /// 获取 所有桌面组件
        /// </summary>
        /// <returns></returns>
        public static DataTable GetDesktopAllPlug()
        {
            string mysql = "select * from DesktopPlug";
            return DbHelperSQL.Query(mysql).Tables[0];
        }
    }
}
