﻿using System;
using System.Data;
using System.Collections.Generic;
using XinYiOffice.Common;
using XinYiOffice.Model;
namespace XinYiOffice.BLL
{
	/// <summary>
	/// 搜索项
	/// </summary>
	public partial class SearchConfigItem
	{
		private readonly XinYiOffice.DAL.SearchConfigItem dal=new XinYiOffice.DAL.SearchConfigItem();
		public SearchConfigItem()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			return dal.Exists(Id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(XinYiOffice.Model.SearchConfigItem model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.SearchConfigItem model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			return dal.Delete(Id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			return dal.DeleteList(Idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.SearchConfigItem GetModel(int Id)
		{
			
			return dal.GetModel(Id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public XinYiOffice.Model.SearchConfigItem GetModelByCache(int Id)
		{
			
			string CacheKey = "SearchConfigItemModel-" + Id;
			object objModel = XinYiOffice.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(Id);
					if (objModel != null)
					{
						int ModelCache = XinYiOffice.Common.ConfigHelper.GetConfigInt("ModelCache");
						XinYiOffice.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (XinYiOffice.Model.SearchConfigItem)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.SearchConfigItem> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.SearchConfigItem> DataTableToList(DataTable dt)
		{
			List<XinYiOffice.Model.SearchConfigItem> modelList = new List<XinYiOffice.Model.SearchConfigItem>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				XinYiOffice.Model.SearchConfigItem model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new XinYiOffice.Model.SearchConfigItem();
					if(dt.Rows[n]["Id"]!=null && dt.Rows[n]["Id"].ToString()!="")
					{
						model.Id=int.Parse(dt.Rows[n]["Id"].ToString());
					}
					if(dt.Rows[n]["DisplayName"]!=null && dt.Rows[n]["DisplayName"].ToString()!="")
					{
					model.DisplayName=dt.Rows[n]["DisplayName"].ToString();
					}
					if(dt.Rows[n]["FieldName"]!=null && dt.Rows[n]["FieldName"].ToString()!="")
					{
					model.FieldName=dt.Rows[n]["FieldName"].ToString();
					}
					if(dt.Rows[n]["SearchDataSoureId"]!=null && dt.Rows[n]["SearchDataSoureId"].ToString()!="")
					{
						model.SearchDataSoureId=int.Parse(dt.Rows[n]["SearchDataSoureId"].ToString());
					}
					if(dt.Rows[n]["Sort"]!=null && dt.Rows[n]["Sort"].ToString()!="")
					{
						model.Sort=int.Parse(dt.Rows[n]["Sort"].ToString());
					}
					if(dt.Rows[n]["ValueType"]!=null && dt.Rows[n]["ValueType"].ToString()!="")
					{
						model.ValueType=int.Parse(dt.Rows[n]["ValueType"].ToString());
					}
					if(dt.Rows[n]["ValueRelChar"]!=null && dt.Rows[n]["ValueRelChar"].ToString()!="")
					{
					model.ValueRelChar=dt.Rows[n]["ValueRelChar"].ToString();
					}
					if(dt.Rows[n]["PrefixChar"]!=null && dt.Rows[n]["PrefixChar"].ToString()!="")
					{
					model.PrefixChar=dt.Rows[n]["PrefixChar"].ToString();
					}
					if(dt.Rows[n]["PrefixRelChar"]!=null && dt.Rows[n]["PrefixRelChar"].ToString()!="")
					{
					model.PrefixRelChar=dt.Rows[n]["PrefixRelChar"].ToString();
					}
					if(dt.Rows[n]["SuffixChar"]!=null && dt.Rows[n]["SuffixChar"].ToString()!="")
					{
					model.SuffixChar=dt.Rows[n]["SuffixChar"].ToString();
					}
					if(dt.Rows[n]["SuffixRelChar"]!=null && dt.Rows[n]["SuffixRelChar"].ToString()!="")
					{
					model.SuffixRelChar=dt.Rows[n]["SuffixRelChar"].ToString();
					}
					if(dt.Rows[n]["SearchConfigId"]!=null && dt.Rows[n]["SearchConfigId"].ToString()!="")
					{
						model.SearchConfigId=int.Parse(dt.Rows[n]["SearchConfigId"].ToString());
					}
					if(dt.Rows[n]["UseValueType"]!=null && dt.Rows[n]["UseValueType"].ToString()!="")
					{
						model.UseValueType=int.Parse(dt.Rows[n]["UseValueType"].ToString());
					}
					if(dt.Rows[n]["DefaultValue"]!=null && dt.Rows[n]["DefaultValue"].ToString()!="")
					{
					model.DefaultValue=dt.Rows[n]["DefaultValue"].ToString();
					}
					if(dt.Rows[n]["CreateAccountId"]!=null && dt.Rows[n]["CreateAccountId"].ToString()!="")
					{
						model.CreateAccountId=int.Parse(dt.Rows[n]["CreateAccountId"].ToString());
					}
					if(dt.Rows[n]["RefreshAccountId"]!=null && dt.Rows[n]["RefreshAccountId"].ToString()!="")
					{
						model.RefreshAccountId=int.Parse(dt.Rows[n]["RefreshAccountId"].ToString());
					}
					if(dt.Rows[n]["CreateTime"]!=null && dt.Rows[n]["CreateTime"].ToString()!="")
					{
						model.CreateTime=DateTime.Parse(dt.Rows[n]["CreateTime"].ToString());
					}
					if(dt.Rows[n]["RefreshTime"]!=null && dt.Rows[n]["RefreshTime"].ToString()!="")
					{
						model.RefreshTime=DateTime.Parse(dt.Rows[n]["RefreshTime"].ToString());
					}
					if(dt.Rows[n]["TenantId"]!=null && dt.Rows[n]["TenantId"].ToString()!="")
					{
						model.TenantId=int.Parse(dt.Rows[n]["TenantId"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

