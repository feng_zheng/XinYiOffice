﻿using System;
using System.Data;
using System.Collections.Generic;
using XinYiOffice.Common;
using XinYiOffice.Model;
namespace XinYiOffice.BLL
{
	/// <summary>
	/// 客户联络人
	/// </summary>
	public partial class ClientLiaisons
	{
		private readonly XinYiOffice.DAL.ClientLiaisons dal=new XinYiOffice.DAL.ClientLiaisons();
		public ClientLiaisons()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			return dal.Exists(Id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(XinYiOffice.Model.ClientLiaisons model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.ClientLiaisons model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			return dal.Delete(Id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			return dal.DeleteList(Idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.ClientLiaisons GetModel(int Id)
		{
			
			return dal.GetModel(Id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public XinYiOffice.Model.ClientLiaisons GetModelByCache(int Id)
		{
			
			string CacheKey = "ClientLiaisonsModel-" + Id;
			object objModel = XinYiOffice.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(Id);
					if (objModel != null)
					{
						int ModelCache = XinYiOffice.Common.ConfigHelper.GetConfigInt("ModelCache");
						XinYiOffice.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (XinYiOffice.Model.ClientLiaisons)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.ClientLiaisons> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.ClientLiaisons> DataTableToList(DataTable dt)
		{
			List<XinYiOffice.Model.ClientLiaisons> modelList = new List<XinYiOffice.Model.ClientLiaisons>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				XinYiOffice.Model.ClientLiaisons model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new XinYiOffice.Model.ClientLiaisons();
					if(dt.Rows[n]["Id"]!=null && dt.Rows[n]["Id"].ToString()!="")
					{
						model.Id=int.Parse(dt.Rows[n]["Id"].ToString());
					}
					if(dt.Rows[n]["ClientInfoId"]!=null && dt.Rows[n]["ClientInfoId"].ToString()!="")
					{
						model.ClientInfoId=int.Parse(dt.Rows[n]["ClientInfoId"].ToString());
					}
					if(dt.Rows[n]["PersonNumber"]!=null && dt.Rows[n]["PersonNumber"].ToString()!="")
					{
					model.PersonNumber=dt.Rows[n]["PersonNumber"].ToString();
					}
					if(dt.Rows[n]["FullName"]!=null && dt.Rows[n]["FullName"].ToString()!="")
					{
					model.FullName=dt.Rows[n]["FullName"].ToString();
					}
					if(dt.Rows[n]["Power"]!=null && dt.Rows[n]["Power"].ToString()!="")
					{
						model.Power=int.Parse(dt.Rows[n]["Power"].ToString());
					}
					if(dt.Rows[n]["Phone"]!=null && dt.Rows[n]["Phone"].ToString()!="")
					{
					model.Phone=dt.Rows[n]["Phone"].ToString();
					}
					if(dt.Rows[n]["OtherPhone"]!=null && dt.Rows[n]["OtherPhone"].ToString()!="")
					{
					model.OtherPhone=dt.Rows[n]["OtherPhone"].ToString();
					}
					if(dt.Rows[n]["Fax"]!=null && dt.Rows[n]["Fax"].ToString()!="")
					{
					model.Fax=dt.Rows[n]["Fax"].ToString();
					}
					if(dt.Rows[n]["Email"]!=null && dt.Rows[n]["Email"].ToString()!="")
					{
					model.Email=dt.Rows[n]["Email"].ToString();
					}
					if(dt.Rows[n]["QQ"]!=null && dt.Rows[n]["QQ"].ToString()!="")
					{
					model.QQ=dt.Rows[n]["QQ"].ToString();
					}
					if(dt.Rows[n]["Birthday"]!=null && dt.Rows[n]["Birthday"].ToString()!="")
					{
					model.Birthday=dt.Rows[n]["Birthday"].ToString();
					}
					if(dt.Rows[n]["ByAccountId"]!=null && dt.Rows[n]["ByAccountId"].ToString()!="")
					{
						model.ByAccountId=int.Parse(dt.Rows[n]["ByAccountId"].ToString());
					}
					if(dt.Rows[n]["Department"]!=null && dt.Rows[n]["Department"].ToString()!="")
					{
					model.Department=dt.Rows[n]["Department"].ToString();
					}
					if(dt.Rows[n]["FunctionName"]!=null && dt.Rows[n]["FunctionName"].ToString()!="")
					{
					model.FunctionName=dt.Rows[n]["FunctionName"].ToString();
					}
					if(dt.Rows[n]["DirectSuperior_BK2_"]!=null && dt.Rows[n]["DirectSuperior_BK2_"].ToString()!="")
					{
					model.DirectSuperior_BK2_=dt.Rows[n]["DirectSuperior_BK2_"].ToString();
					}
					if(dt.Rows[n]["Source"]!=null && dt.Rows[n]["Source"].ToString()!="")
					{
						model.Source=int.Parse(dt.Rows[n]["Source"].ToString());
					}
					if(dt.Rows[n]["IfContact"]!=null && dt.Rows[n]["IfContact"].ToString()!="")
					{
						model.IfContact=int.Parse(dt.Rows[n]["IfContact"].ToString());
					}
					if(dt.Rows[n]["ClassiFication"]!=null && dt.Rows[n]["ClassiFication"].ToString()!="")
					{
						model.ClassiFication=int.Parse(dt.Rows[n]["ClassiFication"].ToString());
					}
					if(dt.Rows[n]["Business"]!=null && dt.Rows[n]["Business"].ToString()!="")
					{
					model.Business=dt.Rows[n]["Business"].ToString();
					}
					if(dt.Rows[n]["PrimaryContact"]!=null && dt.Rows[n]["PrimaryContact"].ToString()!="")
					{
						model.PrimaryContact=int.Parse(dt.Rows[n]["PrimaryContact"].ToString());
					}
					if(dt.Rows[n]["ShortPhone"]!=null && dt.Rows[n]["ShortPhone"].ToString()!="")
					{
					model.ShortPhone=dt.Rows[n]["ShortPhone"].ToString();
					}
					if(dt.Rows[n]["Sex"]!=null && dt.Rows[n]["Sex"].ToString()!="")
					{
						model.Sex=int.Parse(dt.Rows[n]["Sex"].ToString());
					}
					if(dt.Rows[n]["CountriesRegions"]!=null && dt.Rows[n]["CountriesRegions"].ToString()!="")
					{
					model.CountriesRegions=dt.Rows[n]["CountriesRegions"].ToString();
					}
					if(dt.Rows[n]["Zip"]!=null && dt.Rows[n]["Zip"].ToString()!="")
					{
					model.Zip=dt.Rows[n]["Zip"].ToString();
					}
					if(dt.Rows[n]["Address"]!=null && dt.Rows[n]["Address"].ToString()!="")
					{
					model.Address=dt.Rows[n]["Address"].ToString();
					}
					if(dt.Rows[n]["Province"]!=null && dt.Rows[n]["Province"].ToString()!="")
					{
						model.Province=int.Parse(dt.Rows[n]["Province"].ToString());
					}
					if(dt.Rows[n]["County"]!=null && dt.Rows[n]["County"].ToString()!="")
					{
						model.County=int.Parse(dt.Rows[n]["County"].ToString());
					}
					if(dt.Rows[n]["Remarks"]!=null && dt.Rows[n]["Remarks"].ToString()!="")
					{
					model.Remarks=dt.Rows[n]["Remarks"].ToString();
					}
					if(dt.Rows[n]["CreateAccountId"]!=null && dt.Rows[n]["CreateAccountId"].ToString()!="")
					{
						model.CreateAccountId=int.Parse(dt.Rows[n]["CreateAccountId"].ToString());
					}
					if(dt.Rows[n]["RefreshAccountId"]!=null && dt.Rows[n]["RefreshAccountId"].ToString()!="")
					{
						model.RefreshAccountId=int.Parse(dt.Rows[n]["RefreshAccountId"].ToString());
					}
					if(dt.Rows[n]["CreateTime"]!=null && dt.Rows[n]["CreateTime"].ToString()!="")
					{
						model.CreateTime=DateTime.Parse(dt.Rows[n]["CreateTime"].ToString());
					}
					if(dt.Rows[n]["RefreshTime"]!=null && dt.Rows[n]["RefreshTime"].ToString()!="")
					{
						model.RefreshTime=DateTime.Parse(dt.Rows[n]["RefreshTime"].ToString());
					}
					if(dt.Rows[n]["DirectSuperior"]!=null && dt.Rows[n]["DirectSuperior"].ToString()!="")
					{
						model.DirectSuperior=int.Parse(dt.Rows[n]["DirectSuperior"].ToString());
					}
					if(dt.Rows[n]["TenantId"]!=null && dt.Rows[n]["TenantId"].ToString()!="")
					{
						model.TenantId=int.Parse(dt.Rows[n]["TenantId"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

