﻿using System;
using System.Data;
using System.Collections.Generic;
using XinYiOffice.Common;
using XinYiOffice.Model;
namespace XinYiOffice.BLL
{
	/// <summary>
	/// 搜索项所使用的数据源
	/// </summary>
	public partial class SearchDataSoure
	{
		private readonly XinYiOffice.DAL.SearchDataSoure dal=new XinYiOffice.DAL.SearchDataSoure();
		public SearchDataSoure()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			return dal.Exists(Id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(XinYiOffice.Model.SearchDataSoure model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.SearchDataSoure model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			return dal.Delete(Id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			return dal.DeleteList(Idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.SearchDataSoure GetModel(int Id)
		{
			
			return dal.GetModel(Id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public XinYiOffice.Model.SearchDataSoure GetModelByCache(int Id)
		{
			
			string CacheKey = "SearchDataSoureModel-" + Id;
			object objModel = XinYiOffice.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(Id);
					if (objModel != null)
					{
						int ModelCache = XinYiOffice.Common.ConfigHelper.GetConfigInt("ModelCache");
						XinYiOffice.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (XinYiOffice.Model.SearchDataSoure)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.SearchDataSoure> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<XinYiOffice.Model.SearchDataSoure> DataTableToList(DataTable dt)
		{
			List<XinYiOffice.Model.SearchDataSoure> modelList = new List<XinYiOffice.Model.SearchDataSoure>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				XinYiOffice.Model.SearchDataSoure model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new XinYiOffice.Model.SearchDataSoure();
					if(dt.Rows[n]["Id"]!=null && dt.Rows[n]["Id"].ToString()!="")
					{
						model.Id=int.Parse(dt.Rows[n]["Id"].ToString());
					}
					if(dt.Rows[n]["DataSoureType"]!=null && dt.Rows[n]["DataSoureType"].ToString()!="")
					{
						model.DataSoureType=int.Parse(dt.Rows[n]["DataSoureType"].ToString());
					}
					if(dt.Rows[n]["DataSoureConntion"]!=null && dt.Rows[n]["DataSoureConntion"].ToString()!="")
					{
					model.DataSoureConntion=dt.Rows[n]["DataSoureConntion"].ToString();
					}
					if(dt.Rows[n]["DataType"]!=null && dt.Rows[n]["DataType"].ToString()!="")
					{
						model.DataType=int.Parse(dt.Rows[n]["DataType"].ToString());
					}
					if(dt.Rows[n]["DisplayValue"]!=null && dt.Rows[n]["DisplayValue"].ToString()!="")
					{
					model.DisplayValue=dt.Rows[n]["DisplayValue"].ToString();
					}
					if(dt.Rows[n]["TrueValue"]!=null && dt.Rows[n]["TrueValue"].ToString()!="")
					{
					model.TrueValue=dt.Rows[n]["TrueValue"].ToString();
					}
					if(dt.Rows[n]["FormType"]!=null && dt.Rows[n]["FormType"].ToString()!="")
					{
						model.FormType=int.Parse(dt.Rows[n]["FormType"].ToString());
					}
					if(dt.Rows[n]["FormStyle"]!=null && dt.Rows[n]["FormStyle"].ToString()!="")
					{
					model.FormStyle=dt.Rows[n]["FormStyle"].ToString();
					}
					if(dt.Rows[n]["FormClass"]!=null && dt.Rows[n]["FormClass"].ToString()!="")
					{
					model.FormClass=dt.Rows[n]["FormClass"].ToString();
					}
					if(dt.Rows[n]["CreateAccountId"]!=null && dt.Rows[n]["CreateAccountId"].ToString()!="")
					{
						model.CreateAccountId=int.Parse(dt.Rows[n]["CreateAccountId"].ToString());
					}
					if(dt.Rows[n]["RefreshAccountId"]!=null && dt.Rows[n]["RefreshAccountId"].ToString()!="")
					{
						model.RefreshAccountId=int.Parse(dt.Rows[n]["RefreshAccountId"].ToString());
					}
					if(dt.Rows[n]["CreateTime"]!=null && dt.Rows[n]["CreateTime"].ToString()!="")
					{
						model.CreateTime=DateTime.Parse(dt.Rows[n]["CreateTime"].ToString());
					}
					if(dt.Rows[n]["RefreshTime"]!=null && dt.Rows[n]["RefreshTime"].ToString()!="")
					{
						model.RefreshTime=DateTime.Parse(dt.Rows[n]["RefreshTime"].ToString());
					}
					if(dt.Rows[n]["TenantId"]!=null && dt.Rows[n]["TenantId"].ToString()!="")
					{
						model.TenantId=int.Parse(dt.Rows[n]["TenantId"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

