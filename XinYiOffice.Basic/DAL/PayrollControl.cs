﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XinYiOffice.Common;//Please add references
namespace XinYiOffice.DAL
{
	/// <summary>
	/// 数据访问类:PayrollControl
	/// </summary>
	public partial class PayrollControl
	{
		public PayrollControl()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Id", "PayrollControl"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from PayrollControl");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(XinYiOffice.Model.PayrollControl model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into PayrollControl(");
			strSql.Append("PayDate,BasePay,AttendanceWages,CommunicationExpense,PersonalIncomeTax,HousingFund,ResidualAmount,AllWages,OvertimeWage,PercentageWages,OtherBenefits,SocialSecurity,DeductMoney,RepaymentAmount,ActualPayment,BankAccountId,PersonnelAccount,Sate,CreateAccountId,CreateTime,TenantId)");
			strSql.Append(" values (");
			strSql.Append("@PayDate,@BasePay,@AttendanceWages,@CommunicationExpense,@PersonalIncomeTax,@HousingFund,@ResidualAmount,@AllWages,@OvertimeWage,@PercentageWages,@OtherBenefits,@SocialSecurity,@DeductMoney,@RepaymentAmount,@ActualPayment,@BankAccountId,@PersonnelAccount,@Sate,@CreateAccountId,@CreateTime,@TenantId)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@PayDate", SqlDbType.DateTime),
					new SqlParameter("@BasePay", SqlDbType.Float,8),
					new SqlParameter("@AttendanceWages", SqlDbType.Float,8),
					new SqlParameter("@CommunicationExpense", SqlDbType.Float,8),
					new SqlParameter("@PersonalIncomeTax", SqlDbType.Float,8),
					new SqlParameter("@HousingFund", SqlDbType.Float,8),
					new SqlParameter("@ResidualAmount", SqlDbType.Float,8),
					new SqlParameter("@AllWages", SqlDbType.Float,8),
					new SqlParameter("@OvertimeWage", SqlDbType.Float,8),
					new SqlParameter("@PercentageWages", SqlDbType.Float,8),
					new SqlParameter("@OtherBenefits", SqlDbType.Float,8),
					new SqlParameter("@SocialSecurity", SqlDbType.Float,8),
					new SqlParameter("@DeductMoney", SqlDbType.Float,8),
					new SqlParameter("@RepaymentAmount", SqlDbType.Float,8),
					new SqlParameter("@ActualPayment", SqlDbType.Float,8),
					new SqlParameter("@BankAccountId", SqlDbType.Int,4),
					new SqlParameter("@PersonnelAccount", SqlDbType.Int,4),
					new SqlParameter("@Sate", SqlDbType.Int,4),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4)};
			parameters[0].Value = model.PayDate;
			parameters[1].Value = model.BasePay;
			parameters[2].Value = model.AttendanceWages;
			parameters[3].Value = model.CommunicationExpense;
			parameters[4].Value = model.PersonalIncomeTax;
			parameters[5].Value = model.HousingFund;
			parameters[6].Value = model.ResidualAmount;
			parameters[7].Value = model.AllWages;
			parameters[8].Value = model.OvertimeWage;
			parameters[9].Value = model.PercentageWages;
			parameters[10].Value = model.OtherBenefits;
			parameters[11].Value = model.SocialSecurity;
			parameters[12].Value = model.DeductMoney;
			parameters[13].Value = model.RepaymentAmount;
			parameters[14].Value = model.ActualPayment;
			parameters[15].Value = model.BankAccountId;
			parameters[16].Value = model.PersonnelAccount;
			parameters[17].Value = model.Sate;
			parameters[18].Value = model.CreateAccountId;
			parameters[19].Value = model.CreateTime;
			parameters[20].Value = model.TenantId;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.PayrollControl model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update PayrollControl set ");
			strSql.Append("PayDate=@PayDate,");
			strSql.Append("BasePay=@BasePay,");
			strSql.Append("AttendanceWages=@AttendanceWages,");
			strSql.Append("CommunicationExpense=@CommunicationExpense,");
			strSql.Append("PersonalIncomeTax=@PersonalIncomeTax,");
			strSql.Append("HousingFund=@HousingFund,");
			strSql.Append("ResidualAmount=@ResidualAmount,");
			strSql.Append("AllWages=@AllWages,");
			strSql.Append("OvertimeWage=@OvertimeWage,");
			strSql.Append("PercentageWages=@PercentageWages,");
			strSql.Append("OtherBenefits=@OtherBenefits,");
			strSql.Append("SocialSecurity=@SocialSecurity,");
			strSql.Append("DeductMoney=@DeductMoney,");
			strSql.Append("RepaymentAmount=@RepaymentAmount,");
			strSql.Append("ActualPayment=@ActualPayment,");
			strSql.Append("BankAccountId=@BankAccountId,");
			strSql.Append("PersonnelAccount=@PersonnelAccount,");
			strSql.Append("Sate=@Sate,");
			strSql.Append("CreateAccountId=@CreateAccountId,");
			strSql.Append("CreateTime=@CreateTime,");
			strSql.Append("TenantId=@TenantId");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@PayDate", SqlDbType.DateTime),
					new SqlParameter("@BasePay", SqlDbType.Float,8),
					new SqlParameter("@AttendanceWages", SqlDbType.Float,8),
					new SqlParameter("@CommunicationExpense", SqlDbType.Float,8),
					new SqlParameter("@PersonalIncomeTax", SqlDbType.Float,8),
					new SqlParameter("@HousingFund", SqlDbType.Float,8),
					new SqlParameter("@ResidualAmount", SqlDbType.Float,8),
					new SqlParameter("@AllWages", SqlDbType.Float,8),
					new SqlParameter("@OvertimeWage", SqlDbType.Float,8),
					new SqlParameter("@PercentageWages", SqlDbType.Float,8),
					new SqlParameter("@OtherBenefits", SqlDbType.Float,8),
					new SqlParameter("@SocialSecurity", SqlDbType.Float,8),
					new SqlParameter("@DeductMoney", SqlDbType.Float,8),
					new SqlParameter("@RepaymentAmount", SqlDbType.Float,8),
					new SqlParameter("@ActualPayment", SqlDbType.Float,8),
					new SqlParameter("@BankAccountId", SqlDbType.Int,4),
					new SqlParameter("@PersonnelAccount", SqlDbType.Int,4),
					new SqlParameter("@Sate", SqlDbType.Int,4),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.PayDate;
			parameters[1].Value = model.BasePay;
			parameters[2].Value = model.AttendanceWages;
			parameters[3].Value = model.CommunicationExpense;
			parameters[4].Value = model.PersonalIncomeTax;
			parameters[5].Value = model.HousingFund;
			parameters[6].Value = model.ResidualAmount;
			parameters[7].Value = model.AllWages;
			parameters[8].Value = model.OvertimeWage;
			parameters[9].Value = model.PercentageWages;
			parameters[10].Value = model.OtherBenefits;
			parameters[11].Value = model.SocialSecurity;
			parameters[12].Value = model.DeductMoney;
			parameters[13].Value = model.RepaymentAmount;
			parameters[14].Value = model.ActualPayment;
			parameters[15].Value = model.BankAccountId;
			parameters[16].Value = model.PersonnelAccount;
			parameters[17].Value = model.Sate;
			parameters[18].Value = model.CreateAccountId;
			parameters[19].Value = model.CreateTime;
			parameters[20].Value = model.TenantId;
			parameters[21].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from PayrollControl ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from PayrollControl ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.PayrollControl GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,PayDate,BasePay,AttendanceWages,CommunicationExpense,PersonalIncomeTax,HousingFund,ResidualAmount,AllWages,OvertimeWage,PercentageWages,OtherBenefits,SocialSecurity,DeductMoney,RepaymentAmount,ActualPayment,BankAccountId,PersonnelAccount,Sate,CreateAccountId,CreateTime,TenantId from PayrollControl ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			XinYiOffice.Model.PayrollControl model=new XinYiOffice.Model.PayrollControl();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Id"]!=null && ds.Tables[0].Rows[0]["Id"].ToString()!="")
				{
					model.Id=int.Parse(ds.Tables[0].Rows[0]["Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PayDate"]!=null && ds.Tables[0].Rows[0]["PayDate"].ToString()!="")
				{
					model.PayDate=DateTime.Parse(ds.Tables[0].Rows[0]["PayDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["BasePay"]!=null && ds.Tables[0].Rows[0]["BasePay"].ToString()!="")
				{
					model.BasePay=decimal.Parse(ds.Tables[0].Rows[0]["BasePay"].ToString());
				}
				if(ds.Tables[0].Rows[0]["AttendanceWages"]!=null && ds.Tables[0].Rows[0]["AttendanceWages"].ToString()!="")
				{
					model.AttendanceWages=decimal.Parse(ds.Tables[0].Rows[0]["AttendanceWages"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CommunicationExpense"]!=null && ds.Tables[0].Rows[0]["CommunicationExpense"].ToString()!="")
				{
					model.CommunicationExpense=decimal.Parse(ds.Tables[0].Rows[0]["CommunicationExpense"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PersonalIncomeTax"]!=null && ds.Tables[0].Rows[0]["PersonalIncomeTax"].ToString()!="")
				{
					model.PersonalIncomeTax=decimal.Parse(ds.Tables[0].Rows[0]["PersonalIncomeTax"].ToString());
				}
				if(ds.Tables[0].Rows[0]["HousingFund"]!=null && ds.Tables[0].Rows[0]["HousingFund"].ToString()!="")
				{
					model.HousingFund=decimal.Parse(ds.Tables[0].Rows[0]["HousingFund"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ResidualAmount"]!=null && ds.Tables[0].Rows[0]["ResidualAmount"].ToString()!="")
				{
					model.ResidualAmount=decimal.Parse(ds.Tables[0].Rows[0]["ResidualAmount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["AllWages"]!=null && ds.Tables[0].Rows[0]["AllWages"].ToString()!="")
				{
					model.AllWages=decimal.Parse(ds.Tables[0].Rows[0]["AllWages"].ToString());
				}
				if(ds.Tables[0].Rows[0]["OvertimeWage"]!=null && ds.Tables[0].Rows[0]["OvertimeWage"].ToString()!="")
				{
					model.OvertimeWage=decimal.Parse(ds.Tables[0].Rows[0]["OvertimeWage"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PercentageWages"]!=null && ds.Tables[0].Rows[0]["PercentageWages"].ToString()!="")
				{
					model.PercentageWages=decimal.Parse(ds.Tables[0].Rows[0]["PercentageWages"].ToString());
				}
				if(ds.Tables[0].Rows[0]["OtherBenefits"]!=null && ds.Tables[0].Rows[0]["OtherBenefits"].ToString()!="")
				{
					model.OtherBenefits=decimal.Parse(ds.Tables[0].Rows[0]["OtherBenefits"].ToString());
				}
				if(ds.Tables[0].Rows[0]["SocialSecurity"]!=null && ds.Tables[0].Rows[0]["SocialSecurity"].ToString()!="")
				{
					model.SocialSecurity=decimal.Parse(ds.Tables[0].Rows[0]["SocialSecurity"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DeductMoney"]!=null && ds.Tables[0].Rows[0]["DeductMoney"].ToString()!="")
				{
					model.DeductMoney=decimal.Parse(ds.Tables[0].Rows[0]["DeductMoney"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RepaymentAmount"]!=null && ds.Tables[0].Rows[0]["RepaymentAmount"].ToString()!="")
				{
					model.RepaymentAmount=decimal.Parse(ds.Tables[0].Rows[0]["RepaymentAmount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ActualPayment"]!=null && ds.Tables[0].Rows[0]["ActualPayment"].ToString()!="")
				{
					model.ActualPayment=decimal.Parse(ds.Tables[0].Rows[0]["ActualPayment"].ToString());
				}
				if(ds.Tables[0].Rows[0]["BankAccountId"]!=null && ds.Tables[0].Rows[0]["BankAccountId"].ToString()!="")
				{
					model.BankAccountId=int.Parse(ds.Tables[0].Rows[0]["BankAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PersonnelAccount"]!=null && ds.Tables[0].Rows[0]["PersonnelAccount"].ToString()!="")
				{
					model.PersonnelAccount=int.Parse(ds.Tables[0].Rows[0]["PersonnelAccount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Sate"]!=null && ds.Tables[0].Rows[0]["Sate"].ToString()!="")
				{
					model.Sate=int.Parse(ds.Tables[0].Rows[0]["Sate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateAccountId"]!=null && ds.Tables[0].Rows[0]["CreateAccountId"].ToString()!="")
				{
					model.CreateAccountId=int.Parse(ds.Tables[0].Rows[0]["CreateAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateTime"]!=null && ds.Tables[0].Rows[0]["CreateTime"].ToString()!="")
				{
					model.CreateTime=DateTime.Parse(ds.Tables[0].Rows[0]["CreateTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TenantId"]!=null && ds.Tables[0].Rows[0]["TenantId"].ToString()!="")
				{
					model.TenantId=int.Parse(ds.Tables[0].Rows[0]["TenantId"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,PayDate,BasePay,AttendanceWages,CommunicationExpense,PersonalIncomeTax,HousingFund,ResidualAmount,AllWages,OvertimeWage,PercentageWages,OtherBenefits,SocialSecurity,DeductMoney,RepaymentAmount,ActualPayment,BankAccountId,PersonnelAccount,Sate,CreateAccountId,CreateTime,TenantId ");
			strSql.Append(" FROM PayrollControl ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,PayDate,BasePay,AttendanceWages,CommunicationExpense,PersonalIncomeTax,HousingFund,ResidualAmount,AllWages,OvertimeWage,PercentageWages,OtherBenefits,SocialSecurity,DeductMoney,RepaymentAmount,ActualPayment,BankAccountId,PersonnelAccount,Sate,CreateAccountId,CreateTime,TenantId ");
			strSql.Append(" FROM PayrollControl ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM PayrollControl ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from PayrollControl T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "PayrollControl";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

