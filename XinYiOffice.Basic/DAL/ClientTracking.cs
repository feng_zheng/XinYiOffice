﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XinYiOffice.Common;//Please add references
namespace XinYiOffice.DAL
{
	/// <summary>
	/// 数据访问类:ClientTracking
	/// </summary>
	public partial class ClientTracking
	{
		public ClientTracking()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Id", "ClientTracking"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from ClientTracking");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(XinYiOffice.Model.ClientTracking model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into ClientTracking(");
			strSql.Append("ClientHeat,SalesOpportunitiesId,SalesOpportunitiesSate,BackNotes,Sate,IsFollowUp,CloseNotes,ReturnAccountId,ActualTime,PlanTime,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId)");
			strSql.Append(" values (");
			strSql.Append("@ClientHeat,@SalesOpportunitiesId,@SalesOpportunitiesSate,@BackNotes,@Sate,@IsFollowUp,@CloseNotes,@ReturnAccountId,@ActualTime,@PlanTime,@CreateAccountId,@RefreshAccountId,@CreateTime,@RefreshTime,@TenantId)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@ClientHeat", SqlDbType.Int,4),
					new SqlParameter("@SalesOpportunitiesId", SqlDbType.Int,4),
					new SqlParameter("@SalesOpportunitiesSate", SqlDbType.Int,4),
					new SqlParameter("@BackNotes", SqlDbType.VarChar,4000),
					new SqlParameter("@Sate", SqlDbType.Int,4),
					new SqlParameter("@IsFollowUp", SqlDbType.Int,4),
					new SqlParameter("@CloseNotes", SqlDbType.VarChar,4000),
					new SqlParameter("@ReturnAccountId", SqlDbType.Int,4),
					new SqlParameter("@ActualTime", SqlDbType.DateTime),
					new SqlParameter("@PlanTime", SqlDbType.DateTime),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4)};
			parameters[0].Value = model.ClientHeat;
			parameters[1].Value = model.SalesOpportunitiesId;
			parameters[2].Value = model.SalesOpportunitiesSate;
			parameters[3].Value = model.BackNotes;
			parameters[4].Value = model.Sate;
			parameters[5].Value = model.IsFollowUp;
			parameters[6].Value = model.CloseNotes;
			parameters[7].Value = model.ReturnAccountId;
			parameters[8].Value = model.ActualTime;
			parameters[9].Value = model.PlanTime;
			parameters[10].Value = model.CreateAccountId;
			parameters[11].Value = model.RefreshAccountId;
			parameters[12].Value = model.CreateTime;
			parameters[13].Value = model.RefreshTime;
			parameters[14].Value = model.TenantId;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.ClientTracking model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update ClientTracking set ");
			strSql.Append("ClientHeat=@ClientHeat,");
			strSql.Append("SalesOpportunitiesId=@SalesOpportunitiesId,");
			strSql.Append("SalesOpportunitiesSate=@SalesOpportunitiesSate,");
			strSql.Append("BackNotes=@BackNotes,");
			strSql.Append("Sate=@Sate,");
			strSql.Append("IsFollowUp=@IsFollowUp,");
			strSql.Append("CloseNotes=@CloseNotes,");
			strSql.Append("ReturnAccountId=@ReturnAccountId,");
			strSql.Append("ActualTime=@ActualTime,");
			strSql.Append("PlanTime=@PlanTime,");
			strSql.Append("CreateAccountId=@CreateAccountId,");
			strSql.Append("RefreshAccountId=@RefreshAccountId,");
			strSql.Append("CreateTime=@CreateTime,");
			strSql.Append("RefreshTime=@RefreshTime,");
			strSql.Append("TenantId=@TenantId");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@ClientHeat", SqlDbType.Int,4),
					new SqlParameter("@SalesOpportunitiesId", SqlDbType.Int,4),
					new SqlParameter("@SalesOpportunitiesSate", SqlDbType.Int,4),
					new SqlParameter("@BackNotes", SqlDbType.VarChar,4000),
					new SqlParameter("@Sate", SqlDbType.Int,4),
					new SqlParameter("@IsFollowUp", SqlDbType.Int,4),
					new SqlParameter("@CloseNotes", SqlDbType.VarChar,4000),
					new SqlParameter("@ReturnAccountId", SqlDbType.Int,4),
					new SqlParameter("@ActualTime", SqlDbType.DateTime),
					new SqlParameter("@PlanTime", SqlDbType.DateTime),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.ClientHeat;
			parameters[1].Value = model.SalesOpportunitiesId;
			parameters[2].Value = model.SalesOpportunitiesSate;
			parameters[3].Value = model.BackNotes;
			parameters[4].Value = model.Sate;
			parameters[5].Value = model.IsFollowUp;
			parameters[6].Value = model.CloseNotes;
			parameters[7].Value = model.ReturnAccountId;
			parameters[8].Value = model.ActualTime;
			parameters[9].Value = model.PlanTime;
			parameters[10].Value = model.CreateAccountId;
			parameters[11].Value = model.RefreshAccountId;
			parameters[12].Value = model.CreateTime;
			parameters[13].Value = model.RefreshTime;
			parameters[14].Value = model.TenantId;
			parameters[15].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ClientTracking ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ClientTracking ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.ClientTracking GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,ClientHeat,SalesOpportunitiesId,SalesOpportunitiesSate,BackNotes,Sate,IsFollowUp,CloseNotes,ReturnAccountId,ActualTime,PlanTime,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId from ClientTracking ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			XinYiOffice.Model.ClientTracking model=new XinYiOffice.Model.ClientTracking();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Id"]!=null && ds.Tables[0].Rows[0]["Id"].ToString()!="")
				{
					model.Id=int.Parse(ds.Tables[0].Rows[0]["Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ClientHeat"]!=null && ds.Tables[0].Rows[0]["ClientHeat"].ToString()!="")
				{
					model.ClientHeat=int.Parse(ds.Tables[0].Rows[0]["ClientHeat"].ToString());
				}
				if(ds.Tables[0].Rows[0]["SalesOpportunitiesId"]!=null && ds.Tables[0].Rows[0]["SalesOpportunitiesId"].ToString()!="")
				{
					model.SalesOpportunitiesId=int.Parse(ds.Tables[0].Rows[0]["SalesOpportunitiesId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["SalesOpportunitiesSate"]!=null && ds.Tables[0].Rows[0]["SalesOpportunitiesSate"].ToString()!="")
				{
					model.SalesOpportunitiesSate=int.Parse(ds.Tables[0].Rows[0]["SalesOpportunitiesSate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["BackNotes"]!=null && ds.Tables[0].Rows[0]["BackNotes"].ToString()!="")
				{
					model.BackNotes=ds.Tables[0].Rows[0]["BackNotes"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Sate"]!=null && ds.Tables[0].Rows[0]["Sate"].ToString()!="")
				{
					model.Sate=int.Parse(ds.Tables[0].Rows[0]["Sate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["IsFollowUp"]!=null && ds.Tables[0].Rows[0]["IsFollowUp"].ToString()!="")
				{
					model.IsFollowUp=int.Parse(ds.Tables[0].Rows[0]["IsFollowUp"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CloseNotes"]!=null && ds.Tables[0].Rows[0]["CloseNotes"].ToString()!="")
				{
					model.CloseNotes=ds.Tables[0].Rows[0]["CloseNotes"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ReturnAccountId"]!=null && ds.Tables[0].Rows[0]["ReturnAccountId"].ToString()!="")
				{
					model.ReturnAccountId=int.Parse(ds.Tables[0].Rows[0]["ReturnAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ActualTime"]!=null && ds.Tables[0].Rows[0]["ActualTime"].ToString()!="")
				{
					model.ActualTime=DateTime.Parse(ds.Tables[0].Rows[0]["ActualTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PlanTime"]!=null && ds.Tables[0].Rows[0]["PlanTime"].ToString()!="")
				{
					model.PlanTime=DateTime.Parse(ds.Tables[0].Rows[0]["PlanTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateAccountId"]!=null && ds.Tables[0].Rows[0]["CreateAccountId"].ToString()!="")
				{
					model.CreateAccountId=int.Parse(ds.Tables[0].Rows[0]["CreateAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshAccountId"]!=null && ds.Tables[0].Rows[0]["RefreshAccountId"].ToString()!="")
				{
					model.RefreshAccountId=int.Parse(ds.Tables[0].Rows[0]["RefreshAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateTime"]!=null && ds.Tables[0].Rows[0]["CreateTime"].ToString()!="")
				{
					model.CreateTime=DateTime.Parse(ds.Tables[0].Rows[0]["CreateTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshTime"]!=null && ds.Tables[0].Rows[0]["RefreshTime"].ToString()!="")
				{
					model.RefreshTime=DateTime.Parse(ds.Tables[0].Rows[0]["RefreshTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TenantId"]!=null && ds.Tables[0].Rows[0]["TenantId"].ToString()!="")
				{
					model.TenantId=int.Parse(ds.Tables[0].Rows[0]["TenantId"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,ClientHeat,SalesOpportunitiesId,SalesOpportunitiesSate,BackNotes,Sate,IsFollowUp,CloseNotes,ReturnAccountId,ActualTime,PlanTime,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM ClientTracking ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,ClientHeat,SalesOpportunitiesId,SalesOpportunitiesSate,BackNotes,Sate,IsFollowUp,CloseNotes,ReturnAccountId,ActualTime,PlanTime,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM ClientTracking ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM ClientTracking ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from ClientTracking T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "ClientTracking";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

