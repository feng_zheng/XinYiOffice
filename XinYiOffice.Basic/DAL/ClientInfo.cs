﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XinYiOffice.Common;//Please add references
namespace XinYiOffice.DAL
{
	/// <summary>
	/// 数据访问类:ClientInfo
	/// </summary>
	public partial class ClientInfo
	{
		public ClientInfo()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Id", "ClientInfo"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from ClientInfo");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(XinYiOffice.Model.ClientInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into ClientInfo(");
			strSql.Append("ByAccountId,Name,Heat,ClientType,Industry,Source,MarketingPlanId,Tag,BankDeposit,TaxNo,IntNumber,Power,QualityRating,ValueAssessment,CustomerLevel,RelBetweenGrade,Seedtime,SupClientInfoId,BankAccount,MobilePhone,CountriesRegions,Phone,Fax,Email,ZipCode,Province,City,County,Address,CompanyProfile,Remarks,GeneralManager,BusinessEntity,BusinessLicence,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,LinkClientId,TenantId)");
			strSql.Append(" values (");
			strSql.Append("@ByAccountId,@Name,@Heat,@ClientType,@Industry,@Source,@MarketingPlanId,@Tag,@BankDeposit,@TaxNo,@IntNumber,@Power,@QualityRating,@ValueAssessment,@CustomerLevel,@RelBetweenGrade,@Seedtime,@SupClientInfoId,@BankAccount,@MobilePhone,@CountriesRegions,@Phone,@Fax,@Email,@ZipCode,@Province,@City,@County,@Address,@CompanyProfile,@Remarks,@GeneralManager,@BusinessEntity,@BusinessLicence,@CreateAccountId,@RefreshAccountId,@CreateTime,@RefreshTime,@LinkClientId,@TenantId)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@ByAccountId", SqlDbType.Int,4),
					new SqlParameter("@Name", SqlDbType.VarChar,4000),
					new SqlParameter("@Heat", SqlDbType.Int,4),
					new SqlParameter("@ClientType", SqlDbType.Int,4),
					new SqlParameter("@Industry", SqlDbType.Int,4),
					new SqlParameter("@Source", SqlDbType.Int,4),
					new SqlParameter("@MarketingPlanId", SqlDbType.Int,4),
					new SqlParameter("@Tag", SqlDbType.VarChar,4000),
					new SqlParameter("@BankDeposit", SqlDbType.VarChar,4000),
					new SqlParameter("@TaxNo", SqlDbType.VarChar,4000),
					new SqlParameter("@IntNumber", SqlDbType.VarChar,4000),
					new SqlParameter("@Power", SqlDbType.Int,4),
					new SqlParameter("@QualityRating", SqlDbType.Int,4),
					new SqlParameter("@ValueAssessment", SqlDbType.Int,4),
					new SqlParameter("@CustomerLevel", SqlDbType.Int,4),
					new SqlParameter("@RelBetweenGrade", SqlDbType.Int,4),
					new SqlParameter("@Seedtime", SqlDbType.Int,4),
					new SqlParameter("@SupClientInfoId", SqlDbType.Int,4),
					new SqlParameter("@BankAccount", SqlDbType.VarChar,4000),
					new SqlParameter("@MobilePhone", SqlDbType.VarChar,4000),
					new SqlParameter("@CountriesRegions", SqlDbType.Int,4),
					new SqlParameter("@Phone", SqlDbType.VarChar,4000),
					new SqlParameter("@Fax", SqlDbType.VarChar,4000),
					new SqlParameter("@Email", SqlDbType.VarChar,4000),
					new SqlParameter("@ZipCode", SqlDbType.VarChar,4000),
					new SqlParameter("@Province", SqlDbType.Int,4),
					new SqlParameter("@City", SqlDbType.Int,4),
					new SqlParameter("@County", SqlDbType.VarChar,4000),
					new SqlParameter("@Address", SqlDbType.VarChar,4000),
					new SqlParameter("@CompanyProfile", SqlDbType.VarChar,4000),
					new SqlParameter("@Remarks", SqlDbType.VarChar,4000),
					new SqlParameter("@GeneralManager", SqlDbType.VarChar,4000),
					new SqlParameter("@BusinessEntity", SqlDbType.VarChar,4000),
					new SqlParameter("@BusinessLicence", SqlDbType.VarChar,4000),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@LinkClientId", SqlDbType.Int,4),
					new SqlParameter("@TenantId", SqlDbType.Int,4)};
			parameters[0].Value = model.ByAccountId;
			parameters[1].Value = model.Name;
			parameters[2].Value = model.Heat;
			parameters[3].Value = model.ClientType;
			parameters[4].Value = model.Industry;
			parameters[5].Value = model.Source;
			parameters[6].Value = model.MarketingPlanId;
			parameters[7].Value = model.Tag;
			parameters[8].Value = model.BankDeposit;
			parameters[9].Value = model.TaxNo;
			parameters[10].Value = model.IntNumber;
			parameters[11].Value = model.Power;
			parameters[12].Value = model.QualityRating;
			parameters[13].Value = model.ValueAssessment;
			parameters[14].Value = model.CustomerLevel;
			parameters[15].Value = model.RelBetweenGrade;
			parameters[16].Value = model.Seedtime;
			parameters[17].Value = model.SupClientInfoId;
			parameters[18].Value = model.BankAccount;
			parameters[19].Value = model.MobilePhone;
			parameters[20].Value = model.CountriesRegions;
			parameters[21].Value = model.Phone;
			parameters[22].Value = model.Fax;
			parameters[23].Value = model.Email;
			parameters[24].Value = model.ZipCode;
			parameters[25].Value = model.Province;
			parameters[26].Value = model.City;
			parameters[27].Value = model.County;
			parameters[28].Value = model.Address;
			parameters[29].Value = model.CompanyProfile;
			parameters[30].Value = model.Remarks;
			parameters[31].Value = model.GeneralManager;
			parameters[32].Value = model.BusinessEntity;
			parameters[33].Value = model.BusinessLicence;
			parameters[34].Value = model.CreateAccountId;
			parameters[35].Value = model.RefreshAccountId;
			parameters[36].Value = model.CreateTime;
			parameters[37].Value = model.RefreshTime;
			parameters[38].Value = model.LinkClientId;
			parameters[39].Value = model.TenantId;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.ClientInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update ClientInfo set ");
			strSql.Append("ByAccountId=@ByAccountId,");
			strSql.Append("Name=@Name,");
			strSql.Append("Heat=@Heat,");
			strSql.Append("ClientType=@ClientType,");
			strSql.Append("Industry=@Industry,");
			strSql.Append("Source=@Source,");
			strSql.Append("MarketingPlanId=@MarketingPlanId,");
			strSql.Append("Tag=@Tag,");
			strSql.Append("BankDeposit=@BankDeposit,");
			strSql.Append("TaxNo=@TaxNo,");
			strSql.Append("IntNumber=@IntNumber,");
			strSql.Append("Power=@Power,");
			strSql.Append("QualityRating=@QualityRating,");
			strSql.Append("ValueAssessment=@ValueAssessment,");
			strSql.Append("CustomerLevel=@CustomerLevel,");
			strSql.Append("RelBetweenGrade=@RelBetweenGrade,");
			strSql.Append("Seedtime=@Seedtime,");
			strSql.Append("SupClientInfoId=@SupClientInfoId,");
			strSql.Append("BankAccount=@BankAccount,");
			strSql.Append("MobilePhone=@MobilePhone,");
			strSql.Append("CountriesRegions=@CountriesRegions,");
			strSql.Append("Phone=@Phone,");
			strSql.Append("Fax=@Fax,");
			strSql.Append("Email=@Email,");
			strSql.Append("ZipCode=@ZipCode,");
			strSql.Append("Province=@Province,");
			strSql.Append("City=@City,");
			strSql.Append("County=@County,");
			strSql.Append("Address=@Address,");
			strSql.Append("CompanyProfile=@CompanyProfile,");
			strSql.Append("Remarks=@Remarks,");
			strSql.Append("GeneralManager=@GeneralManager,");
			strSql.Append("BusinessEntity=@BusinessEntity,");
			strSql.Append("BusinessLicence=@BusinessLicence,");
			strSql.Append("CreateAccountId=@CreateAccountId,");
			strSql.Append("RefreshAccountId=@RefreshAccountId,");
			strSql.Append("CreateTime=@CreateTime,");
			strSql.Append("RefreshTime=@RefreshTime,");
			strSql.Append("LinkClientId=@LinkClientId,");
			strSql.Append("TenantId=@TenantId");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@ByAccountId", SqlDbType.Int,4),
					new SqlParameter("@Name", SqlDbType.VarChar,4000),
					new SqlParameter("@Heat", SqlDbType.Int,4),
					new SqlParameter("@ClientType", SqlDbType.Int,4),
					new SqlParameter("@Industry", SqlDbType.Int,4),
					new SqlParameter("@Source", SqlDbType.Int,4),
					new SqlParameter("@MarketingPlanId", SqlDbType.Int,4),
					new SqlParameter("@Tag", SqlDbType.VarChar,4000),
					new SqlParameter("@BankDeposit", SqlDbType.VarChar,4000),
					new SqlParameter("@TaxNo", SqlDbType.VarChar,4000),
					new SqlParameter("@IntNumber", SqlDbType.VarChar,4000),
					new SqlParameter("@Power", SqlDbType.Int,4),
					new SqlParameter("@QualityRating", SqlDbType.Int,4),
					new SqlParameter("@ValueAssessment", SqlDbType.Int,4),
					new SqlParameter("@CustomerLevel", SqlDbType.Int,4),
					new SqlParameter("@RelBetweenGrade", SqlDbType.Int,4),
					new SqlParameter("@Seedtime", SqlDbType.Int,4),
					new SqlParameter("@SupClientInfoId", SqlDbType.Int,4),
					new SqlParameter("@BankAccount", SqlDbType.VarChar,4000),
					new SqlParameter("@MobilePhone", SqlDbType.VarChar,4000),
					new SqlParameter("@CountriesRegions", SqlDbType.Int,4),
					new SqlParameter("@Phone", SqlDbType.VarChar,4000),
					new SqlParameter("@Fax", SqlDbType.VarChar,4000),
					new SqlParameter("@Email", SqlDbType.VarChar,4000),
					new SqlParameter("@ZipCode", SqlDbType.VarChar,4000),
					new SqlParameter("@Province", SqlDbType.Int,4),
					new SqlParameter("@City", SqlDbType.Int,4),
					new SqlParameter("@County", SqlDbType.VarChar,4000),
					new SqlParameter("@Address", SqlDbType.VarChar,4000),
					new SqlParameter("@CompanyProfile", SqlDbType.VarChar,4000),
					new SqlParameter("@Remarks", SqlDbType.VarChar,4000),
					new SqlParameter("@GeneralManager", SqlDbType.VarChar,4000),
					new SqlParameter("@BusinessEntity", SqlDbType.VarChar,4000),
					new SqlParameter("@BusinessLicence", SqlDbType.VarChar,4000),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@LinkClientId", SqlDbType.Int,4),
					new SqlParameter("@TenantId", SqlDbType.Int,4),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.ByAccountId;
			parameters[1].Value = model.Name;
			parameters[2].Value = model.Heat;
			parameters[3].Value = model.ClientType;
			parameters[4].Value = model.Industry;
			parameters[5].Value = model.Source;
			parameters[6].Value = model.MarketingPlanId;
			parameters[7].Value = model.Tag;
			parameters[8].Value = model.BankDeposit;
			parameters[9].Value = model.TaxNo;
			parameters[10].Value = model.IntNumber;
			parameters[11].Value = model.Power;
			parameters[12].Value = model.QualityRating;
			parameters[13].Value = model.ValueAssessment;
			parameters[14].Value = model.CustomerLevel;
			parameters[15].Value = model.RelBetweenGrade;
			parameters[16].Value = model.Seedtime;
			parameters[17].Value = model.SupClientInfoId;
			parameters[18].Value = model.BankAccount;
			parameters[19].Value = model.MobilePhone;
			parameters[20].Value = model.CountriesRegions;
			parameters[21].Value = model.Phone;
			parameters[22].Value = model.Fax;
			parameters[23].Value = model.Email;
			parameters[24].Value = model.ZipCode;
			parameters[25].Value = model.Province;
			parameters[26].Value = model.City;
			parameters[27].Value = model.County;
			parameters[28].Value = model.Address;
			parameters[29].Value = model.CompanyProfile;
			parameters[30].Value = model.Remarks;
			parameters[31].Value = model.GeneralManager;
			parameters[32].Value = model.BusinessEntity;
			parameters[33].Value = model.BusinessLicence;
			parameters[34].Value = model.CreateAccountId;
			parameters[35].Value = model.RefreshAccountId;
			parameters[36].Value = model.CreateTime;
			parameters[37].Value = model.RefreshTime;
			parameters[38].Value = model.LinkClientId;
			parameters[39].Value = model.TenantId;
			parameters[40].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ClientInfo ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ClientInfo ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.ClientInfo GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,ByAccountId,Name,Heat,ClientType,Industry,Source,MarketingPlanId,Tag,BankDeposit,TaxNo,IntNumber,Power,QualityRating,ValueAssessment,CustomerLevel,RelBetweenGrade,Seedtime,SupClientInfoId,BankAccount,MobilePhone,CountriesRegions,Phone,Fax,Email,ZipCode,Province,City,County,Address,CompanyProfile,Remarks,GeneralManager,BusinessEntity,BusinessLicence,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,LinkClientId,TenantId from ClientInfo ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			XinYiOffice.Model.ClientInfo model=new XinYiOffice.Model.ClientInfo();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Id"]!=null && ds.Tables[0].Rows[0]["Id"].ToString()!="")
				{
					model.Id=int.Parse(ds.Tables[0].Rows[0]["Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ByAccountId"]!=null && ds.Tables[0].Rows[0]["ByAccountId"].ToString()!="")
				{
					model.ByAccountId=int.Parse(ds.Tables[0].Rows[0]["ByAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Name"]!=null && ds.Tables[0].Rows[0]["Name"].ToString()!="")
				{
					model.Name=ds.Tables[0].Rows[0]["Name"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Heat"]!=null && ds.Tables[0].Rows[0]["Heat"].ToString()!="")
				{
					model.Heat=int.Parse(ds.Tables[0].Rows[0]["Heat"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ClientType"]!=null && ds.Tables[0].Rows[0]["ClientType"].ToString()!="")
				{
					model.ClientType=int.Parse(ds.Tables[0].Rows[0]["ClientType"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Industry"]!=null && ds.Tables[0].Rows[0]["Industry"].ToString()!="")
				{
					model.Industry=int.Parse(ds.Tables[0].Rows[0]["Industry"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Source"]!=null && ds.Tables[0].Rows[0]["Source"].ToString()!="")
				{
					model.Source=int.Parse(ds.Tables[0].Rows[0]["Source"].ToString());
				}
				if(ds.Tables[0].Rows[0]["MarketingPlanId"]!=null && ds.Tables[0].Rows[0]["MarketingPlanId"].ToString()!="")
				{
					model.MarketingPlanId=int.Parse(ds.Tables[0].Rows[0]["MarketingPlanId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Tag"]!=null && ds.Tables[0].Rows[0]["Tag"].ToString()!="")
				{
					model.Tag=ds.Tables[0].Rows[0]["Tag"].ToString();
				}
				if(ds.Tables[0].Rows[0]["BankDeposit"]!=null && ds.Tables[0].Rows[0]["BankDeposit"].ToString()!="")
				{
					model.BankDeposit=ds.Tables[0].Rows[0]["BankDeposit"].ToString();
				}
				if(ds.Tables[0].Rows[0]["TaxNo"]!=null && ds.Tables[0].Rows[0]["TaxNo"].ToString()!="")
				{
					model.TaxNo=ds.Tables[0].Rows[0]["TaxNo"].ToString();
				}
				if(ds.Tables[0].Rows[0]["IntNumber"]!=null && ds.Tables[0].Rows[0]["IntNumber"].ToString()!="")
				{
					model.IntNumber=ds.Tables[0].Rows[0]["IntNumber"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Power"]!=null && ds.Tables[0].Rows[0]["Power"].ToString()!="")
				{
					model.Power=int.Parse(ds.Tables[0].Rows[0]["Power"].ToString());
				}
				if(ds.Tables[0].Rows[0]["QualityRating"]!=null && ds.Tables[0].Rows[0]["QualityRating"].ToString()!="")
				{
					model.QualityRating=int.Parse(ds.Tables[0].Rows[0]["QualityRating"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ValueAssessment"]!=null && ds.Tables[0].Rows[0]["ValueAssessment"].ToString()!="")
				{
					model.ValueAssessment=int.Parse(ds.Tables[0].Rows[0]["ValueAssessment"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CustomerLevel"]!=null && ds.Tables[0].Rows[0]["CustomerLevel"].ToString()!="")
				{
					model.CustomerLevel=int.Parse(ds.Tables[0].Rows[0]["CustomerLevel"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RelBetweenGrade"]!=null && ds.Tables[0].Rows[0]["RelBetweenGrade"].ToString()!="")
				{
					model.RelBetweenGrade=int.Parse(ds.Tables[0].Rows[0]["RelBetweenGrade"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Seedtime"]!=null && ds.Tables[0].Rows[0]["Seedtime"].ToString()!="")
				{
					model.Seedtime=int.Parse(ds.Tables[0].Rows[0]["Seedtime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["SupClientInfoId"]!=null && ds.Tables[0].Rows[0]["SupClientInfoId"].ToString()!="")
				{
					model.SupClientInfoId=int.Parse(ds.Tables[0].Rows[0]["SupClientInfoId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["BankAccount"]!=null && ds.Tables[0].Rows[0]["BankAccount"].ToString()!="")
				{
					model.BankAccount=ds.Tables[0].Rows[0]["BankAccount"].ToString();
				}
				if(ds.Tables[0].Rows[0]["MobilePhone"]!=null && ds.Tables[0].Rows[0]["MobilePhone"].ToString()!="")
				{
					model.MobilePhone=ds.Tables[0].Rows[0]["MobilePhone"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CountriesRegions"]!=null && ds.Tables[0].Rows[0]["CountriesRegions"].ToString()!="")
				{
					model.CountriesRegions=int.Parse(ds.Tables[0].Rows[0]["CountriesRegions"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Phone"]!=null && ds.Tables[0].Rows[0]["Phone"].ToString()!="")
				{
					model.Phone=ds.Tables[0].Rows[0]["Phone"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Fax"]!=null && ds.Tables[0].Rows[0]["Fax"].ToString()!="")
				{
					model.Fax=ds.Tables[0].Rows[0]["Fax"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Email"]!=null && ds.Tables[0].Rows[0]["Email"].ToString()!="")
				{
					model.Email=ds.Tables[0].Rows[0]["Email"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ZipCode"]!=null && ds.Tables[0].Rows[0]["ZipCode"].ToString()!="")
				{
					model.ZipCode=ds.Tables[0].Rows[0]["ZipCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Province"]!=null && ds.Tables[0].Rows[0]["Province"].ToString()!="")
				{
					model.Province=int.Parse(ds.Tables[0].Rows[0]["Province"].ToString());
				}
				if(ds.Tables[0].Rows[0]["City"]!=null && ds.Tables[0].Rows[0]["City"].ToString()!="")
				{
					model.City=int.Parse(ds.Tables[0].Rows[0]["City"].ToString());
				}
				if(ds.Tables[0].Rows[0]["County"]!=null && ds.Tables[0].Rows[0]["County"].ToString()!="")
				{
					model.County=ds.Tables[0].Rows[0]["County"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Address"]!=null && ds.Tables[0].Rows[0]["Address"].ToString()!="")
				{
					model.Address=ds.Tables[0].Rows[0]["Address"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CompanyProfile"]!=null && ds.Tables[0].Rows[0]["CompanyProfile"].ToString()!="")
				{
					model.CompanyProfile=ds.Tables[0].Rows[0]["CompanyProfile"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Remarks"]!=null && ds.Tables[0].Rows[0]["Remarks"].ToString()!="")
				{
					model.Remarks=ds.Tables[0].Rows[0]["Remarks"].ToString();
				}
				if(ds.Tables[0].Rows[0]["GeneralManager"]!=null && ds.Tables[0].Rows[0]["GeneralManager"].ToString()!="")
				{
					model.GeneralManager=ds.Tables[0].Rows[0]["GeneralManager"].ToString();
				}
				if(ds.Tables[0].Rows[0]["BusinessEntity"]!=null && ds.Tables[0].Rows[0]["BusinessEntity"].ToString()!="")
				{
					model.BusinessEntity=ds.Tables[0].Rows[0]["BusinessEntity"].ToString();
				}
				if(ds.Tables[0].Rows[0]["BusinessLicence"]!=null && ds.Tables[0].Rows[0]["BusinessLicence"].ToString()!="")
				{
					model.BusinessLicence=ds.Tables[0].Rows[0]["BusinessLicence"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CreateAccountId"]!=null && ds.Tables[0].Rows[0]["CreateAccountId"].ToString()!="")
				{
					model.CreateAccountId=int.Parse(ds.Tables[0].Rows[0]["CreateAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshAccountId"]!=null && ds.Tables[0].Rows[0]["RefreshAccountId"].ToString()!="")
				{
					model.RefreshAccountId=int.Parse(ds.Tables[0].Rows[0]["RefreshAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateTime"]!=null && ds.Tables[0].Rows[0]["CreateTime"].ToString()!="")
				{
					model.CreateTime=DateTime.Parse(ds.Tables[0].Rows[0]["CreateTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshTime"]!=null && ds.Tables[0].Rows[0]["RefreshTime"].ToString()!="")
				{
					model.RefreshTime=DateTime.Parse(ds.Tables[0].Rows[0]["RefreshTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["LinkClientId"]!=null && ds.Tables[0].Rows[0]["LinkClientId"].ToString()!="")
				{
					model.LinkClientId=int.Parse(ds.Tables[0].Rows[0]["LinkClientId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TenantId"]!=null && ds.Tables[0].Rows[0]["TenantId"].ToString()!="")
				{
					model.TenantId=int.Parse(ds.Tables[0].Rows[0]["TenantId"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,ByAccountId,Name,Heat,ClientType,Industry,Source,MarketingPlanId,Tag,BankDeposit,TaxNo,IntNumber,Power,QualityRating,ValueAssessment,CustomerLevel,RelBetweenGrade,Seedtime,SupClientInfoId,BankAccount,MobilePhone,CountriesRegions,Phone,Fax,Email,ZipCode,Province,City,County,Address,CompanyProfile,Remarks,GeneralManager,BusinessEntity,BusinessLicence,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,LinkClientId,TenantId ");
			strSql.Append(" FROM ClientInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,ByAccountId,Name,Heat,ClientType,Industry,Source,MarketingPlanId,Tag,BankDeposit,TaxNo,IntNumber,Power,QualityRating,ValueAssessment,CustomerLevel,RelBetweenGrade,Seedtime,SupClientInfoId,BankAccount,MobilePhone,CountriesRegions,Phone,Fax,Email,ZipCode,Province,City,County,Address,CompanyProfile,Remarks,GeneralManager,BusinessEntity,BusinessLicence,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,LinkClientId,TenantId ");
			strSql.Append(" FROM ClientInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM ClientInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from ClientInfo T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "ClientInfo";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

