﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XinYiOffice.Common;//Please add references
namespace XinYiOffice.DAL
{
	/// <summary>
	/// 数据访问类:Programme
	/// </summary>
	public partial class Programme
	{
		public Programme()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Id", "Programme"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from Programme");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(XinYiOffice.Model.Programme model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into Programme(");
			strSql.Append("StartTime,EndTime,ScheduleType,ProjectInfoId,InstitutionId,Title,Locale,Note,IsRepeat,RepeatInterval,UpToDate,IsRemind,RemindDay,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId)");
			strSql.Append(" values (");
			strSql.Append("@StartTime,@EndTime,@ScheduleType,@ProjectInfoId,@InstitutionId,@Title,@Locale,@Note,@IsRepeat,@RepeatInterval,@UpToDate,@IsRemind,@RemindDay,@CreateAccountId,@RefreshAccountId,@CreateTime,@RefreshTime,@TenantId)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@StartTime", SqlDbType.DateTime),
					new SqlParameter("@EndTime", SqlDbType.DateTime),
					new SqlParameter("@ScheduleType", SqlDbType.Int,4),
					new SqlParameter("@ProjectInfoId", SqlDbType.Int,4),
					new SqlParameter("@InstitutionId", SqlDbType.Int,4),
					new SqlParameter("@Title", SqlDbType.VarChar,4000),
					new SqlParameter("@Locale", SqlDbType.VarChar,4000),
					new SqlParameter("@Note", SqlDbType.VarChar,4000),
					new SqlParameter("@IsRepeat", SqlDbType.Int,4),
					new SqlParameter("@RepeatInterval", SqlDbType.Int,4),
					new SqlParameter("@UpToDate", SqlDbType.DateTime),
					new SqlParameter("@IsRemind", SqlDbType.Int,4),
					new SqlParameter("@RemindDay", SqlDbType.Int,4),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4)};
			parameters[0].Value = model.StartTime;
			parameters[1].Value = model.EndTime;
			parameters[2].Value = model.ScheduleType;
			parameters[3].Value = model.ProjectInfoId;
			parameters[4].Value = model.InstitutionId;
			parameters[5].Value = model.Title;
			parameters[6].Value = model.Locale;
			parameters[7].Value = model.Note;
			parameters[8].Value = model.IsRepeat;
			parameters[9].Value = model.RepeatInterval;
			parameters[10].Value = model.UpToDate;
			parameters[11].Value = model.IsRemind;
			parameters[12].Value = model.RemindDay;
			parameters[13].Value = model.CreateAccountId;
			parameters[14].Value = model.RefreshAccountId;
			parameters[15].Value = model.CreateTime;
			parameters[16].Value = model.RefreshTime;
			parameters[17].Value = model.TenantId;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.Programme model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update Programme set ");
			strSql.Append("StartTime=@StartTime,");
			strSql.Append("EndTime=@EndTime,");
			strSql.Append("ScheduleType=@ScheduleType,");
			strSql.Append("ProjectInfoId=@ProjectInfoId,");
			strSql.Append("InstitutionId=@InstitutionId,");
			strSql.Append("Title=@Title,");
			strSql.Append("Locale=@Locale,");
			strSql.Append("Note=@Note,");
			strSql.Append("IsRepeat=@IsRepeat,");
			strSql.Append("RepeatInterval=@RepeatInterval,");
			strSql.Append("UpToDate=@UpToDate,");
			strSql.Append("IsRemind=@IsRemind,");
			strSql.Append("RemindDay=@RemindDay,");
			strSql.Append("CreateAccountId=@CreateAccountId,");
			strSql.Append("RefreshAccountId=@RefreshAccountId,");
			strSql.Append("CreateTime=@CreateTime,");
			strSql.Append("RefreshTime=@RefreshTime,");
			strSql.Append("TenantId=@TenantId");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@StartTime", SqlDbType.DateTime),
					new SqlParameter("@EndTime", SqlDbType.DateTime),
					new SqlParameter("@ScheduleType", SqlDbType.Int,4),
					new SqlParameter("@ProjectInfoId", SqlDbType.Int,4),
					new SqlParameter("@InstitutionId", SqlDbType.Int,4),
					new SqlParameter("@Title", SqlDbType.VarChar,4000),
					new SqlParameter("@Locale", SqlDbType.VarChar,4000),
					new SqlParameter("@Note", SqlDbType.VarChar,4000),
					new SqlParameter("@IsRepeat", SqlDbType.Int,4),
					new SqlParameter("@RepeatInterval", SqlDbType.Int,4),
					new SqlParameter("@UpToDate", SqlDbType.DateTime),
					new SqlParameter("@IsRemind", SqlDbType.Int,4),
					new SqlParameter("@RemindDay", SqlDbType.Int,4),
					new SqlParameter("@CreateAccountId", SqlDbType.Int,4),
					new SqlParameter("@RefreshAccountId", SqlDbType.Int,4),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@RefreshTime", SqlDbType.DateTime),
					new SqlParameter("@TenantId", SqlDbType.Int,4),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.StartTime;
			parameters[1].Value = model.EndTime;
			parameters[2].Value = model.ScheduleType;
			parameters[3].Value = model.ProjectInfoId;
			parameters[4].Value = model.InstitutionId;
			parameters[5].Value = model.Title;
			parameters[6].Value = model.Locale;
			parameters[7].Value = model.Note;
			parameters[8].Value = model.IsRepeat;
			parameters[9].Value = model.RepeatInterval;
			parameters[10].Value = model.UpToDate;
			parameters[11].Value = model.IsRemind;
			parameters[12].Value = model.RemindDay;
			parameters[13].Value = model.CreateAccountId;
			parameters[14].Value = model.RefreshAccountId;
			parameters[15].Value = model.CreateTime;
			parameters[16].Value = model.RefreshTime;
			parameters[17].Value = model.TenantId;
			parameters[18].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Programme ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Programme ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.Programme GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,StartTime,EndTime,ScheduleType,ProjectInfoId,InstitutionId,Title,Locale,Note,IsRepeat,RepeatInterval,UpToDate,IsRemind,RemindDay,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId from Programme ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			XinYiOffice.Model.Programme model=new XinYiOffice.Model.Programme();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Id"]!=null && ds.Tables[0].Rows[0]["Id"].ToString()!="")
				{
					model.Id=int.Parse(ds.Tables[0].Rows[0]["Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["StartTime"]!=null && ds.Tables[0].Rows[0]["StartTime"].ToString()!="")
				{
					model.StartTime=DateTime.Parse(ds.Tables[0].Rows[0]["StartTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["EndTime"]!=null && ds.Tables[0].Rows[0]["EndTime"].ToString()!="")
				{
					model.EndTime=DateTime.Parse(ds.Tables[0].Rows[0]["EndTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ScheduleType"]!=null && ds.Tables[0].Rows[0]["ScheduleType"].ToString()!="")
				{
					model.ScheduleType=int.Parse(ds.Tables[0].Rows[0]["ScheduleType"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ProjectInfoId"]!=null && ds.Tables[0].Rows[0]["ProjectInfoId"].ToString()!="")
				{
					model.ProjectInfoId=int.Parse(ds.Tables[0].Rows[0]["ProjectInfoId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["InstitutionId"]!=null && ds.Tables[0].Rows[0]["InstitutionId"].ToString()!="")
				{
					model.InstitutionId=int.Parse(ds.Tables[0].Rows[0]["InstitutionId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Title"]!=null && ds.Tables[0].Rows[0]["Title"].ToString()!="")
				{
					model.Title=ds.Tables[0].Rows[0]["Title"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Locale"]!=null && ds.Tables[0].Rows[0]["Locale"].ToString()!="")
				{
					model.Locale=ds.Tables[0].Rows[0]["Locale"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Note"]!=null && ds.Tables[0].Rows[0]["Note"].ToString()!="")
				{
					model.Note=ds.Tables[0].Rows[0]["Note"].ToString();
				}
				if(ds.Tables[0].Rows[0]["IsRepeat"]!=null && ds.Tables[0].Rows[0]["IsRepeat"].ToString()!="")
				{
					model.IsRepeat=int.Parse(ds.Tables[0].Rows[0]["IsRepeat"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RepeatInterval"]!=null && ds.Tables[0].Rows[0]["RepeatInterval"].ToString()!="")
				{
					model.RepeatInterval=int.Parse(ds.Tables[0].Rows[0]["RepeatInterval"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpToDate"]!=null && ds.Tables[0].Rows[0]["UpToDate"].ToString()!="")
				{
					model.UpToDate=DateTime.Parse(ds.Tables[0].Rows[0]["UpToDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["IsRemind"]!=null && ds.Tables[0].Rows[0]["IsRemind"].ToString()!="")
				{
					model.IsRemind=int.Parse(ds.Tables[0].Rows[0]["IsRemind"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RemindDay"]!=null && ds.Tables[0].Rows[0]["RemindDay"].ToString()!="")
				{
					model.RemindDay=int.Parse(ds.Tables[0].Rows[0]["RemindDay"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateAccountId"]!=null && ds.Tables[0].Rows[0]["CreateAccountId"].ToString()!="")
				{
					model.CreateAccountId=int.Parse(ds.Tables[0].Rows[0]["CreateAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshAccountId"]!=null && ds.Tables[0].Rows[0]["RefreshAccountId"].ToString()!="")
				{
					model.RefreshAccountId=int.Parse(ds.Tables[0].Rows[0]["RefreshAccountId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateTime"]!=null && ds.Tables[0].Rows[0]["CreateTime"].ToString()!="")
				{
					model.CreateTime=DateTime.Parse(ds.Tables[0].Rows[0]["CreateTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RefreshTime"]!=null && ds.Tables[0].Rows[0]["RefreshTime"].ToString()!="")
				{
					model.RefreshTime=DateTime.Parse(ds.Tables[0].Rows[0]["RefreshTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TenantId"]!=null && ds.Tables[0].Rows[0]["TenantId"].ToString()!="")
				{
					model.TenantId=int.Parse(ds.Tables[0].Rows[0]["TenantId"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,StartTime,EndTime,ScheduleType,ProjectInfoId,InstitutionId,Title,Locale,Note,IsRepeat,RepeatInterval,UpToDate,IsRemind,RemindDay,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM Programme ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,StartTime,EndTime,ScheduleType,ProjectInfoId,InstitutionId,Title,Locale,Note,IsRepeat,RepeatInterval,UpToDate,IsRemind,RemindDay,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId ");
			strSql.Append(" FROM Programme ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Programme ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from Programme T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Programme";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

