﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XinYiOffice.Common;//Please add references
namespace XinYiOffice.DAL
{
	/// <summary>
	/// 数据访问类:LocalTenants
	/// </summary>
	public partial class LocalTenants
	{
		public LocalTenants()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Id", "LocalTenants"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from LocalTenants");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(XinYiOffice.Model.LocalTenants model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into LocalTenants(");
			strSql.Append("TenantGuid,Name,NameEn,NameCn,Email,Remarks,State,CreateUserId,UpUserId,UpTime,CreateTime)");
			strSql.Append(" values (");
			strSql.Append("@TenantGuid,@Name,@NameEn,@NameCn,@Email,@Remarks,@State,@CreateUserId,@UpUserId,@UpTime,@CreateTime)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@TenantGuid", SqlDbType.VarChar,4000),
					new SqlParameter("@Name", SqlDbType.VarChar,4000),
					new SqlParameter("@NameEn", SqlDbType.VarChar,4000),
					new SqlParameter("@NameCn", SqlDbType.VarChar,4000),
					new SqlParameter("@Email", SqlDbType.VarChar,4000),
					new SqlParameter("@Remarks", SqlDbType.VarChar,4000),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@CreateUserId", SqlDbType.Int,4),
					new SqlParameter("@UpUserId", SqlDbType.Int,4),
					new SqlParameter("@UpTime", SqlDbType.DateTime),
					new SqlParameter("@CreateTime", SqlDbType.DateTime)};
			parameters[0].Value = model.TenantGuid;
			parameters[1].Value = model.Name;
			parameters[2].Value = model.NameEn;
			parameters[3].Value = model.NameCn;
			parameters[4].Value = model.Email;
			parameters[5].Value = model.Remarks;
			parameters[6].Value = model.State;
			parameters[7].Value = model.CreateUserId;
			parameters[8].Value = model.UpUserId;
			parameters[9].Value = model.UpTime;
			parameters[10].Value = model.CreateTime;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(XinYiOffice.Model.LocalTenants model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update LocalTenants set ");
			strSql.Append("TenantGuid=@TenantGuid,");
			strSql.Append("Name=@Name,");
			strSql.Append("NameEn=@NameEn,");
			strSql.Append("NameCn=@NameCn,");
			strSql.Append("Email=@Email,");
			strSql.Append("Remarks=@Remarks,");
			strSql.Append("State=@State,");
			strSql.Append("CreateUserId=@CreateUserId,");
			strSql.Append("UpUserId=@UpUserId,");
			strSql.Append("UpTime=@UpTime,");
			strSql.Append("CreateTime=@CreateTime");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@TenantGuid", SqlDbType.VarChar,4000),
					new SqlParameter("@Name", SqlDbType.VarChar,4000),
					new SqlParameter("@NameEn", SqlDbType.VarChar,4000),
					new SqlParameter("@NameCn", SqlDbType.VarChar,4000),
					new SqlParameter("@Email", SqlDbType.VarChar,4000),
					new SqlParameter("@Remarks", SqlDbType.VarChar,4000),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@CreateUserId", SqlDbType.Int,4),
					new SqlParameter("@UpUserId", SqlDbType.Int,4),
					new SqlParameter("@UpTime", SqlDbType.DateTime),
					new SqlParameter("@CreateTime", SqlDbType.DateTime),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.TenantGuid;
			parameters[1].Value = model.Name;
			parameters[2].Value = model.NameEn;
			parameters[3].Value = model.NameCn;
			parameters[4].Value = model.Email;
			parameters[5].Value = model.Remarks;
			parameters[6].Value = model.State;
			parameters[7].Value = model.CreateUserId;
			parameters[8].Value = model.UpUserId;
			parameters[9].Value = model.UpTime;
			parameters[10].Value = model.CreateTime;
			parameters[11].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from LocalTenants ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from LocalTenants ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public XinYiOffice.Model.LocalTenants GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,TenantGuid,Name,NameEn,NameCn,Email,Remarks,State,CreateUserId,UpUserId,UpTime,CreateTime from LocalTenants ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			XinYiOffice.Model.LocalTenants model=new XinYiOffice.Model.LocalTenants();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Id"]!=null && ds.Tables[0].Rows[0]["Id"].ToString()!="")
				{
					model.Id=int.Parse(ds.Tables[0].Rows[0]["Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TenantGuid"]!=null && ds.Tables[0].Rows[0]["TenantGuid"].ToString()!="")
				{
					model.TenantGuid=ds.Tables[0].Rows[0]["TenantGuid"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Name"]!=null && ds.Tables[0].Rows[0]["Name"].ToString()!="")
				{
					model.Name=ds.Tables[0].Rows[0]["Name"].ToString();
				}
				if(ds.Tables[0].Rows[0]["NameEn"]!=null && ds.Tables[0].Rows[0]["NameEn"].ToString()!="")
				{
					model.NameEn=ds.Tables[0].Rows[0]["NameEn"].ToString();
				}
				if(ds.Tables[0].Rows[0]["NameCn"]!=null && ds.Tables[0].Rows[0]["NameCn"].ToString()!="")
				{
					model.NameCn=ds.Tables[0].Rows[0]["NameCn"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Email"]!=null && ds.Tables[0].Rows[0]["Email"].ToString()!="")
				{
					model.Email=ds.Tables[0].Rows[0]["Email"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Remarks"]!=null && ds.Tables[0].Rows[0]["Remarks"].ToString()!="")
				{
					model.Remarks=ds.Tables[0].Rows[0]["Remarks"].ToString();
				}
				if(ds.Tables[0].Rows[0]["State"]!=null && ds.Tables[0].Rows[0]["State"].ToString()!="")
				{
					model.State=int.Parse(ds.Tables[0].Rows[0]["State"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateUserId"]!=null && ds.Tables[0].Rows[0]["CreateUserId"].ToString()!="")
				{
					model.CreateUserId=int.Parse(ds.Tables[0].Rows[0]["CreateUserId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpUserId"]!=null && ds.Tables[0].Rows[0]["UpUserId"].ToString()!="")
				{
					model.UpUserId=int.Parse(ds.Tables[0].Rows[0]["UpUserId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpTime"]!=null && ds.Tables[0].Rows[0]["UpTime"].ToString()!="")
				{
					model.UpTime=DateTime.Parse(ds.Tables[0].Rows[0]["UpTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreateTime"]!=null && ds.Tables[0].Rows[0]["CreateTime"].ToString()!="")
				{
					model.CreateTime=DateTime.Parse(ds.Tables[0].Rows[0]["CreateTime"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,TenantGuid,Name,NameEn,NameCn,Email,Remarks,State,CreateUserId,UpUserId,UpTime,CreateTime ");
			strSql.Append(" FROM LocalTenants ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,TenantGuid,Name,NameEn,NameCn,Email,Remarks,State,CreateUserId,UpUserId,UpTime,CreateTime ");
			strSql.Append(" FROM LocalTenants ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM LocalTenants ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from LocalTenants T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "LocalTenants";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

