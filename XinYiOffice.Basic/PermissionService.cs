﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using XinYiOffice.Model;
using XinYiOffice.Common;

namespace XinYiOffice.Basic
{
    public static class PermissionService
    {

        //通用判断方法

        /// <summary>
        /// 判断用户是否有此权限点
        /// </summary>
        /// <param name="_user">当前用户</param>
        /// <param name="permissionSign">权限签名</param>
        /// <returns></returns>
        public static bool IsValid(Accounts _user, string permissionSign)
        {

            //获取此用户的所属角色
            //获取此用户角色下的所有权限列
            //判断此签名是否存在于权限列表中,如果不存在返回false

            //List<AccountRoles> rurList = UserRoleService.FindAll(new Query("UsersId={0}", _user.UsersId.ToString()));
            List<AccountRoles> rurList = new BLL.AccountRoles().GetModelList(string.Format("AccountId={0}", _user.Id.ToString()));
            StringBuilder sb = new StringBuilder();

            if (rurList.Count > 0)
            {
                int i = 0;
                foreach (AccountRoles _rur in rurList)
                {
                    sb.Append(_rur.RoleId);
                    if (i != rurList.Count - 1)
                    {
                        sb.Append(",");
                    }
                    i++;
                }

                //查找所有有效角色

                string mysql = string.Format("select * from vRolePermissions where RoleId in ({0})", sb.ToString());
                DataSet ds = Common.DbHelperSQL.Query(mysql);

                if (ds != null && ds.Tables[0].Rows[0] != null)
                {
                    DataRow[] dr = ds.Tables[0].Select(string.Format("Sign='{0}'", permissionSign));
                    if (dr.Length > 0)
                    {
                        return true;
                    }
                }

            }
            return false;
        }

        /// <summary>
        /// 判断用户是否有此权限点
        /// </summary>
        /// <param name="_userid"></param>
        /// <param name="permissionSign"></param>
        /// <returns></returns>
        public static bool IsValid(DataTable dt, int _userid, string permissionSign)
        {
            if (dt!=null&&dt.Rows.Count>0)
            {
                DataRow[] dr = dt.Select(string.Format("Sign='{0}' and AccountId={1}", permissionSign, _userid));
                if (dr.Length > 0)
                {
                    return true;
                }
            }

            return false;

        }

        /// <summary>
        /// 获取所有权限节点
        /// </summary>
        /// <param name="TenantId"></param>
        /// <returns></returns>
        public static DataSet GetPermissionList(int TenantId)
        {
            string sql = string.Format("select * from vPermissions where TenantId={0}", TenantId);
            DataSet ds = null;
            try
            {
                ds = Common.DbHelperSQL.Query(sql);
            }
            catch
            {
 
            }
            return ds;
        }

        /// <summary>
        /// 获取所有权限节点
        /// </summary>
        /// <param name="TenantId"></param>
        /// <returns></returns>
        public static string GetPermissionListSql(int TenantId)
        {
            return string.Format("select * from vPermissions where TenantId={0} ", TenantId);

        }

        /// <summary>
        /// 根据判断角色是否有此权限签名
        /// </summary>
        /// <param name="role"></param>
        /// <param name="permissionSign"></param>
        /// <returns></returns>
        //public static bool IsValid(Role role, string permissionSign)
        //{
        //    Permission pe = PermissionService.Find(new Query("PermissionSign='{0}'", permissionSign));
        //    if (pe == null)
        //    {
        //        return false;
        //    }
        //    else
        //    {
        //        return IsValid(role, pe.PermissionId);
        //    }
        //}

        /// <summary>
        /// 判断角色List是否有此权限签名
        /// </summary>
        /// <param name="listRole"></param>
        /// <param name="permissionSign"></param>
        /// <returns></returns>
        //public static bool IsValid(List<Role> listRole, string permissionSign)
        //{
        //    bool ist = false;

        //    foreach (Role rol in listRole)
        //    {
        //        ist = IsValid(rol, permissionSign);
        //        if (ist)
        //        {
        //            break;
        //        }
        //    }
        //    return ist;
        //}


        //public static bool IsValid(Role role, int permissionId)
        //{
        //    RolePermission rrp = RolePermissionService.Find(new Query("RoleId={0} and PermissionId={1}", role.RoleId.ToString(), permissionId.ToString()));

        //    if (rrp != null)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

    }
}
