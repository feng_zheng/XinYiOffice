﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using XinYiOffice.Common;

namespace XinYiOffice.Basic
{
    public static class MailTool
    {

        public static void SendStmp(string to, string subjcet, string body, bool ishtml)
        {
            try
            {
                string frommail = "server@xinyioffice.com";
                string pas = "9890295";
                string server = "smtp.xinyioffice.com";
                string username = frommail.Split('@')[0].ToString();

                MailMessage mess = new MailMessage();
                mess.From = new MailAddress(frommail);
                //send E-mail Address要正确

                mess.Subject = subjcet;
                //邮件标题
                mess.IsBodyHtml = ishtml;
                mess.BodyEncoding = System.Text.Encoding.UTF8;
                //邮件编码
                mess.Body = body;
                //邮件正文
                SmtpClient client = new SmtpClient();
                client.Host = server;
                //SMTP服务器要正确，经测试，可以使用smtp.sina.com(.cn)    smtp.163.com   smtp.126.com
                client.Credentials = new System.Net.NetworkCredential(frommail, pas);
                client.Port = 25;
                //client.EnableSsl = true;
                //需要验证，用户名和密码要正确

                if (!string.IsNullOrEmpty(to))
                {
                    mess.To.Add(new MailAddress(to));
                    //接收邮件的邮箱要正确
                }
                else
                {
                    //mess.To.Add(new MailAddress(Basic.GetModelSiteInfo.EmailPost));
                }


                client.Send(mess);
                //Response.Write("邮件发送到" + mess.To.ToString() + "<br>");
            }
            catch (Exception ee)
            {
                //Response.Write(ee.Message + "<br>");
                EasyLog.WriteLog(ee);
            }
        }

    }
}
