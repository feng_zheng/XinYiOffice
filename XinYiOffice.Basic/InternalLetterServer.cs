﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using XinYiOffice.Common;

namespace XinYiOffice.Basic
{
    public class InternalLetterServer
    {
        /// <summary>
        /// 根据用户id获取我的内部信
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="topNum"></param>
        /// <returns></returns>
        public static DataTable GetMyInternalLetter(int accountId, int TenantId)
        {
            string mysql = string.Format("select top 10 * from vInternalReceiver where RecipientAccountId={0} and TenantId={1} and IsView=0 order by CreateTime desc ", accountId, TenantId);

            return Common.DbHelperSQL.Query(mysql).Tables[0];
        }

        public static DataTable GetvInternalReceiver(int insId)
        {
            StringBuilder strWhere = new StringBuilder();

            strWhere.AppendFormat(" where Id={0}", insId);
            return DbHelperSQL.Query("select * from vInternalReceiver" + strWhere.ToString() + " order by CreateTime desc").Tables[0];
        }

    }
}
