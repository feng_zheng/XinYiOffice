﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data;
using XinYiOffice.Common;

namespace XinYiOffice.Basic
{
    public static class CreateWebSiteMap
    {
        public static bool UpWebSiteMap()
        {
            System.Web.HttpContext hc=(System.Web.HttpContext)System.Web.HttpContext.Current as System.Web.HttpContext;
            bool t=false;
            int pid = 0;
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(hc.Server.MapPath("~/Web.sitemap"));
                SetMap(ref doc,pid);
                doc.Save(hc.Server.MapPath("~/Web.sitemap"));
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
            
             return t;
        }

        public static void SetMap(ref XmlDocument doc,int pid)
        {
            DataRow[] dr=AppDataCacheServer.MenuTreeList().Select(string.Format("MenuTreeId={0}",pid));

            foreach(DataRow _dr in dr)
            {
                string _title=SafeConvert.ToString(_dr["Name"]);
                string _url = SafeConvert.ToString(_dr["ChainedAddress"]);
                string _des = SafeConvert.ToString(_dr["Description"]);
                string _pid=SafeConvert.ToString(_dr["MenuTreeId"]);

                doc.CreateNode(XmlNodeType.Element, string.Format("siteMap"), "siteMap");


                //XmlNode node = doc.SelectSingleNode(string.Format("/siteMap/siteMapNode[@title='{0}']", _title));
                //if(node==null)
                //{
                //    doc.CreateNode(XmlNodeType.Element, "siteMapNode", string.Format("/siteMap/siteMapNode[@title='{0}']", _title));
                //    node = doc.SelectSingleNode(string.Format("/siteMap/siteMapNode[@title='{0}']", _title));
                //}

                //XmlElement xe = doc.CreateElement("SiteMapNode");
                //xe.SetAttribute("url", _url);
                //xe.SetAttribute("title", _title);
                //xe.SetAttribute("description",_des);

                //node.AppendChild((XmlNode)xe);
                //doc.AppendChild(node);

                SetMap(ref doc, SafeConvert.ToInt(_pid));

                //doc.AppendChild(node);
            }

            
            
        }
    }
}
