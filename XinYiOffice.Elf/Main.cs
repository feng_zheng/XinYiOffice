﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XinYiOffice.Elf
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();

            this.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width - this.Size.Width, Screen.PrimaryScreen.WorkingArea.Height - this.Size.Height);

            webBrowser1.Navigate("http://www.xinyioffice.com",false);
        }


        protected void Exitdy()
        {
            DialogResult dr = MessageBox.Show("您确定要退出小精灵么?", "退出", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                this.Visible = false;

                this.Dispose(); this.Close();
            }
            else if (dr == DialogResult.No)
            {

            } 
        }

        private void 退出ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Exitdy();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Showdy();
            
        }

        private void 关于ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new about().Show();
        }

        private void 最小化ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mindy();
        }

        private void 访问首页ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.xinyioffice.com/");
        }

        private void 设置ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Setting ss = new Setting();
            ss.Show();
        }

        private void 刷新ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.webBrowser1.Refresh();
        }

        private void 显示主界面ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Showdy();
            
        }

        private void 退出ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Exitdy();
        }

        protected void Showdy()
        {
            this.Show();
            WindowState = FormWindowState.Normal;

            this.Visible = true;
        }

        protected void Mindy()
        {
            WindowState = FormWindowState.Minimized;
            this.Hide();
            //this.Visible = false;
            //this.notifyIcon1.Visible = true;
            
        }

        private void Main_MinimumSizeChanged(object sender, EventArgs e)
        {
            //最小化
            Mindy();
            
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }


    }
}
