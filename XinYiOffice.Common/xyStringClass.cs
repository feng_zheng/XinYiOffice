﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XinYiOffice.Common
{
    public class xyStringClass
    {
        /// <summary>
        /// 将字符串累加
        /// </summary>
        /// <param name="str">字符串</param>
        /// <param name="num">累加数</param>
        /// <returns></returns>
        public static string StringAddByNum(string str,int num)
        {
            StringBuilder sb = new StringBuilder(str);
            for (int i = 1; i <=(num-1);i++ )
            {
                sb.Append(str);
            }
            return sb.ToString();
        }

        /// <summary>
        /// 源字符串是否存在目标字符串
        /// </summary>
        /// <param name="Enstr">源字符串</param>
        /// <param name="Destr">目标字符串</param>
        /// <returns></returns>
        public static bool StringIsExistIN(string Enstr, string Destr)
        {
            bool t = false;

            if (StringFindStringCount(Enstr, Destr)>=1)
            {
                t = true;
            }

            return t;
        }


        /// <summary>
        /// C#获得1个字符串在另一个字符串里出现的次数的巧妙方法
        /// 返回>1则存在重复的字符串，并返回值，如果小于等于1则不存在重复的字符串返回
        /// 查找某一字符串 在目标字符串里出现的次数   思想：首先把目标字符串赋给一个字符串，然后把赋值后的字符串 
        /// 把源目标字符串替换成空值，这样把源字符串的总长度减去赋值后的字符后的总长度便是目标字符串的倍数，
        /// 如果小于等于1，则不存在，如果>1则存在
        /// </summary>
        /// <param name="Enstr">源字符串</param>
        /// <param name="Destr">目标字符串</param>
        /// <returns></returns>
        public static int StringFindStringCount(string Enstr, string Destr)
        {
            int result = 0;

            if (Enstr == null || Enstr.ToString().Trim().Replace("'", "") == "")//当源字符串为空时
            {
                result = 0;
            }
            else
            {
                if (Destr == null || Destr.ToString().Trim().Replace("'", "") == "")//当目标字符串为空时
                {
                    result = 0;
                }
                else
                {
                    if (Enstr.Length < Destr.Length)//当源字符串长度小于目标字符串长度时
                    {
                        result = 0;
                    }
                    else
                    {
                        string str = Enstr;

                        str = str.Replace(Destr, "");

                        int count = (Enstr.Length - str.Length);

                        if (count > 0)
                        {
                            result = count / Destr.Length;//如果此值大于1，则说明存在重复字符串，否则不存在重复的字符串
                        }
                        else
                        {
                            result = 0;
                        }
                    }
                }
            }

            return result;
        }


        /// <summary>
        /// 字符串分割符
        /// </summary>
        public const string SEPARATORSTRING = "{xyc:page}";
        /// <summary>
        /// 取得字符的第几个字符，通过分割符分割的字符串,pos以1开始
        /// </summary>
        /// <param name="str">字符串</param>
        /// <param name="separator">分割符</param>
        /// <param name="pos">第几个</param>
        /// <returns>返回第几个字符串</returns>
        /// <example>string str = Split("rrrrddrew@@tqwewerewddccc", "@@", 1);</example>
        public static string Split(string str, string separator, int pos)
        {
            int Len = str.Length;
            int Count = str.Length - str.Replace(separator, "").Length;
            int BitCount = 0;
            string returnValue = "";
            string strValue = "";
            string InSeparator = "";
            if (Count == 0)
            {
                return str;
            }
            InSeparator = str.Substring(str.Length - separator.Length, separator.Length);
            if (InSeparator != separator)
            {
                str += separator;
            }

            for (int i = 0; i < Count; i++)
            {
                BitCount = str.IndexOf(separator, 0);
                strValue = str.Substring(0, BitCount);
                str = str.Replace(strValue + separator, "");
                if (i == pos - 1)
                {
                    returnValue = strValue;
                    break;
                }
            }
            return returnValue;
        }
        /// <summary>
        /// 取得字符的第几个字符，通过分割符分割的字符串,pos以1开始，以默认的字符串分割
        /// </summary>
        /// <param name="str">字符串</param>
        /// <param name="pos">第几个</param>
        /// <returns>返回第几个字符串</returns>
        /// <example>string str = Split("rrrrddrew@@tqwewerewddccc",1);</example>
        public static string Split(string str, int pos)
        {
            int Len = str.Length;
            int Count = str.Length - str.Replace(SEPARATORSTRING, "").Length;
            int BitCount = 0;
            string returnValue = "";
            string strValue = "";
            string InSeparator = "";
            if (Count == 0)
            {
                return str;
            }
            InSeparator = str.Substring(str.Length - SEPARATORSTRING.Length, SEPARATORSTRING.Length);
            if (InSeparator != SEPARATORSTRING)
            {
                str += SEPARATORSTRING;
            }

            for (int i = 0; i < Count; i++)
            {
                BitCount = str.IndexOf(SEPARATORSTRING, 0);
                strValue = str.Substring(0, BitCount);
                str = str.Replace(strValue + SEPARATORSTRING, "");
                if (i == pos - 1)
                {
                    returnValue = strValue;
                    break;
                }
            }
            return returnValue;
        }
        /// <summary>
        /// 取得分割后的字符串数据,数组以0开始
        /// </summary>
        /// <param name="str">字符串</param>
        /// <param name="separator">分割符</param>
        /// <returns>返回分割后的字符串数组</returns>
        /// <example>string str=Split("rrrrddrew@@tqwewerewddccc@@", "@@")[1].ToString()</example>
        public static string[] Split(string str, string separator)
        {
            int Len = str.Length;
            int Count = 0;
            int BitCount = 0;
            string strValue = "";
            string InSeparator = "";
            string[] returnValue = null;

            if (str.Length >= separator.Length)
            { 
                InSeparator = str.Substring(str.Length - separator.Length, separator.Length);
                if (InSeparator != separator)
                {
                    str += separator;
                }
                Count = (str.Length - str.Replace(separator, "").Length) / separator.Length;
                returnValue = new string[Count];
                for (int i = 0; i < Count; i++)
                {
                    BitCount = str.IndexOf(separator, 0);
                    strValue = str.Substring(0, BitCount);
                    str = str.Replace(strValue + separator, "");
                    //MessageBox.Show(BitCount.ToString() + ";" + strValue.ToString() + ";" + str);
                    returnValue[i] = strValue;

                }
            }

            return returnValue;
        }
        /// <summary>
        /// 取得分割后的字符串数据,数组以0开始,以默认的字符串分割
        /// </summary>
        /// <param name="str">字符串</param>
        /// <returns>返回分割后的字符串数组</returns>
        /// <example>string str=Split("rrrrddrew@@tqwewerewddccc@@")[1].ToString()</example>
        public static string[] Split(string str)
        {
            int Len = str.Length;
            int Count = 0;
            int BitCount = 0;
            string strValue = "";
            string InSeparator = "";
            string[] returnValue=null;

            if (str.Length >= SEPARATORSTRING.Length)
            {

                InSeparator = str.Substring(str.Length - SEPARATORSTRING.Length, SEPARATORSTRING.Length);
                if (InSeparator != SEPARATORSTRING)
                {
                    str += SEPARATORSTRING;
                }
                Count = (str.Length - str.Replace(SEPARATORSTRING, "").Length) / SEPARATORSTRING.Length;

                returnValue = new string[Count];
                for (int i = 0; i < Count; i++)
                {
                    BitCount = str.IndexOf(SEPARATORSTRING, 0);
                    strValue = str.Substring(0, BitCount);
                    str = str.Replace(strValue + SEPARATORSTRING, "");
                    //MessageBox.Show(BitCount.ToString() + ";" + strValue.ToString() + ";" + str);
                    returnValue[i] = strValue;

                }

            }


            return returnValue;
        } 




    }
}
