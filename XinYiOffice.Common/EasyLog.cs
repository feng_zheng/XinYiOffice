﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Web;
using System.Web.UI;

namespace XinYiOffice.Common
{

    /// <summary>
    /// 简单日志类,2011-10-28,Gaowenlong
    /// 2013-3-11 修改gwl
    /// </summary>
    public class EasyLog
    {
        public string ClientIP { get { return _clientIP; } set { _clientIP = value; } }
        public string URL { get { return _uRL; } set { _uRL = value; } }
        public string RequestForm { get { return _requestForm; } set { _requestForm = value; } }
        public string RequestQueryString { get { return _requestQueryString; } set { _requestQueryString = value; } }
        public string RequestFiles { get { return _requestFiles; } set { _requestFiles = value; } }
        public string HelpLink { get { return _helpLink; } set { _helpLink = value; } }
        public string Message { get { return _message; } set { _message = value; } }
        public string Source { get { return _source; } set { _source = value; } }
        public string StackTrace { get { return _stackTrace; } set { _stackTrace = value; } }
        public string TargetSite { get { return _targetSite; } set { _targetSite = value; } }
        public string CreateDate { get { return _createDate; } set { _createDate = value; } }

        private string _clientIP;
        private string _uRL;
        private string _requestForm;
        private string _requestQueryString;
        private string _requestFiles;
        private string _helpLink;
        private string _message;
        private string _source;
        private string _stackTrace;
        private string _targetSite;
        private string _createDate;

        public static string filepaht = string.Empty;

        /// <summary>
        /// 写入文件,方法1
        /// </summary>
        /// <param name="fPath">文件路径</param>
        /// <param name="input">写入内容</param>
        private static void WriteFile(string fPath, string fContent)
        {
            //创建文件
            using (FileStream fs = File.Create(fPath))
            {
                byte[] info = Encoding.UTF8.GetBytes(fContent);
                fs.Write(info, 0, info.Length);
            }
        }

        public static void WriteLog(string txt)
        {
            StringBuilder sbText = new StringBuilder();
            DateTime now = DateTime.Now;
            string _mytime = now.ToString("yyyy-MM-dd HH:mm:ss").ToString();
            string _myfilename = now.ToString("yyyy-MM-dd-HH-mm-ss").ToString();

            sbText.Append("------------------------开发者自行记录： \r\n ");
            sbText.Append("------------------------Time:" + _mytime + " \r\n ");
            sbText.Append("" + txt);

            WriteFile(filepaht + "/" + _myfilename + ".txt", sbText.ToString());
        }

        public static void WriteLog(System.Web.UI.Page page, string txt)
        {
            StringBuilder sbText = new StringBuilder();
            DateTime now = DateTime.Now;
            string _mytime = now.ToString("yyyy-MM-dd HH:mm:ss").ToString();
            string _myfilename = now.ToString("yyyy-MM-dd-HH-mm-ss").ToString();
            sbText.Append("------------------------开发者自行记录： \r\n ");
            sbText.Append("------------------------Time:" + _mytime + " \r\n ");
            sbText.Append("ClientIP:" + page.Request.UserHostAddress == null ? "" : page.Request.UserHostAddress + "\r\n ");
            sbText.Append("URL:" + page.Request.Url.OriginalString + " \r\n ");
            sbText.Append("Request.Form:" + (page.Request.Form.Count > 0 ? "True" : "False") + " \r\n ");
            sbText.Append("Request.QueryString:" + (page.Request.QueryString.Count > 0 ? "True" : "False") + " \r\n ");
            sbText.Append("Request.Files:" + (page.Request.Files.Count > 0 ? "True" : "False") + " \r\n");
            //sbText.Append("HelpLink:" + ex.HelpLink + " \r\n");
            //sbText.Append("Message:" + ex.Message + " \r\n ");
            //sbText.Append("Source:" + ex.Source + " \r\n ");
            //sbText.Append("StackTrace:" + ex.StackTrace + " \r\n ");
            //sbText.Append("TargetSite:" + ex.TargetSite.ToString() + " \r\n ");
            sbText.Append("CreateDate:" + (DateTime.Now.ToString()) + "\r\n");

            WriteFile(filepaht+ "/" + _myfilename + ".txt", sbText.ToString());
        }

        public static void WriteLog(Exception ex)
        {
            try
            {
                Page p = (Page)HttpContext.Current.CurrentHandler as Page;
                if (p != null)
                {
                    WriteLog(ex, p);
                }
                else
                {
                    WriteLog(ex.Message);
                }
            }
            catch
            {
            }
            finally
            {

            }

        }

        public static void WriteLog(Exception ex, System.Web.UI.Page page)
        {
            try
            {
                StringBuilder sbText = new StringBuilder();
                DateTime now = DateTime.Now;
                string _mytime = now.ToString("yyyy-MM-dd HH:mm:ss").ToString();
                string _myfilename = now.ToString("yyyy-MM-dd-HH-mm-ss").ToString();
                sbText.Append("------------------------Time:" + _mytime + " \r\n ");
                sbText.Append("ClientIP:" + page.Request.UserHostAddress == null ? "" : page.Request.UserHostAddress + "\r\n ");
                sbText.Append("URL:" + page.Request.Url.OriginalString + " \r\n ");
                sbText.Append("Request.Form:" + (page.Request.Form.Count > 0 ? "True" : "False") + " \r\n ");
                sbText.Append("Request.QueryString:" + (page.Request.QueryString.Count > 0 ? "True" : "False") + " \r\n ");
                sbText.Append("Request.Files:" + (page.Request.Files.Count > 0 ? "True" : "False") + " \r\n");
                sbText.Append("HelpLink:" + ex.HelpLink + " \r\n");
                sbText.Append("Message:" + ex.Message + " \r\n ");
                sbText.Append("Source:" + ex.Source + " \r\n ");
                sbText.Append("StackTrace:" + ex.StackTrace + " \r\n ");
                sbText.Append("TargetSite:" + ex.TargetSite.ToString() + " \r\n ");
                sbText.Append("CreateDate:" + (DateTime.Now.ToString()) + "\r\n");

                WriteFile(filepaht+ "/" + _myfilename + ".txt", sbText.ToString());

                // string _path=page.Server.MapPath(fileName);
                //File.SetAttributes(_path, FileAttributes.Directory);
                //File.WriteAllText(_path, sbText.ToString(), Encoding.UTF8);

            }
            catch
            {
            }
            finally
            {
            }


        }

        static EasyLog()
        {
            filepaht = "~/log/expretion_" + DateTime.Today.ToString("yyyyMMdd");
            string path = string.Empty;
            if (HttpContext.Current != null)
            {
                path = HttpContext.Current.Server.MapPath(filepaht);
            }
            else
            {
                path = System.Environment.CurrentDirectory + filepaht.TrimStart('~').Replace("/", @"\");
            }

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            filepaht = path;
        }
    }

}