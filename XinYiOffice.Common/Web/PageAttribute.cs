﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XinYiOffice.Common.Web
{
    /// <summary>
    /// 分页信息配置类
    /// </summary>
    [Serializable]
    public class PageAttribute
    {
        private string _page = string.Empty;// no为采用
        private string _page_linage = string.Empty;//分页时,每页行数
        private string _page_cur = string.Empty;//当前第几页
        private string _pageshowfl = string.Empty;
        private string _pageshowgo = string.Empty;

        private string _page_pars = string.Empty;//分页时,当前页地址参数 如 page|test
        private string _page_parsvalue = string.Empty;//参数对照值
        private string _pagedefaulturl = string.Empty;//默认分页,一般用于设置第一页,不需要参数如 art_1.html,一般形式如 art.html
        private string _page_url = string.Empty;//当前分页的地址如 art.aspx?page=$page$&aid=199
        private string _tablename = string.Empty;//表名
        private string _pagefildsort = string.Empty;//关键字段排序
        private string _where = string.Empty;//分页sql条件

        public string Page { get { return _page; } set { _page = value; } }
        public string PageLinage { get { return _page_linage; } set { _page_linage = value; } }
        public string PageShowFL { get { return _pageshowfl; } set { _pageshowfl = value; } }
        public string PageShowGo { get { return _pageshowgo; } set { _pageshowgo = value; } }
        public string PagCur { get { return _page_cur; } set { _page_cur = value; } }
        public string PagePars { get { return _page_pars; } set { _page_pars = value; } }
        public string PageParsValue { get { return _page_parsvalue; } set { _page_parsvalue = value; } }
        public string PageDefaultUrl { get { return _pagedefaulturl; } set { _pagedefaulturl = value; } }
        public string PageUrl { get { return _page_url; } set { _page_url = value; } }
        public string PageFildSort { get { return _pagefildsort; } set { _pagefildsort = value; } }
        public string TableName { get { return _tablename; } set { _tablename = value; } }
        public string Where { get { return _where; } set { _where = value; } }
        /// <summary>
        /// 分页信息设置类
        /// </summary>
        /// <param name="page">no为采用</param>
        /// <param name="page_linage">分页时,每页行数</param>
        /// <param name="page_cur">当前第几页</param>
        /// <param name="page_pars">分页时,当前页地址参数 如 page|test</param>
        /// <param name="page_parvalue">参数对照值</param>
        public PageAttribute(string page, string page_linage, string page_cur, string page_pars, string page_parvalue, string page_url)
        {
            _page = page;
            _page_linage = page_linage;
            _page_cur = page_cur;
            _page_pars = page_pars;
            _page_parsvalue = page_parvalue;
            _page_url = page_url;


        }

        public PageAttribute()
        {
            _page_linage = "";//默认为空
            _page_cur = "1";//默认当前第1页
            _where = "";
        }
    }
}
