﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XinYiOffice.Common
{
    public class PageDataSet
    {
        public PageDataSet()
        {

        }

        /// <summary>
        /// 分页获取数据列表[sql语句版本]
        /// </summary>
        /// <param name="mysql">sql</param>
        /// <param name="pageindex">当前页索引</param>
        /// <param name="pagesize">每页显示条目</param>
        /// <param name="_zs">总分页</param>
        /// <param name="_datacount">数据总数</param>
        /// <returns></returns>
        public DataTable GetListPageBySql(string mysql, int pageindex, int pagesize, ref int _zs, ref int _datacount)
        {
            DataSet ds = new DataSet();

            SqlConnection cn = new SqlConnection(DbHelperSQL.connectionString);
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            SqlCommand mycommand = new SqlCommand("UP_Global_GetPageWithCount", cn);
            mycommand.CommandType = CommandType.StoredProcedure;

            mycommand.Parameters.Add("@sql", SqlDbType.VarChar, 4000);
            mycommand.Parameters.Add("@pageindex", SqlDbType.Int, 255);
            mycommand.Parameters.Add("@pagesize", SqlDbType.Int);
            mycommand.Parameters.Add("@pagecount", SqlDbType.BigInt);


            mycommand.Parameters["@sql"].Value = mysql;
            mycommand.Parameters["@pageindex"].Value = pageindex;
            mycommand.Parameters["@pagesize"].Value = pagesize;
            mycommand.Parameters["@pagecount"].Direction = ParameterDirection.Output;


            mycommand.CommandTimeout = 0;
            mycommand.ExecuteNonQuery();
            SqlDataAdapter dap = new SqlDataAdapter();
            dap.SelectCommand = mycommand;
            if (ds != null)
            {
                dap.Fill(ds, "t");
            }

            if (mycommand.Parameters["@pagecount"] != null)
            {
                _datacount = int.Parse(mycommand.Parameters["@pagecount"].Value.ToString());
            }


            if (cn.State == ConnectionState.Open)
            {
                cn.Close();
            }

            double dDataCount = double.Parse(_datacount.ToString());
            double dPageSize = double.Parse(pagesize.ToString());

            _zs = int.Parse(Math.Ceiling(dDataCount / dPageSize).ToString());

            //DataSet nds = new DataSet();
            //nds.Tables.Add(ds.Tables["t1"].Copy());

            // return nds;

            return ds.Tables["t1"];
        }

    }
}
