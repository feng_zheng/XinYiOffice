﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace XinYiOffice.Common
{
    public static class IO
    {

        //判断路径是否存在
        public static bool DirectoryIsExists(string path)
        {
            string p = System.Web.HttpContext.Current.Server.MapPath(path);

            if (System.IO.Directory.Exists(p))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //判断文件是否存在
        public static bool FileIsExists(string path)
        {
            string p = System.Web.HttpContext.Current.Server.MapPath(path);

            if (System.IO.File.Exists(p))
            {
                return true;
            }
            else
            {
                return false;
            }
        
        }
    }
}
