﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Common.Web;
using XinYiOffice.Model;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web
{
    public partial class MyAccount : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                ShowInfo();
            }
            
        }

        private void ShowInfo()
        {
            XinYiOffice.BLL.Accounts bll = new XinYiOffice.BLL.Accounts();
            XinYiOffice.Model.Accounts model =CurrentAccount;
            this.hidAccountId.Value = model.Id.ToString();
            this.litAccountName.Text = model.AccountName;
         
            this.txtNiceName.Text = model.NiceName;

            this.txtEnName.Text = model.EnName;
          
            this.txtEmail.Text = model.Email;
            DropDownList_HeadPortrait.SelectedValue= model.HeadPortrait;

            Image1.ImageUrl = string.Format("/img/face/{0}.png", model.HeadPortrait);

            this.txtPhone.Text = model.Phone;
            
            int iAccountType = SafeConvert.ToInt(model.AccountType,0);
            switch (iAccountType)
            {
                case 1:
                    litAccountType.Text = "公司内部职员";
                    break;

                case 2:
                    litAccountType.Text = "客户";
                    break;
                case 3:
                    litAccountType.Text = "合作伙伴";
                    break;

                default:
                    break;
            }

        }


    

        protected void alinkSave_Click(object sender, EventArgs e)
        {
            int Id = SafeConvert.ToInt(this.hidAccountId.Value,0);
            string AccountName = this.litAccountName.Text;
            
            string NiceName = this.txtNiceName.Text;

            string EnName = this.txtEnName.Text;
            int TimeZone = SafeConvert.ToInt(this.txtTimeZone.Text, 1);
            string Email = this.txtEmail.Text;
            string HeadPortrait = DropDownList_HeadPortrait.SelectedValue;
            string Phone = this.txtPhone.Text;
            


            Accounts model = new BLL.Accounts().GetModel(Id);

            model.AccountName = AccountName;
            //model.AccountPassword = AccountPassword;
            //model.Sate = Sate;
            model.NiceName = NiceName;
            model.FullName = txtFull.Text;
            model.EnName = EnName;
            //model.TimeZone = TimeZone;
            model.Email = Email;
            model.HeadPortrait = HeadPortrait;
            model.Phone = Phone;
            //model.AccountType = AccountType;
            //model.KeyId = KeyId;
           // model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = CurrentAccountId;
            //model.CreateTime = CreateTime;
            model.RefreshTime = DateTime.Now;
            model.TenantId = CurrentTenantId;
            XinYiOffice.BLL.Accounts bll = new XinYiOffice.BLL.Accounts();
            bll.Update(model);
            xytools.web_alert_new_url("保存成功！");
        }


    }
}