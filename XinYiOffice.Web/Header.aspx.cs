﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Basic;
using System.Data;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web
{
    public partial class Header : BasicPage
    {
        BLL.MenuTree ment = new BLL.MenuTree();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitData();
            }
        }

        protected void InitData()
        {
            DataRow[] dr = AppDataCacheServer.MenuTreeList().Select("MenuTreeId=0 and IsShow=1");
            DataTable dt = AppDataCacheServer.MenuTreeList().Clone();
            
            foreach (DataRow _dr in dr)
            {
                string sign = string.Format("MENUTREE_{0}", _dr["Guid"]);

                if (ValidatePermission(sign))
                {
                    dt.ImportRow(_dr);
                }
            }

            repMent.DataSource = dt;
            repMent.DataBind();
        }


        public string GetAccountsHeadPortrait()
        {
            if (CurrentAccountId != 0)
            {
                return AccountsServer.GetAccountsHeadPortrait(CurrentAccount.HeadPortrait);
            }
            else
            {
                return "/img/face/2.png";
            }
        }

    }
}