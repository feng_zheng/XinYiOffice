﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Web.UI;
using XinYiOffice.Common;

namespace XinYiOffice.Web.Finance.ReimbursementExpenses
{
    public partial class Show : BasicPage
    {
        public string strid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(base.ValidatePermission("REIMBURSEMENTEXPENSES_MANAGE") && base.ValidatePermission("REIMBURSEMENTEXPENSES_MANAGE_LIST_VIEW")))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    strid = Request.Params["id"];
                    int Id = (Convert.ToInt32(strid));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            //XinYiOffice.BLL.ReimbursementExpenses bll = new XinYiOffice.BLL.ReimbursementExpenses();
            //XinYiOffice.Model.ReimbursementExpenses model = bll.GetModel(Id);

            DataSet ds = XinYiOffice.Common.DbHelperSQL.Query(string.Format("select * from vReimbursementExpenses where Id={0}",Id.ToString()));
            DataRow dr = ds.Tables[0].Rows[0];

            this.lblId.Text = SafeConvert.ToString(dr["Id"]);
            this.lblCostTilte.Text = SafeConvert.ToString(dr["CostTilte"]);
            this.lblCostCon.Text = SafeConvert.ToString(dr["CostCon"]);
            this.lblRemarks.Text = SafeConvert.ToString(dr["Remarks"]);
            this.lblApplicationDate.Text = SafeConvert.ToString(dr["ApplicationDate"]);
            this.lblAmount.Text = SafeConvert.ToString(dr["Amount"]);
            this.lblCostType.Text = SafeConvert.ToString(dr["CostTypeName"]);
            this.lblBillNumber.Text = SafeConvert.ToString(dr["BillNumber"]);
            this.lblApplicantName.Text = SafeConvert.ToString(dr["ApplicantName"]);
            this.lblUserAccountId.Text = SafeConvert.ToString(dr["UserAccountName"]);
            this.lblRelatedClient.Text = SafeConvert.ToString(dr["RelatedClient"]);
            this.lblRelatedProject.Text = SafeConvert.ToString(dr["RelatedProject"]);
            this.lblAuditOpinion.Text = SafeConvert.ToString(dr["AuditOpinion"]);
            this.lblState.Text = SafeConvert.ToString(dr["SateName"]);
            this.lblDealingPeopleAccountId.Text =SafeConvert.ToString(dr["DealingPeopleAccountName"]);
            this.lblCreateAccountId.Text = SafeConvert.ToString(dr["CreateAccountName"]);
            this.lblCreateTime.Text = SafeConvert.ToString(dr["CreateTime"]);

        }


    }
}
