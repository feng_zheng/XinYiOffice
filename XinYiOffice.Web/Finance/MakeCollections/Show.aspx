﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="XinYiOffice.Web.Finance.MakeCollections.Show" Title="显示页" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<form id="f2" name="f2" runat="server">
 <div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>收款单详情</h4></div>
 <table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		单据编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblDocumentNumber" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		单据描述
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblDocumentDescription" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		收款方式
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblMethodPayment" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		供应商
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblSupplierName" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		收款人
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblGatheringFullName" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		收款人帐号
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblGatheringAccountId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		收款日期
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblReceiptDate" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		约定收款日期
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblTargetDate" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		摘要
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblRemark" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		客户
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblClientName" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		收款金额
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblGatheringAmount" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		收款银行账户
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblGatheringBankAccountId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		状态：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblSate" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		创建账户：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCreateAccountId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		添加时间：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCreateTime" runat="server"></asp:Label>
	</td></tr>
</table>

</div>
            </form>





</asp:Content>

