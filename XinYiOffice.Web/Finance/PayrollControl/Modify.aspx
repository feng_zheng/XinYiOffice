﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="XinYiOffice.Web.Finance.PayrollControl.Modify" Title="修改页" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

 <form id="f2" name="f2" runat="server">
  <div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>修改工资单</h4></div>

   <table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:label id="lblId" runat="server"></asp:label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		工资年月日期
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox ID="txtPayDate" runat="server" Width="70px"  onfocus="setday(this)"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		基本工资
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtBasePay" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		考勤工资
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtAttendanceWages" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		通讯费
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtCommunicationExpense" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		个人所得税
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtPersonalIncomeTax" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		住房公积金
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtHousingFund" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		剩余还款金额
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtResidualAmount" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		全部工资
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtAllWages" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		加班工资
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtOvertimeWage" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		提成工资
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtPercentageWages" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		其他补助
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtOtherBenefits" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		社保
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtSocialSecurity" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		扣钱
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtDeductMoney" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		本次抵扣还款金额
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtRepaymentAmount" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		实际发放工资
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtActualPayment" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		支付账户
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtBankAccountId" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		人员帐号
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtPersonnelAccount" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		工资状态 ：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtSate" runat="server" Width="200px"></asp:TextBox>
	&nbsp;1-已发,2-未发 
        <asp:HiddenField ID="txtCreateTime" runat="server" />
        <asp:HiddenField ID="txtCreateAccountId" runat="server" />
	</td></tr>
	</table>

</div>

<div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
        <a href="javascript:history.go(-1);" class="cancel">取消</a>
    </div>

    </form>

</asp:Content>
