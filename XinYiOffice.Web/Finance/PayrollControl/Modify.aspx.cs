﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Finance.PayrollControl
{
    public partial class Modify : BasicPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(base.ValidatePermission("PAYROLLCONTROL_MANAGE") && base.ValidatePermission("PAYROLLCONTROL_MANAGE_LIST_EDIT")))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.PayrollControl bll = new XinYiOffice.BLL.PayrollControl();
            XinYiOffice.Model.PayrollControl model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.txtPayDate.Text = model.PayDate.ToString();
            this.txtBasePay.Text = model.BasePay.ToString();
            this.txtAttendanceWages.Text = model.AttendanceWages.ToString();
            this.txtCommunicationExpense.Text = model.CommunicationExpense.ToString();
            this.txtPersonalIncomeTax.Text = model.PersonalIncomeTax.ToString();
            this.txtHousingFund.Text = model.HousingFund.ToString();
            this.txtResidualAmount.Text = model.ResidualAmount.ToString();
            this.txtAllWages.Text = model.AllWages.ToString();
            this.txtOvertimeWage.Text = model.OvertimeWage.ToString();
            this.txtPercentageWages.Text = model.PercentageWages.ToString();
            this.txtOtherBenefits.Text = model.OtherBenefits.ToString();
            this.txtSocialSecurity.Text = model.SocialSecurity.ToString();
            this.txtDeductMoney.Text = model.DeductMoney.ToString();
            this.txtRepaymentAmount.Text = model.RepaymentAmount.ToString();
            this.txtActualPayment.Text = model.ActualPayment.ToString();
            this.txtBankAccountId.Text = model.BankAccountId.ToString();
            this.txtPersonnelAccount.Text = model.PersonnelAccount.ToString();
            this.txtSate.Text = model.Sate.ToString();
            this.txtCreateAccountId.Value = model.CreateAccountId.ToString();
            this.txtCreateTime.Value = model.CreateTime.ToString();

        }



        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = int.Parse(this.lblId.Text);
            DateTime PayDate = DateTime.Parse(this.txtPayDate.Text);
            decimal BasePay = decimal.Parse(this.txtBasePay.Text);
            decimal AttendanceWages = decimal.Parse(this.txtAttendanceWages.Text);
            decimal CommunicationExpense = decimal.Parse(this.txtCommunicationExpense.Text);
            decimal PersonalIncomeTax = decimal.Parse(this.txtPersonalIncomeTax.Text);
            decimal HousingFund = decimal.Parse(this.txtHousingFund.Text);
            decimal ResidualAmount = decimal.Parse(this.txtResidualAmount.Text);
            decimal AllWages = decimal.Parse(this.txtAllWages.Text);
            decimal OvertimeWage = decimal.Parse(this.txtOvertimeWage.Text);
            decimal PercentageWages = decimal.Parse(this.txtPercentageWages.Text);
            decimal OtherBenefits = decimal.Parse(this.txtOtherBenefits.Text);
            decimal SocialSecurity = decimal.Parse(this.txtSocialSecurity.Text);
            decimal DeductMoney = decimal.Parse(this.txtDeductMoney.Text);
            decimal RepaymentAmount = decimal.Parse(this.txtRepaymentAmount.Text);
            decimal ActualPayment = decimal.Parse(this.txtActualPayment.Text);
            int BankAccountId = int.Parse(this.txtBankAccountId.Text);
            int PersonnelAccount = int.Parse(this.txtPersonnelAccount.Text);
            int Sate = int.Parse(this.txtSate.Text);
            int CreateAccountId = int.Parse(this.txtCreateAccountId.Value);
            DateTime CreateTime = SafeConvert.ToDateTime(txtCreateTime.Value);


            XinYiOffice.Model.PayrollControl model = new XinYiOffice.Model.PayrollControl();
            model.Id = Id;
            model.PayDate = PayDate;
            model.BasePay = BasePay;
            model.AttendanceWages = AttendanceWages;
            model.CommunicationExpense = CommunicationExpense;
            model.PersonalIncomeTax = PersonalIncomeTax;
            model.HousingFund = HousingFund;
            model.ResidualAmount = ResidualAmount;
            model.AllWages = AllWages;
            model.OvertimeWage = OvertimeWage;
            model.PercentageWages = PercentageWages;
            model.OtherBenefits = OtherBenefits;
            model.SocialSecurity = SocialSecurity;
            model.DeductMoney = DeductMoney;
            model.RepaymentAmount = RepaymentAmount;
            model.ActualPayment = ActualPayment;
            model.BankAccountId = BankAccountId;
            model.PersonnelAccount = PersonnelAccount;
            model.Sate = Sate;
            model.CreateAccountId = CreateAccountId;
            model.CreateTime = CreateTime;
            model.TenantId = CurrentTenantId;
            XinYiOffice.BLL.PayrollControl bll = new XinYiOffice.BLL.PayrollControl();
            bll.Update(model);
            xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
