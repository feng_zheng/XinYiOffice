﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="XinYiOffice.Web.BankAccount.Modify" Title="修改页" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
  <script src="/js/chosen/chosen.jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="/js/chosen/chosen.css" />

<script>
    $(function () {

        var sel_ins = $("#<%=DropDownList_InstitutionId.ClientID%>");
        var sel_dep = $("#<%=DropDownList_DepartmentId.ClientID%>");
        var hid_dep = $("#<%=HiddenField_DepartmentId.ClientID%>");

        //机构选择事件
        sel_ins.change(function () {
            sel_dep.html("");
            sel_dep.append("<option value='0'>请选择部门</option>");
            //left_list_p.html('');

            var insid = sel_ins.val();
            $.getJSON("/Call/Ajax.aspx?action=getdepartmentbyins&insid=" + insid + "&sj=" + Math.random(), function (json) {

                $.each(json, function (i) {
                    sel_dep.append("<option value='" + json[i].value + "'>" + json[i].label + "</option>");
                });
            });
        });


        //部门事件
        sel_dep.change(function () {

            hid_dep.val(sel_dep.val());

        });



        //绑定下拉样式
        window.setTimeout(function () {
            $(".chzn-select").chosen({ width: "220px" });
        }, 100);

    })
</script>
</asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form id="f2" name="f2" runat="server">

 <div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>修改银行账户</h4></div>
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:label id="lblId" runat="server"></asp:label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		开户行信息 ：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtBankInfo" runat="server" Width="600px" Height="80px" 
            TextMode="MultiLine"></asp:TextBox>
	&nbsp;银行名称及地址
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		户名
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtBankAccountName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		银行帐号
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtBankAccountNumber" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		联系手机号：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtBankAccountTel" runat="server" Width="200px"></asp:TextBox>
	&nbsp;用于接收银行或网银
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		现有金额
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtCashIn" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		账户状态：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList1_Sate" runat="server">
        </asp:DropDownList>
	&nbsp;1-正常,2-冻结
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		开户时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox ID="txtOpeningTime" runat="server" Width="200px"   class="Wdate"  onClick="WdatePicker()" ></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		所属机构
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_InstitutionId" runat="server">
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		所属部门
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_DepartmentId" runat="server">
        </asp:DropDownList>
        <asp:HiddenField ID="HiddenField_DepartmentId" runat="server" />
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		入库时金额：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtStorageAmount" runat="server" Width="200px"></asp:TextBox>
	&nbsp;新建后此字段值不做改变
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		帐号持有人
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_BearerAccountId" runat="server" CssClass="chzn-select">
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		帐号类型：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList1_AccountType" runat="server">
        </asp:DropDownList>
	&nbsp;1-对公,2-个人账
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		帐号用途：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_AccountPurpose" runat="server">
        </asp:DropDownList>
	&nbsp;1-对外贸易,2-工 
        <asp:HiddenField ID="txtCreateTime" runat="server" />
        <asp:HiddenField ID="txtCreateAccountId" runat="server" />
	</td></tr>
	</table>

</div>

<div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
         <a href="javascript:history.go(-1);" class="cancel">取消</a>
    </div>
    </form>

</asp:Content>

 