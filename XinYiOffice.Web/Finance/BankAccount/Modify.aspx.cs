﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.BankAccount
{
    public partial class Modify : BasicPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(base.ValidatePermission("BANKACCOUNT_MANAGE") && base.ValidatePermission("BANKACCOUNT_MANAGE_LIST_EDIT")))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.BankAccount bll = new XinYiOffice.BLL.BankAccount();
            XinYiOffice.Model.BankAccount model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.txtBankInfo.Text = model.BankInfo;
            this.txtBankAccountName.Text = model.BankAccountName;
            this.txtBankAccountNumber.Text = model.BankAccountNumber;
            this.txtBankAccountTel.Text = model.BankAccountTel;
            this.txtCashIn.Text = model.CashIn.ToString();
            //this.txtSate.Text = model.Sate.ToString();

            this.txtOpeningTime.Text = model.OpeningTime.ToString();
            //this.txtInstitutionId.Text = model.InstitutionId.ToString();
            //this.txtDepartmentId.Text = model.DepartmentId.ToString();

            this.txtStorageAmount.Text = model.StorageAmount.ToString();

            //this.txtBearerAccountId.Text = model.BearerAccountId.ToString();
            //this.txtAccountType.Text = model.AccountType.ToString();
            //this.txtAccountPurpose.Text = model.AccountPurpose.ToString();

            this.txtCreateAccountId.Value = model.CreateAccountId.ToString();
            this.txtCreateTime.Value = model.CreateTime.ToString();

            InitData();

            DropDownList1_Sate.SelectedValue = model.Sate.ToString();
            DropDownList_InstitutionId.SelectedValue = model.InstitutionId.ToString();

            DropDownList_DepartmentId.DataSource = DepartmentServer.GetDepartmentList(string.Format("InstitutionId={0}",model.InstitutionId),CurrentTenantId);

            DropDownList_DepartmentId.DataTextField = "DepartmentName";
            DropDownList_DepartmentId.DataValueField = "Id";
            DropDownList_DepartmentId.DataBind();

            DropDownList_DepartmentId.SelectedValue = model.DepartmentId.ToString();
            HiddenField_DepartmentId.Value = model.DepartmentId.ToString();

        }

        protected void InitData()
        {
            //机构信息
            DropDownList_InstitutionId.DataSource = new BLL.Institution().GetList(string.Format("1=1 and TenantId={0} ",CurrentTenantId));
            DropDownList_InstitutionId.DataTextField = "OrganizationName";
            DropDownList_InstitutionId.DataValueField = "Id";
            DropDownList_InstitutionId.DataBind();
            DropDownList_InstitutionId.Items.Insert(0, new ListItem("请选择机构", "0"));

            //帐号持有人
            DataTable dtAccountList = AccountsServer.GetAccountAllList(CurrentTenantId);
            DropDownList_BearerAccountId.DataSource = dtAccountList;
            DropDownList_BearerAccountId.DataTextField = "FullName";
            DropDownList_BearerAccountId.DataValueField = "Id";
            DropDownList_BearerAccountId.DataBind();


            SetDropDownList_BankAccount("Sate", ref DropDownList1_Sate);
            SetDropDownList_BankAccount("AccountType", ref DropDownList1_AccountType);
            SetDropDownList_BankAccount("AccountPurpose", ref DropDownList_AccountPurpose);
        }

        protected void SetDropDownList_BankAccount(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.BankAccountDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = int.Parse(this.lblId.Text);
            string BankInfo = this.txtBankInfo.Text;
            string BankAccountName = this.txtBankAccountName.Text;
            string BankAccountNumber = this.txtBankAccountNumber.Text;
            string BankAccountTel = this.txtBankAccountTel.Text;
            decimal CashIn = decimal.Parse(this.txtCashIn.Text);
            int Sate = SafeConvert.ToInt(DropDownList1_Sate.SelectedValue);

            DateTime OpeningTime = DateTime.Parse(this.txtOpeningTime.Text);
            int InstitutionId = SafeConvert.ToInt(DropDownList_InstitutionId.SelectedValue);
            int DepartmentId = SafeConvert.ToInt(HiddenField_DepartmentId.Value);
            decimal StorageAmount = decimal.Parse(this.txtStorageAmount.Text);
            int BearerAccountId = SafeConvert.ToInt(DropDownList_BearerAccountId.SelectedValue);
            int AccountType = SafeConvert.ToInt(DropDownList1_AccountType.SelectedValue);
            int AccountPurpose = SafeConvert.ToInt(DropDownList_AccountPurpose.SelectedValue);
            int CreateAccountId = SafeConvert.ToInt(this.txtCreateAccountId.Value);
            DateTime CreateTime = SafeConvert.ToDateTime(txtCreateTime.Value);


            XinYiOffice.Model.BankAccount model = new XinYiOffice.Model.BankAccount();
            model.Id = Id;
            model.BankInfo = BankInfo;
            model.BankAccountName = BankAccountName;
            model.BankAccountNumber = BankAccountNumber;
            model.BankAccountTel = BankAccountTel;
            model.CashIn = CashIn;
            model.Sate = Sate;
            model.OpeningTime = OpeningTime;
            model.InstitutionId = InstitutionId;
            model.DepartmentId = DepartmentId;
            model.StorageAmount = StorageAmount;
            model.BearerAccountId = BearerAccountId;
            model.AccountType = AccountType;
            model.AccountPurpose = AccountPurpose;
            model.CreateAccountId = CreateAccountId;
            model.CreateTime = CreateTime;
            model.TenantId = CurrentTenantId;
            XinYiOffice.BLL.BankAccount bll = new XinYiOffice.BLL.BankAccount();
            bll.Update(model);
            xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
