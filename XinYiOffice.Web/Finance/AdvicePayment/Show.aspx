﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="XinYiOffice.Web.Finance.AdvicePayment.Show" Title="显示页" %>

<asp:Content ContentPlaceHolderID="head" runat="server">

</asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<form id="f2" name="f2" runat="server">

 <div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>付款详情</h4></div>


<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		单据编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblDocumentNumber" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		单据描述
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblDocumentDescription" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		付款方式 1-现金,2-银行转
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblMethodPayment" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		供应商
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblSupplierName" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		客户名称 客户可能也是付款人
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblClientName" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		付款人
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPayeeFullName" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		付款人帐号 没有则为0
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPayeeAccountId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		实际付款日期
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPaymentDate" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		预定付款日期
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblTargetDate" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		摘要
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblRemark" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		付款金额
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPaymentAmount" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		付款银行账户
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPaymentBankAccountId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		状态 0-应付,1-已付
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblSate" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		创建账户
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCreateAccountId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		添加时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCreateTime" runat="server"></asp:Label>
	</td></tr>
</table>

</div>
            </form>





</asp:Content>
