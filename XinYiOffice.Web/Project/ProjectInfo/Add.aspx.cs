﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Project.ProjectInfo
{
    public partial class Add : BasicPage
    {
        public DataTable dtProjectList;
        public int lay;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("PROJECTINFO_LIST_ADD"))
            {
                base.NoPermissionPage();
            }

            if(!IsPostBack)
            {
                InitData();
            }
        }

        #region 项目下拉列表
        protected void SetDropDownListProject(int pid, ref DropDownList ddl)
        {
            DataRow[] dr = dtProjectList.Select(string.Format("ParentProjectId={0}", pid));
            int Id = 0;
            int ProjectId = 0;


            foreach (DataRow _dr in dr)
            {
                Id = SafeConvert.ToInt(_dr["Id"], 1);
                ProjectId = Id;

                lay++;

                string ProjectTitle = Nex(lay, ProjectId, pid, SafeConvert.ToString(_dr["Title"]));
                ddl.Items.Add(new ListItem(ProjectTitle, ProjectId.ToString()));

                SetDropDownListProject(Id, ref ddl);

                lay--;
            }
        }

        public string Nex(int x, int bid, int pid, string cname)
        {
            StringBuilder s = new StringBuilder();

            string kg = HttpUtility.HtmlDecode("&nbsp;");

            char nbsp = (char)0xA0;


            for (int i = 0; i < x; i++)
            {
                kg += kg;

                nbsp += nbsp;
            }

            if (pid == 0)
            {
                s.Append("" + cname);
            }
            else
            {
                if (!GetCustomInExist(bid))
                {
                    //不存在子级
                    s.Append(kg + "├" + cname);
                }
                else
                {//存在子级
                    s.Append(kg + "└" + cname);
                }
            }

            return s.ToString();
        }

        protected bool GetCustomInExist(int pid)
        {
            DataRow[] dr = dtProjectList.Select(string.Format("Id={0}", pid));

            if (dr.Length > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        } 
        #endregion


        protected void InitData()
        {
            DateTime dtNow = DateTime.Now;
            ddl_Year.SelectedValue = SafeConvert.ToString(dtNow.Year);
            ddl_month.SelectedValue = SafeConvert.ToString(dtNow.Month);

            //绑定项目树形
            DataSet ds=new BLL.ProjectInfo().GetList(string.Format("DATEPART(year,CreateTime)='{0}' and DATEPART(month,CreateTime)='{1}' and TenantId={2} ", dtNow.Year, dtNow.Month,CurrentTenantId));
            if(ds!=null&&ds.Tables.Count>0)
            {
                dtProjectList = ds.Tables[0];
                SetDropDownListProject(0, ref DropDownList_ParentProjectId);
            }

            ddl_ProjectTypeId.DataSource = new BLL.ProjectTypes().GetList(string.Format(" TenantId={0} ",CurrentTenantId));
            ddl_ProjectTypeId.DataTextField = "TypeName";
            ddl_ProjectTypeId.DataValueField = "Id";
            ddl_ProjectTypeId.DataBind();

            SetDropDownList("State", ref DropDownList_State);
            SetDropDownList("ProjectPropertie", ref DropDownList_ProjectPropertie);

            txtProjectManagerAccountId.Text = CurrentAccountTrueName;
            TextBox_OrganiserAccountId.Text = CurrentAccountTrueName;
            TextBox_ExecutorAccountId.Text = CurrentAccountTrueName;

            hidProjectManagerAccountId.Value = CurrentAccountId.ToString();
            HiddenField_OrganiserAccountId.Value = CurrentAccountId.ToString();
            HiddenField_ExecutorAccountId.Value = CurrentAccountId.ToString();

            txtCompletionTime.Text = dtNow.ToShortDateString();
            txtEstTimeStart.Text = dtNow.ToShortDateString();
            txtActualFinishTime.Text = dtNow.ToShortDateString();
            txtActualStartTime.Text = dtNow.ToShortDateString();
        }

        protected void SetDropDownList(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.ProjectInfoDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {

            int ParentProjectId = SafeConvert.ToInt(hid_ParentProjectId.Value);
            int ProjectTypeId = SafeConvert.ToInt(ddl_ProjectTypeId.SelectedValue);
            string Title = this.txtTitle.Text;
            string Name = this.txtName.Text;
            int ProjectManagerAccountId = SafeConvert.ToInt(hidProjectManagerAccountId.Value,0);
            DateTime CompletionTime = DateTime.Parse(this.txtCompletionTime.Text);
            DateTime ActualFinishTime = DateTime.Parse(this.txtActualFinishTime.Text);
            decimal EstDisbursement = decimal.Parse(this.txtEstDisbursement.Text);
            decimal ActualPayment = decimal.Parse(this.txtActualPayment.Text);
            DateTime ActualStartTime = DateTime.Parse(this.txtActualStartTime.Text);
            DateTime EstTimeStart = DateTime.Parse(this.txtEstTimeStart.Text);
            int CompleteWorkDay = int.Parse(this.txtCompleteWorkDay.Text);
            int ActualWorkingDay = int.Parse(this.txtActualWorkingDay.Text);
            int State = SafeConvert.ToInt(DropDownList_State.SelectedValue);
            string JobDescription = this.txtJobDescription.Text;
            //string TaskDescription = this.txtTaskDescription.Text;

            decimal EstExpenses = SafeConvert.ToDecimal(this.txtEstExpenses.Text);
            decimal ActualExpenditure = SafeConvert.ToDecimal(this.txtActualExpenditure.Text);
            decimal AnticipatedRevenue = SafeConvert.ToDecimal(this.txtAnticipatedRevenue.Text);
            decimal Income = SafeConvert.ToDecimal(this.txtIncome.Text);
            int PredecessorTaskProjectId = SafeConvert.ToInt(ddl_PredecessorTaskProjectId.SelectedValue);
            int SurveyorAccountId = SafeConvert.ToInt(hid_SurveyorAccountId.Value,0);
            int BySalesOpportunitiesId = SafeConvert.ToInt(hid_BySalesOpportunitiesId.Value,0);
            int ByClientInfoId = SafeConvert.ToInt(hid_ByClientInfoId,0);
            string Remarks = this.txtRemarks.Text;
            int CreateAccountId = CurrentAccountId;
            int RefreshAccountId = CurrentAccountId;
            DateTime CreateTime = DateTime.Now;
            DateTime RefreshTime = DateTime.Now;

            XinYiOffice.Model.ProjectInfo model = new XinYiOffice.Model.ProjectInfo();
            model.ParentProjectId = ParentProjectId;
            model.ProjectTypeId = ProjectTypeId;
            model.Title = Title;
            model.Name = Name;
            model.ProjectManagerAccountId = ProjectManagerAccountId;
            model.CompletionTime = CompletionTime;
            model.ActualFinishTime = ActualFinishTime;
            model.EstDisbursement = EstDisbursement;
            model.ActualPayment = ActualPayment;
            model.ActualStartTime = ActualStartTime;
            model.EstTimeStart = EstTimeStart;
            model.CompleteWorkDay = CompleteWorkDay;
            model.ActualWorkingDay = ActualWorkingDay;
            model.State = State;
            model.JobDescription = JobDescription;
            //model.TaskDescription = TaskDescription;
            
            model.EstExpenses = EstExpenses;
            model.ActualExpenditure = ActualExpenditure;
            model.AnticipatedRevenue = AnticipatedRevenue;
            model.Income = Income;
            model.PredecessorTaskProjectId = PredecessorTaskProjectId;
            model.SurveyorAccountId = SurveyorAccountId;
            model.BySalesOpportunitiesId = BySalesOpportunitiesId;
            model.ByClientInfoId = ByClientInfoId;

            model.ProjectPropertie = DropDownList_ProjectPropertie.SelectedValue;
            model.URL = TextBox_URL.Text;
            model.OrganiserAccountId = SafeConvert.ToInt(HiddenField_OrganiserAccountId.Value);
            model.ExecutorAccountId = SafeConvert.ToInt(HiddenField_ExecutorAccountId.Value);

            model.Remarks = Remarks;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.ProjectInfo bll = new XinYiOffice.BLL.ProjectInfo();
            bll.Add(model);

            xytools.web_alert("创建成功！", "list.aspx");
        }
    }
}
