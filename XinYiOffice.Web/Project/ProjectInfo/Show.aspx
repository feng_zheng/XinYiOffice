﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="XinYiOffice.Web.Project.ProjectInfo.Show" Title="显示页" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
<link rel="stylesheet" href="/css/jquery.treetable.theme.default.css" />
<link rel="stylesheet" href="/css/jquery.treetable.css" />

<script src="/js/vendor/jquery-ui.js"></script>
<script src="/js/jquery.treetable.js"></script>

<link rel="stylesheet" href="/css/project.css"/>

</asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<form id="f2" name="f2" runat="server">
  <div class="clear">
    	<div class="pro_left">
        	<div class="setup_box">
                <div class="h_title clear"><h4>基本信息</h4></div>

                <asp:Repeater ID="repProjectInfo" runat="server">
                <ItemTemplate>
                <table cellpadding="0" cellspacing="0" class="plan_table">
                    <tr><td class="table_left">项目名称：</td><td><%#DataBinder.Eval(Container.DataItem, "Title")%></td>
                      <td class="table_left">工作属性：</td>
                      <td><%#DataBinder.Eval(Container.DataItem, "ProjectPropertieName")%></td>
                    </tr>
                    <tr><td class="table_left">项目类型：</td><td><%#DataBinder.Eval(Container.DataItem, "TypeName")%></td>
                      <td class="table_left">发起人：</td>
                      <td><%#DataBinder.Eval(Container.DataItem, "OrganiserAccountName")%></td>
                    </tr>
                    <tr><td class="table_left">状态：</td><td><span style=" color:Red;"><%#DataBinder.Eval(Container.DataItem, "StateName")%></span></td>
                      <td class="table_left">执行人：</td>
                      <td><%#DataBinder.Eval(Container.DataItem, "ExecutorName")%></td>
                    </tr>
                    <tr><td class="table_left">所属项目：</td><td colspan="3"><%#DataBinder.Eval(Container.DataItem, "ParentProjectTitle")%></td>
                    </tr>
                    <tr><td class="table_left">前置任务：</td><td colspan="3"><%#DataBinder.Eval(Container.DataItem, "PredecessorTaskProjectTitle")%></td></tr>
                    <tr><td class="table_left">项目负责人：</td><td colspan="3"><%#DataBinder.Eval(Container.DataItem, "ProjectManagerAccountName")%></td></tr>
                    <tr><td class="table_left">工作描述：</td><td colspan="3"><%#DataBinder.Eval(Container.DataItem, "JobDescription")%></td></tr>
                    <tr>
                      <td class="table_left">所属客户:</td>
                      <td colspan="3"><%#DataBinder.Eval(Container.DataItem, "ByClientInfoName")%></td>
                    </tr>
                    <tr>
                      <td class="table_left">验收人:</td>
                      <td colspan="3"><%#DataBinder.Eval(Container.DataItem, "SurveyorAccountName")%></td>
                    </tr>
                    <tr>
                      <td class="table_left">销售来源:</td>
                      <td colspan="3"><%#DataBinder.Eval(Container.DataItem, "BySalesOpportunitiesName")%></td>
                    </tr>
                    <tr>
                      <td colspan="4" align="right"><a href="StartProcessing.aspx?pid=<%#DataBinder.Eval(Container.DataItem, "Id")%>" class="button green medium" id="" style="text-indent:0;">处理此工作</a></td>
                    </tr>
                    
                </table>
                   </ItemTemplate>
                </asp:Repeater>

            </div>
            
      <div class="marketing_table1 clear">
                <div class="h_title cy"><h4>内部工作/模块/任务</h4></div>
                <table id="example-advanced">
        <caption>
          <a href="#" onClick="jQuery('#example-advanced').treetable('expandAll'); return false;">展开</a>
          <a href="#" onClick="jQuery('#example-advanced').treetable('collapseAll'); return false;">收起</a>
        </caption>
        <thead>
        <tr>
          <th>工作名称</th>
                <th>属性</th>
                <th>类型</th>
                <th>状态</th>
                <th>开始时间</th>
                <th>进度</th>
                <th>执行人</th></tr>
        </thead>
        <tbody runat="server" id="tbody_cell">

        </tbody>
      </table>

            </div>
            
            
            <div class="setup_box clear">
                <div class="h_title cy"><h4>项目组成员</h4><a href="/Project/ProjectMembers/List.aspx?ProjectID=<%=ProjectId%>" class="mang">管理成员</a><a href="/Project/ProjectMembers/add.aspx?ProjectID=<%=ProjectId%>">添加成员</a></div>
                
                <div id="project_mem_box">
                
                <asp:Repeater ID="repProjectMem" runat="server">
                <ItemTemplate>
                  <div class="pmb_item">
                     <img src="<%#GetAccountsHeadPortrait(DataBinder.Eval(Container.DataItem,"HeadPortrait").ToString())%>" title="<%#DataBinder.Eval(Container.DataItem,"Name")%>" width="37" height="37"/><br/>
                     <a><%#DataBinder.Eval(Container.DataItem, "FullName")%></a><br/>
                     <span><%#DataBinder.Eval(Container.DataItem, "Name")%></span>
                  </div>
                 </ItemTemplate>
                </asp:Repeater>
                  
                </div>
                
            </div>
            <asp:Panel runat="server" ID="panTime">
            <div class="setup_box clear">
                <div class="h_title time"><h4>项目时间管理</h4><a href="javascript:;" style="display:none;">甘特图</a></div>

                <asp:Repeater ID="repProject_Time" runat="server">
                <ItemTemplate>
                <table cellpadding="0" cellspacing="0" class="plan_table">
                    <tr><td class="table_left">项目预计完成时间：</td><td><%#DataBinder.Eval(Container.DataItem, "CompletionTime")%></td><td class="table_left">实际完成时间：</td><td><%#DataBinder.Eval(Container.DataItem, "ActualFinishTime")%> </td></tr>
                    <tr><td class="table_left">预计开始时间：</td><td><%#DataBinder.Eval(Container.DataItem, "EstTimeStart")%></td><td class="table_left">实际开始时间：</td><td><%#DataBinder.Eval(Container.DataItem, "ActualStartTime")%></td></tr>
                    <tr><td class="table_left">预计完成工作日：</td><td><%#DataBinder.Eval(Container.DataItem, "CompleteWorkDay")%></td><td class="table_left">实际完成工作日：</td><td><%#DataBinder.Eval(Container.DataItem, "ActualWorkingDay")%></td></tr> 
                    <tr><td class="table_left"></td><td></td><td class="table_left">验收人：</td><td><%#DataBinder.Eval(Container.DataItem, "SurveyorAccountName")%></td></tr> 
                </table>
                                </ItemTemplate>
                </asp:Repeater>
            </div>
            </asp:Panel>

            <asp:Panel runat="server" ID="panEstDisbursement">
                <div class="setup_box clear">
                <div class="h_title time"><h4>资金成本</h4></div>
                 <asp:Repeater ID="repProject_Financing" runat="server">
                <ItemTemplate>
                <table cellpadding="0" cellspacing="0" class="plan_table">
                    <tr>
                      <td class="table_left">项目预计费用        ：</td><td><%#DataBinder.Eval(Container.DataItem, "EstDisbursement")%></td><td class="table_left">实际支付费用        ：</td><td><%#DataBinder.Eval(Container.DataItem, "ActualPayment")%> </td></tr>
                    <tr>
                      <td class="table_left">预计支出        ：</td><td><%#DataBinder.Eval(Container.DataItem, "EstExpenses")%></td><td class="table_left">实际支出        ：</td><td><%#DataBinder.Eval(Container.DataItem, "ActualExpenditure")%></td></tr>
                    <tr>
                      <td class="table_left">预计收入        ：</td><td><%#DataBinder.Eval(Container.DataItem, "AnticipatedRevenue")%></td><td class="table_left">实际收入        ：</td><td><%#DataBinder.Eval(Container.DataItem, "Income")%></td></tr> 
                </table>
                                </ItemTemplate>
                </asp:Repeater>
		        </div>
                </asp:Panel>

        </div>
    	<div class="pro_right">

        	<div class="setup_box">
                <div class="h_title"><h4>议题追踪</h4></div>
                <ul>
                <asp:Repeater ID="repIssueTrackerTypes" runat="server">
                <ItemTemplate>
                <li><a href="/Project/IssueTracker/List.aspx?ProjectId=<%=ProjectId%>&TypeId=<%#DataBinder.Eval(Container.DataItem, "Id")%>"><%#DataBinder.Eval(Container.DataItem, "ClassName")%> (<span class="hui">开启数: <%# IssueTrackerOpenCount(DataBinder.Eval(Container.DataItem, "Id").ToString(), ProjectId)%> / 总计:<%# IssueTrackerCount(DataBinder.Eval(Container.DataItem, "Id").ToString(), ProjectId)%> </span>)</a></li>
                </ItemTemplate>
                </asp:Repeater>
                	

                	
                </ul>
            </div>

            <%if (ValidatePermission("DISCUSS_BASIC")){ %>
            <div class="setup_box">
                <div class="h_title"><h4>讨论区</h4></div>
                <ul>
                <asp:Repeater ID="repDiscussClass" runat="server">
                <ItemTemplate>
                <li><a href="/Discuss/List.aspx?ProjectId=<%=ProjectId%>&ClassId=<%#DataBinder.Eval(Container.DataItem, "Id")%>"><%#DataBinder.Eval(Container.DataItem, "Name")%> (<span class="hui">今日:<%# DiscussToDayCount(ProjectId.ToString(), DataBinder.Eval(Container.DataItem, "Id").ToString())%> / 总计:<%# DiscussCount(ProjectId.ToString(), DataBinder.Eval(Container.DataItem, "Id").ToString())%></span>)</a></li>
                </ItemTemplate>
                </asp:Repeater>

                </ul>
            </div>
            <%} %>
        </div>
    </div>
            </form>

            <script>
      

                $("#example-advanced").treetable({ expandable: true });


                // Highlight selected row
                $("#example-advanced tbody tr").mousedown(function () {
                    $("tr.selected").removeClass("selected");
                    $(this).addClass("selected");
                });

                // Drag & Drop Example Code
                $("#example-advanced .file, #example-advanced .folder").draggable({
                    helper: "clone",
                    opacity: .75,
                    refreshPositions: true, // Performance?
                    revert: "invalid",
                    revertDuration: 300,
                    scroll: true
                });

                $("#example-advanced .folder").each(function () {
                    $(this).parents("tr").droppable({
                        accept: ".file, .folder",
                        drop: function (e, ui) {
                            var droppedEl = ui.draggable.parents("tr");
                            $("#example-advanced").treetable("move", droppedEl.data("ttId"), $(this).data("ttId"));
                        },
                        hoverClass: "accept",
                        over: function (e, ui) {
                            var droppedEl = ui.draggable.parents("tr");
                            if (this != droppedEl[0] && !$(this).is(".expanded")) {
                                $("#example-advanced").treetable("expandNode", $(this).data("ttId"));
                            }
                        }
                    });
                });

                jQuery('#example-advanced').treetable('expandAll');
                
    </script>

</asp:Content>





