﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="StartProcessing.aspx.cs" Inherits="XinYiOffice.Web.Project.ProjectInfo.StartProcessing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form  id="f2" name="f2" runat="server" enctype="multipart/form-data">
   <div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>开始处理</h4></div>
 
   <table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		项目名
	：</td>
	<td height="25" width="*" align="left">
		
	    <asp:Label ID="lableTitle" runat="server"></asp:Label>
		
	    <asp:HiddenField ID="HiddenField_ProctId" runat="server" />
		
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		备注
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtTypeDescribe" runat="server" Width="561px" Height="179px" 
            TextMode="MultiLine"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		状态：</td>
	<td height="25" width="*" align="left">
	    <asp:DropDownList ID="DropDownList_State" runat="server">
        </asp:DropDownList>
	</td></tr>
	</table>
    </div>
    
    <div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">确定</asp:LinkButton>
        <a href="javascript:history.go(-1);" class="cancel">取消</a>
        
    </div>

    </form>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cp2" runat="server">
</asp:Content>
