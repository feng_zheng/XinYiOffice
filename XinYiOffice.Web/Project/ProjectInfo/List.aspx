﻿<%@ Page Title="项目表 工作"  MasterPageFile="~/BasicContent.Master" Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" EnableViewState="false" Inherits="XinYiOffice.Web.Project.ProjectInfo.List" %>
<%@ Register src="../../InitControl/SearchApp.ascx" tagname="SearchApp" tagprefix="uc1" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
<link rel="stylesheet" href="/css/pagecss.css">
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<form id="f2" name="f2" >
<div class="marketing_box">
<div class="btn_search_box">
        	<div class="clear cc" style=" padding-left:15px;">
            <div class="btn clear">
            
  <%if (ValidatePermission("PROJECTINFO_LIST"))
              { %>
            <a href="javascript:;" id="search_a"  class="sj">条件查询</a>
            <%} %>
            <%if (ValidatePermission("PROJECTINFO_LIST_ADD"))
              { %>
            <a href="add.aspx">添加</a>
            <%} %>
            <%if (ValidatePermission("PROJECTINFO_LIST_EDIT"))
              { %>
            <a href="javascript:;" class="edit">修改</a>
            <%} %>
             <%if (ValidatePermission("PROJECTINFO_LIST_DEL"))
              { %>
            <a href="javascript:;" class="tdel">删除</a>
            <%} %>

            
            </div>
                <div class="del clear"></div>
            </div>
          
            <div class="show_search_box" style="display:none;">
        		<div class="tjss clear">


                    <uc1:searchapp ID="SearchApp1" runat="server" TableName="vProjectInfoList"/>

                </div>
                <div class="clear btn_box btn_box_p">
                <a href="javascript:;" class="save btn_search">查询</a>
                <a href="javascript:;" class="cancel res_search">取消</a>
                
              
                </div>
        	</div>
    
        </div>
        
        <div class="marketing_table">
        	<table cellpadding="0" cellspacing="0" id="ClientTracking_table">

                <asp:Repeater ID="repProject" runat="server" >
                <HeaderTemplate>
                <tr><th><input type="checkbox" id="cb_all"></th>
                <th>工作名称</th>
                <th>项目属性</th>
                <th>项目类型</th>
                <th>状态</th>
                <th>开始时间</th>
                <th>进度</th>
                <th>执行人</th></tr>
                </HeaderTemplate>
                
                <ItemTemplate>
                <tr><td class="center"><input type="checkbox" name="chk_list" va="<%#DataBinder.Eval(Container.DataItem, "Id")%>"></td>
                <td><a href="show.aspx?Id=<%#DataBinder.Eval(Container.DataItem, "Id")%>"><%#Eval("Title").ToString().Length > 50 ? Eval("Title").ToString().Substring(0, 50) + "..." : Eval("Title")%></a></td>
                <td><%#DataBinder.Eval(Container.DataItem, "ProjectPropertieName")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "TypeName")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "StateName")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "ActualStartTime","{0:yyyy-MM-dd}")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "ActualStartTime", "{0:yyyy-MM-dd}")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "ExecutorName")%></td>
                </tr>    
                </ItemTemplate>
                </asp:Repeater>


            </table>
        </div>
        
</div>

<%=pagehtml%>
</form>

<script>
    $('#ClientTracking_table td').hover(function () {
        $(this).parent().find('td').css('background', '#f0f8fc')
    }, function () {
        $(this).parent().find('td').css('background', 'none')
    });
</script>
</asp:Content>


