﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Basic;
using System.Collections.Generic;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Project.ProjectInfo
{
    public partial class Show : BasicPage
    {
        public string strid = "";
        public int ProjectId;

        protected string strMap = "";//JQTreeTable要的参数

        public int lay = 0;//当前循环层级
        public int indexNum = 1;//当前循环总序列

        public DataTable dtIssueTrackerTypes;
        public DataTable dtDiscuss;

        StringBuilder sb = new StringBuilder();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("PROJECTINFO_LIST_VIEW"))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                int Id = SafeConvert.ToInt(xytools.url_get("Id"), 0);
                if (Id == 0)
                {
                    Id = SafeConvert.ToInt(xytools.url_get("ProjectId"), 0);
                }

                ProjectId = Id;
                ShowInfo(Id);

            }
        }

        private void ShowInfo(int Id)
        {
            DataTable dt = ProjectInfoServer.GetProjectList(string.Format("Id={0}", Id),CurrentTenantId);
            DataSet dsProjectMem = new DataSet();
            DataSet ds = new DataSet();

            ds = new BLL.IssueTracker().GetList(string.Format("ProjectInfoId={0} and TenantId={1}", Id.ToString(),CurrentTenantId));
            if (ds != null && ds.Tables.Count > 0)
            {
                dtIssueTrackerTypes = ds.Tables[0];
            }
            dsProjectMem = new BLL.ProjectMembers().GetListView(string.Format("ProjectId={0} and TenantId={1} ", Id,CurrentTenantId));

            repProjectInfo.DataSource = dt;
            repProjectInfo.DataBind();

            repProjectMem.DataSource = dsProjectMem;
            repProjectMem.DataBind();



            if (ValidatePermission("PROJECTROLES_LIST_TIMEMANAGE"))
            {
                panTime.Visible = true;
                repProject_Time.DataSource = dt;
                repProject_Time.DataBind();
            }
            else
            {
                panTime.Visible = false;
            }

            if (ValidatePermission("PROJECTROLES_LIST_ESTMANAGE"))
            {
                repProject_Financing.DataSource = dt;
                repProject_Financing.DataBind();
            }
            else
            {
                panEstDisbursement.Visible = false;
            }


            repIssueTrackerTypes.DataSource = new BLL.IssueTrackerTypes().GetList(string.Format("1=1 and TenantId={0}",CurrentTenantId));
            repIssueTrackerTypes.DataBind();

            repDiscussClass.DataSource = new BLL.DiscussClass().GetList(string.Format("1=1 and TenantId={0} ",CurrentTenantId));
            repDiscussClass.DataBind();


            GetProjectTreeList(ProjectId, ref tbody_cell);


        }

        public string GetAccountsHeadPortrait(string _head)
        {
            return AccountsServer.GetAccountsHeadPortrait(_head);
        }

        /// <summary>
        /// 打开
        /// </summary>
        /// <param name="TypeId"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        public string IssueTrackerOpenCount(string TypeId, int pid)
        {
            DataRow[] dr = dtIssueTrackerTypes.Select(string.Format(" [TYPE]={0} and Sate=1", TypeId));
            string _w = "0";

            if (dr != null && dr.Length > 0)
            {
                _w = SafeConvert.ToString(dr.Length);
            }

            return _w;
        }

        /// <summary>
        /// 追踪总数
        /// </summary>
        /// <param name="TypeId"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        public string IssueTrackerCount(string TypeId, int pid)
        {
            DataRow[] dr = dtIssueTrackerTypes.Select(string.Format("[TYPE]={0} ", TypeId));
            string _w = "0";

            if (dr != null && dr.Length > 0)
            {
                _w = SafeConvert.ToString(dr.Length);
            }

            return _w;
        }

        /// <summary>
        /// 今日讨论
        /// </summary>
        /// <returns></returns>
        public string DiscussToDayCount(string ProjectId, string DiscussClassId)
        {
            int _count = new BLL.Discuss().GetRecordCount(string.Format("ProjectInfoId={0} and DATEPART(year, CreateTime)='{1}' and DATEPART(month, CreateTime)='{2}' and DATEPART(day, CreateTime)='{3}' and DiscussClassId='{4}'",
                ProjectId, DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DiscussClassId));

            return _count.ToString();
        }


        /// <summary>
        /// 讨论总数
        /// </summary>
        /// <returns></returns>
        public string DiscussCount(string ProjectId, string DiscussClassId)
        {
            int _count = new BLL.Discuss().GetRecordCount(string.Format("ProjectInfoId={0}  and DiscussClassId='{1}'",
                ProjectId, DiscussClassId));

            return _count.ToString();
        }

        #region 下属项目

        public void GetProjectTreeList(int pid, ref System.Web.UI.HtmlControls.HtmlGenericControl hgc)
        {
            DataRowCollection dr = ProjectInfoServer.GetProjectList(string.Format("ParentProjectId={0}", pid),CurrentTenantId).Rows;

            int CurrentId = 0;
            int CurrentParentProjectId = 0;

            string _format = string.Empty;

            foreach (DataRow _dr in dr)
            {
                lay++;//层级

                string name = SafeConvert.ToString(_dr["Title"]);
                CurrentId = SafeConvert.ToInt(_dr["Id"], 1);
                CurrentParentProjectId = SafeConvert.ToInt(_dr["ParentProjectId"], 0);
                bool isClass = GetClassInExist(CurrentId);

                System.Web.UI.HtmlControls.HtmlTableRow htr = new System.Web.UI.HtmlControls.HtmlTableRow();
                System.Web.UI.HtmlControls.HtmlTableCell htc_1 = new System.Web.UI.HtmlControls.HtmlTableCell();
                System.Web.UI.HtmlControls.HtmlTableCell htc_2 = new System.Web.UI.HtmlControls.HtmlTableCell();
                System.Web.UI.HtmlControls.HtmlTableCell htc_3 = new System.Web.UI.HtmlControls.HtmlTableCell();
                System.Web.UI.HtmlControls.HtmlTableCell htc_4 = new System.Web.UI.HtmlControls.HtmlTableCell();
                System.Web.UI.HtmlControls.HtmlTableCell htc_5 = new System.Web.UI.HtmlControls.HtmlTableCell();
                System.Web.UI.HtmlControls.HtmlTableCell htc_6 = new System.Web.UI.HtmlControls.HtmlTableCell();
                System.Web.UI.HtmlControls.HtmlTableCell htc_7 = new System.Web.UI.HtmlControls.HtmlTableCell();

                HtmlGenericControl span2 = new HtmlGenericControl("span");
                HtmlGenericControl span3 = new HtmlGenericControl("span");
                HtmlGenericControl span4 = new HtmlGenericControl("span");
                HtmlGenericControl span5 = new HtmlGenericControl("span");
                HtmlGenericControl span6 = new HtmlGenericControl("span");
                HtmlGenericControl span7 = new HtmlGenericControl("span");

                span2.InnerText = SafeConvert.ToString(_dr["ProjectPropertieName"]);

                if (isClass)
                {

                    htc_1.Controls.Add(new LiteralControl(string.Format("<span class='folder'><a href=\"Show.aspx?Id={1}\">{0}</a></span>", name,CurrentId)));
                }
                else
                {
                    htc_1.Controls.Add(new LiteralControl(string.Format("<span class='file'><a href=\"Show.aspx?Id={1}\">{0}</a></span>", name,CurrentId)));
                }

                htc_2.Controls.Add(span2);
                span2.InnerText = SafeConvert.ToString(_dr["ProjectPropertieName"]);

                htc_3.Controls.Add(span3);
                span3.InnerText = SafeConvert.ToString(_dr["TypeName"]);

                htc_4.Controls.Add(span4);
                span4.InnerText = SafeConvert.ToString(_dr["StateName"]);

                htc_5.Controls.Add(span5);
                span5.InnerText = SafeConvert.ToDateTime(_dr["ActualStartTime"]).ToShortDateString();

                htc_6.Controls.Add(span6);
                span6.InnerText = "77";

                htc_7.Controls.Add(span7);
                span7.InnerText = SafeConvert.ToString(_dr["ExecutorName"]).ToString();

                //1个层级
                htr.Attributes.Add("data-tt-id", CurrentId.ToString());

                //有父级
                if (CurrentParentProjectId != 0 && CurrentParentProjectId != ProjectId)
                {
                    htr.Attributes.Add("data-tt-parent-id", CurrentParentProjectId.ToString());
                }

                htr.Controls.Add(htc_1);
                htr.Controls.Add(htc_2);
                htr.Controls.Add(htc_3);
                htr.Controls.Add(htc_4);
                htr.Controls.Add(htc_5);
                htr.Controls.Add(htc_6);
                htr.Controls.Add(htc_7);

                hgc.Controls.Add(htr);

                if (isClass)
                {
                    GetProjectTreeList(CurrentId, ref hgc);
                }
                lay--;//层级减1

                indexNum++;//序列加1
            }
        }

        public static string Nex(int x, int bid, int pid, string cname)
        {
            StringBuilder s = new StringBuilder();

            string kg = HttpUtility.HtmlDecode("&nbsp;");

            char nbsp = (char)0xA0;


            for (int i = 0; i < x; i++)
            {
                kg += kg;

                nbsp += nbsp;
            }

            if (pid == 0)
            {
                //s.Append("─" + cname);
                s.Append("" + cname);
            }
            else
            {
                if (!GetCustomInExist(bid))
                {
                    //不存在子级
                    s.Append(kg + "├" + cname);
                    //s.Append(nbsp + "├" + cname);

                }
                else
                {//存在子级
                    s.Append(kg + "└" + cname);
                    //s.Append(nbsp + "└" + cname);
                }


            }

            return s.ToString();
        }

        /// <summary>
        /// 是否有子集
        /// </summary>
        /// <param name="cid"></param>
        /// <returns></returns>
        protected  bool GetClassInExist(int cid)
        {
            DataTable dt = ProjectInfoServer.GetProjectList(string.Format("ParentProjectId={0}", cid),CurrentTenantId);

            if (dt != null && dt.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        protected static bool GetCustomInExist(int cid)
        {

            //XinYiCMS.BLL.xyc_CustomClass bll_customclass = new XinYiCMS.BLL.xyc_CustomClass();
            //return bll_customclass.GetCustomInExist(pid);

            DataSet ds = new BLL.ProjectInfo().GetList(string.Format(" Id in(select ParentProjectId from ProjectInfo where Id={0})", cid));

            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        #endregion

    }
}
