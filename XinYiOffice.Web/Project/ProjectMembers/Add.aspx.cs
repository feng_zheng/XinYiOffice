﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Project.ProjectMembers
{
    public partial class Add : BasicPage
    {
        public int ProjectId;

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                InitData();
            }
        }

        public void InitData() 
        {
            ProjectId = SafeConvert.ToInt(xytools.url_get("ProjectId"), 0);
            Model.ProjectInfo pi = new Model.ProjectInfo();
            pi = new BLL.ProjectInfo().GetModel(ProjectId);
            Label_Project.Text = pi.Title;
            HiddenField_ProjectId.Value = ProjectId.ToString();

            DropDownList_ProjectRoleId.DataSource=new BLL.ProjectRoles().GetList(string.Format(" TenantId={0}",CurrentTenantId));
            DropDownList_ProjectRoleId.DataTextField = "Name";
            DropDownList_ProjectRoleId.DataValueField = "Id";
            DropDownList_ProjectRoleId.DataBind();

        }

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int ProjectId = SafeConvert.ToInt(HiddenField_ProjectId.Value,0);
            int AccountId = SafeConvert.ToInt(HiddenField_AccountId.Value,0);
            int ProjectRoleId = SafeConvert.ToInt(DropDownList_ProjectRoleId.SelectedValue,0);
            int CreateAccountId = CurrentAccountId;
            int RefreshAccountId =CurrentAccountId;
            DateTime CreateTime = DateTime.Now;
            DateTime RefreshTime = DateTime.Now;

            XinYiOffice.Model.ProjectMembers model = new XinYiOffice.Model.ProjectMembers();
            model.ProjectId = ProjectId;
            model.AccountId = AccountId;
            model.ProjectRoleId = ProjectRoleId;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.ProjectMembers bll = new XinYiOffice.BLL.ProjectMembers();
            bll.Add(model);
            xytools.web_alert_new_url("保存成功！", string.Format("/Project/ProjectInfo/show.aspx?ProjectId={0}", ProjectId));
        }
    }
}
