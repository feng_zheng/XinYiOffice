﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Project.IssueTracker
{
    public partial class Add : BasicPage
    {
        public int projectId = 0;
        public int typeId = 0;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("ISSUETRACKER_LIST_ADD"))
            {
                base.NoPermissionPage();
            }

            if (!IsPostBack)
            {
                InitData();
            }
        }

        protected void InitData()
        {
            #region 项目及追踪类型
            projectId = SafeConvert.ToInt(xytools.url_get("projectId"), 0);
            typeId = SafeConvert.ToInt(xytools.url_get("typeId"), 0);

            if (projectId != 0 && typeId != 0)
            {
                Model.ProjectInfo pi = new BLL.ProjectInfo().GetModel(projectId);
                Model.IssueTrackerTypes itt = new BLL.IssueTrackerTypes().GetModel(typeId);

                Label_ProjectInfoName.Text = pi.Title;
                HiddenField_ProjectInfoId.Value = pi.Id.ToString();

                Label_TypeName.Text = itt.ClassName;
                HiddenField_TypeId.Value = itt.Id.ToString();
            }
            else
            {
                xytools.web_alert("请从项目查看页添加", string.Format("list.aspx?projectId={0}&typeId={1}", projectId, typeId));
            } 
            #endregion

            SetDropDownList("Sate", ref DropDownList_Sate);

            txtAuthorsAccountId.Text = CurrentAccountTrueName;
            txtAssignerAccountId.Text = CurrentAccountTrueName;
            HiddenField_AuthorsAccountId.Value = CurrentAccountId.ToString();
            HiddenField_txtAssignerAccountId.Value = CurrentAccountId.ToString();
        }

        protected void SetDropDownList(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.IssueTrackerDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int ProjectInfoId = SafeConvert.ToInt(HiddenField_ProjectInfoId.Value, 0);
            string Title = this.txtTitle.Text;
            string Tabloid = this.txtTabloid.Text;
            string Item = this.txtItem.Text;
            string Priority = DropDownList_Priority.SelectedValue;
            string Solver = string.Empty;
            int Sate = SafeConvert.ToInt(DropDownList_Sate.SelectedValue, 0);
            string Version = this.txtVersion.Text;
            string Url = this.txtUrl.Text;
            string Constitute = this.txtConstitute.Text;
            string Serious = DropDownList_Serious.SelectedValue;
            int AuthorsAccountId = SafeConvert.ToInt(HiddenField_AuthorsAccountId.Value);
            int AssignerAccountId = SafeConvert.ToInt(HiddenField_txtAssignerAccountId.Value);
            //DateTime StartTime = DateTime.MinValue;
            //DateTime EndTime = DateTime.MinValue;
            int TYPE = SafeConvert.ToInt(HiddenField_TypeId.Value, 0);
            int IsEdit = 0;//不进行编辑
            int CreateAccountId = CurrentAccountId;
            int RefreshAccountId = CurrentAccountId;
            DateTime CreateTime = DateTime.Now;
            DateTime RefreshTime = CreateTime;

            XinYiOffice.Model.IssueTracker model = new XinYiOffice.Model.IssueTracker();
            model.ProjectInfoId = ProjectInfoId;
            model.Title = Title;
            model.Tabloid = Tabloid;
            model.Item = Item;
            model.Priority = Priority;
            model.Solver = Solver;
            model.Sate = Sate;
            model.Version = Version;
            model.Url = Url;
            model.Constitute = Constitute;
            model.Serious = Serious;
            model.AuthorsAccountId = AuthorsAccountId;
            model.AssignerAccountId = AssignerAccountId;
            //model.StartTime = StartTime;
            //model.EndTime = EndTime;
            model.TYPE = TYPE;
            model.IsEdit = IsEdit;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.StartTime = CreateTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.IssueTracker bll = new XinYiOffice.BLL.IssueTracker();
            bll.Add(model);

            xytools.web_alert("保存成功！", string.Format("list.aspx?projectId={0}&typeId={1}", ProjectInfoId, TYPE));
        }
    }
}
