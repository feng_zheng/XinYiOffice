﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Project.ProjectTypes
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("PROJECTTYPES_LIST_ADD"))
            {
                base.NoPermissionPage();
            }
        }

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            string TypeName = this.txtTypeName.Text;
            string TypeDescribe = this.txtTypeDescribe.Text;

            string TypeIco = string.Empty;
            if (!string.IsNullOrEmpty(FileUpload_TypeIco.FileName))
            {
                TypeIco = WebUpLoad.PostActionUpLoad(FileUpload_TypeIco.ID, "ProjectTypes");
            }

            string TypeIcoMax = string.Empty;
            if (!string.IsNullOrEmpty(FileUpload_TypeIcoMax.FileName))
            {
                TypeIco = WebUpLoad.PostActionUpLoad(FileUpload_TypeIcoMax.ID, "ProjectTypes");
            }

            int CreateAccountId = CurrentAccountId;
            int RefreshAccountId = CurrentAccountId;
            DateTime CreateTime = DateTime.Now;
            DateTime RefreshTime = DateTime.Now;

            XinYiOffice.Model.ProjectTypes model = new XinYiOffice.Model.ProjectTypes();
            model.TypeName = TypeName;
            model.TypeDescribe = TypeDescribe;
            model.TypeIco = TypeIco;
            model.TypeIcoMax = TypeIcoMax;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.ProjectTypes bll = new XinYiOffice.BLL.ProjectTypes();
            bll.Add(model);
            xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
