﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Project.ProjectRoles
{
    public partial class Modify : BasicPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("PROJECTROLES_LIST_EDIT"))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }
        }

        private void InitData()
        {
            DropDownList_RoleId.DataSource = new BLL.Roles().GetAllList();
            DropDownList_RoleId.DataTextField = "Name";
            DropDownList_RoleId.DataValueField = "Id";

            DropDownList_RoleId.DataBind();
            DropDownList_RoleId.Items.Insert(0,new ListItem("不集成系统权限","0"));
        }

        private void ShowInfo(int Id)
        {
            InitData();

            XinYiOffice.BLL.ProjectRoles bll = new XinYiOffice.BLL.ProjectRoles();
            XinYiOffice.Model.ProjectRoles model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.txtName.Text = model.Name;
            this.txtNameEn.Text = model.NameEn;
            this.txtDESCRIBE.Text = model.DESCRIBE;

            DropDownList_RoleId.SelectedValue = SafeConvert.ToString(model.RoleId);
            HiddenField_CreateAccountId.Value = model.CreateAccountId.ToString();
            HiddenField_CreateTime.Value = DateTime.Now.ToString();
        }




        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = int.Parse(this.lblId.Text);
            string Name = this.txtName.Text;
            string NameEn = this.txtNameEn.Text;
            string DESCRIBE = this.txtDESCRIBE.Text;
            int RoleId = SafeConvert.ToInt(DropDownList_RoleId.SelectedValue);
            int CreateAccountId = SafeConvert.ToInt(HiddenField_CreateAccountId.Value);
            int RefreshAccountId = CurrentAccountId;
            DateTime CreateTime = SafeConvert.ToDateTime(HiddenField_CreateTime.Value);
            DateTime RefreshTime = DateTime.Now;


            XinYiOffice.Model.ProjectRoles model = new XinYiOffice.Model.ProjectRoles();
            model.Id = Id;
            model.Name = Name;
            model.NameEn = NameEn;
            model.DESCRIBE = DESCRIBE;
            model.RoleId = RoleId;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;
            XinYiOffice.BLL.ProjectRoles bll = new XinYiOffice.BLL.ProjectRoles();
            bll.Update(model);
            xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
