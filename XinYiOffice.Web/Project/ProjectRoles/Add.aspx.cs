﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Project.ProjectRoles
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("PROJECTROLES_LIST_ADD"))
            {
                base.NoPermissionPage();
            }

            if(!IsPostBack)
            {
                InitData();
            }
        }

        public void InitData()
        {
            //DropDownList_RoleId.DataSource = new BLL.Roles().GetAllList();
            //DropDownList_RoleId.DataTextField = "Name";
            //DropDownList_RoleId.DataValueField = "Id";

            //DropDownList_RoleId.DataBind();
            DropDownList_RoleId.Items.Insert(0, new ListItem("不集成系统权限", "0"));
        }

        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            string Name = this.txtName.Text;
            string NameEn = this.txtNameEn.Text;
            string DESCRIBE = this.txtDESCRIBE.Text;
            int RoleId = SafeConvert.ToInt(DropDownList_RoleId.SelectedValue,0);
            int CreateAccountId =CurrentAccountId;

            DateTime CreateTime = DateTime.Now;
             

            XinYiOffice.Model.ProjectRoles model = new XinYiOffice.Model.ProjectRoles();
            model.Name = Name;
            model.NameEn = NameEn;
            model.DESCRIBE = DESCRIBE;
            model.RoleId = RoleId;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = CreateAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = CreateTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.ProjectRoles bll = new XinYiOffice.BLL.ProjectRoles();
            bll.Add(model);
            xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
