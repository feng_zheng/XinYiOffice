﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MinLayer.Master" AutoEventWireup="true" CodeBehind="AddClientTracking.aspx.cs" Inherits="XinYiOffice.Web.Client.ClientInfo.AddClientTracking" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head_layer" runat="server">
    <style type="text/css">
    #<%=RadioButtonList_IsFollowUp.ClientID%>{ border:0px;}
    #<%=RadioButtonList_IsFollowUp.ClientID%> table{ border:0px;}
    #<%=RadioButtonList_IsFollowUp.ClientID%> table td{ border:0px; border-bottom:0px;}
    #<%=RadioButtonList_IsFollowUp.ClientID%> input{ border:0px; height:20px; width:auto; vertical-align:middle}
        #<%=RadioButtonList_IsFollowUp.ClientID%> lable{vertical-align:middle;}
      
      .noneinput{ line-height:150%;}  
      .noneinput li{ clear:both; line-height:150%; line-height:28px;}
      
    #zjtable{ font-size:12px;}
    #zjtable th{padding:0px; padding-left:5px; margin:0px; height:18px; width:auto; color:#fff;  font-weight:normal; background-color:#0092C1; text-align:left;}
    #zjtable td{ padding:0px; padding-left:5px; height:18px; width:auto; text-align:left;}
    </style>
    <script>

        $(function () {

            var name = '<%=RadioButtonList_IsFollowUp.ClientID%>';
            var radList = $("#<%=RadioButtonList_IsFollowUp.ClientID%>");

            var pan1 = $("#<%=pan.ClientID%>");
            var pan2 = $("#<%=Panel2.ClientID%>");

            $("#<%=RadioButtonList_IsFollowUp.ClientID%> input").click(function () {
                var sel_val = $(this).val();
                if (sel_val == 1) {
                    pan1.show();
                    pan2.hide();
                }
                else if (sel_val == 0) {
                    pan2.show();
                    pan1.hide();
                } else {
                    pan1.hide(); pan2.hide();
                }

            });

        })

        function canhf() {

            if (confirm("您确定将此跟踪单设置为'取消回访'么?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="PlaceBody" runat="server">
    <form id="f2" name="f2" runat="server">
    <div class="setup_box">
    
<table cellspacing="0" cellpadding="0" width="100%" border="0" class="plan_table">
            
            <tr>
                <td height="25" align="right" class="table_left">
                    客户热度:
                </td>
                <td height="25" align="left">
                    <asp:DropDownList ID="DropDownList_Head" runat="server" style=" width:120px;">
                    </asp:DropDownList>
                    <asp:HiddenField ID="hidClientId" runat="server" />
                </td>
            </tr>
            <tr>
                <td height="25" align="right" class="table_left">
                    回访备注:
                </td>
                <td height="25" align="left">
                    <asp:TextBox ID="TextBox_BackNotes" runat="server" Height="118px" 
                        TextMode="MultiLine" Width="100%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td height="25" align="right" class="table_left">
                    是否跟进:
                </td>
                <td height="25" align="left">
                    <asp:RadioButtonList ID="RadioButtonList_IsFollowUp" runat="server" 
                        Width="468px" RepeatDirection="Horizontal" >
                        <asp:ListItem Value="-1" Selected="True" style="border: 0px;">暂不处理</asp:ListItem>
                        <asp:ListItem Value="1" style="border: 0px;">继续跟进</asp:ListItem>
                        <asp:ListItem Value="0" style="border: 0px;">不跟进</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:Panel ID="pan" runat="server" CssClass="noneinput" Style="display: none;">
                        <li>计划回访时间:
                        <asp:TextBox ID="TextBox_PlanTime" runat="server"  class="Wdate"  onClick="WdatePicker()"></asp:TextBox>(默认值为现在时间往后推3天)
                        </li>
                       <li> 客户热诚:
                        <asp:DropDownList ID="DropDownList_ClientHeat" runat="server">
                        </asp:DropDownList>
                        </li>
                       <li>更新销售机会阶段为：<asp:DropDownList ID="DropDownList_SalesOpportunitiesSate" runat="server">
                        </asp:DropDownList>
                        </li>
                    </asp:Panel>
                    <asp:Panel ID="Panel2" runat="server" CssClass="noneinput" Style="display:none;">
                        <li>不跟进原因:
                        <asp:TextBox ID="TextBox_CloseNotes" runat="server" Height="91px" TextMode="MultiLine" Width="100%"></asp:TextBox>
                        </li>
                        <li>更新销售机会状态为:
                        <asp:DropDownList ID="DropDownList_OpportunitiesSate" runat="server">
                        </asp:DropDownList></li>

                    </asp:Panel>
                </td>
            </tr>
           
        
        </table>
<div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" OnClick="LinkButton1_Click">确定</asp:LinkButton>
   
    </div>
</div>



    </form>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

</asp:Content>
