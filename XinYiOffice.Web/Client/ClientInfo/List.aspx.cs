﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using XinYiOffice.Common;
using System.Drawing;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;
using XinYiOffice.Common.Web;

namespace XinYiOffice.Web.Client.ClientInfo
{
    public partial class List : BasicPage
    {
        
		XinYiOffice.BLL.ClientInfo bll = new XinYiOffice.BLL.ClientInfo();

        public int _page = 1;
        public int _pagezs = 0;
        public int _datazs = 0;
        string benye_url;//当前url
        public string pagehtml = string.Empty;

        public int CustomerLevel = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (base.ValidatePermission("CLIENT_BASIC") && base.ValidatePermission("CLIENT_MANAGE") && base.ValidatePermission("CLIENTINFO_LIST"))
            {

            }
            else
            {
                base.NoPermissionPage();
            }

            string action = xytools.url_get("action");
            if (!string.IsNullOrEmpty(action))
            {
                switch (action)
                {
                    case "search":
                        SearchApp1.SetSqlWhere();
                        BindData();
                        break;
                }
            }
            else if (!Page.IsPostBack)
            {
                BindData();
            }

        }
        
        
        #region gridView
                        
        public void BindData()
        {
            CustomerLevel = SafeConvert.ToInt(xytools.url_get("level"));

            StringBuilder strWhere = new StringBuilder(" 1=1 ");
            if (CustomerLevel != 0)
            {
                strWhere.AppendFormat(" and CustomerLevel={0} ", CustomerLevel);
            }

            strWhere.Append(SearchApp1.SqlWhere);

            string mysql = ClientServer.GetClientInfoListSql(strWhere.ToString(),CurrentTenantId);

            #region 分页
            if (!string.IsNullOrEmpty(xytools.url_get("page")))
            {
                _page = SafeConvert.ToInt(xytools.url_get("page"), 1);
            }
            PageDataSet pd = new PageDataSet();
            repClientInfo.DataSource = pd.GetListPageBySql(mysql, _page, 10, ref _pagezs, ref _datazs);
            PageAttribute pa = new PageAttribute();
            pa.PagCur = _page.ToString();
            pa.PageLinage = "10";
            pa.PageShowFL = "1";
            pa.PageShowGo = "1";

            benye_url = "list.aspx?" + xyurl.GetQueryRemovePage(Request.QueryString);//获取当前url字符串
            pa.PageUrl = benye_url;
            pagehtml = ParseHTML.PageLabel(pa, _pagezs); 
            #endregion

            //repClientInfo.DataSource = dt;
            repClientInfo.DataBind();

        }

        #endregion

        protected void Button1_Click(object sender, EventArgs e)
        {
            xytools.href_url("Add.aspx");
        }





    }
}
