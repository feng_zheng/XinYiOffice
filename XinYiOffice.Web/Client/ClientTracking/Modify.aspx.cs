﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Client.ClientTracking
{
    public partial class Modify : BasicPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("CLIENTTRACKING_LIST_EDIT"))
            {
                NoPermissionPage();
            }


            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.ClientTracking bll = new XinYiOffice.BLL.ClientTracking();
            XinYiOffice.Model.ClientTracking model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.txtClientHeat.Text = model.ClientHeat.ToString();
            this.txtSalesOpportunitiesId.Text = model.SalesOpportunitiesId.ToString();
            this.txtSalesOpportunitiesSate.Text = model.SalesOpportunitiesSate.ToString();
            this.txtBackNotes.Text = model.BackNotes;
            this.txtSate.Text = model.Sate.ToString();
            this.txtIsFollowUp.Text = model.IsFollowUp.ToString();
            this.txtCloseNotes.Text = model.CloseNotes;
            this.txtReturnAccountId.Text = model.ReturnAccountId.ToString();
            this.txtActualTime.Text = model.ActualTime.ToString();
            this.txtPlanTime.Text = model.PlanTime.ToString();
            //this.txtCreateAccountId.Text = model.CreateAccountId.ToString();
            //this.txtRefreshAccountId.Text = model.RefreshAccountId.ToString();
            //this.txtCreateTime.Text = model.CreateTime.ToString();
            //this.txtRefreshTime.Text = model.RefreshTime.ToString();

        }

      


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = int.Parse(this.lblId.Text);
            int ClientHeat = int.Parse(this.txtClientHeat.Text);
            int SalesOpportunitiesId = int.Parse(this.txtSalesOpportunitiesId.Text);
            int SalesOpportunitiesSate = int.Parse(this.txtSalesOpportunitiesSate.Text);
            string BackNotes = this.txtBackNotes.Text;
            int Sate = int.Parse(this.txtSate.Text);
            int IsFollowUp = int.Parse(this.txtIsFollowUp.Text);
            string CloseNotes = this.txtCloseNotes.Text;
            int ReturnAccountId = int.Parse(this.txtReturnAccountId.Text);
            DateTime ActualTime = DateTime.Parse(this.txtActualTime.Text);
            DateTime PlanTime = DateTime.Parse(this.txtPlanTime.Text);
            //int CreateAccountId = int.Parse(this.txtCreateAccountId.Text);
            //int RefreshAccountId = int.Parse(this.txtRefreshAccountId.Text);
            //DateTime CreateTime = DateTime.Parse(this.txtCreateTime.Text);
            //DateTime RefreshTime = DateTime.Parse(this.txtRefreshTime.Text);


            //XinYiOffice.Model.ClientTracking model = new XinYiOffice.Model.ClientTracking();
            //model.Id = Id;
            //model.ClientHeat = ClientHeat;
            //model.SalesOpportunitiesId = SalesOpportunitiesId;
            //model.SalesOpportunitiesSate = SalesOpportunitiesSate;
            //model.BackNotes = BackNotes;
            //model.Sate = Sate;
            //model.IsFollowUp = IsFollowUp;
            //model.CloseNotes = CloseNotes;
            //model.ReturnAccountId = ReturnAccountId;
            //model.ActualTime = ActualTime;
            //model.PlanTime = PlanTime;
            //model.CreateAccountId = CreateAccountId;
            //model.RefreshAccountId = RefreshAccountId;
            //model.CreateTime = CreateTime;
            //model.RefreshTime = RefreshTime;

            //XinYiOffice.BLL.ClientTracking bll = new XinYiOffice.BLL.ClientTracking();
            //bll.Update(model);
            //xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
