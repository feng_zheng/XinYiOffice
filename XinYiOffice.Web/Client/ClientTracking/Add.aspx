﻿<%@ Page Language="C#"  AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="XinYiOffice.Web.Client.ClientTracking.Add" Title="增加页" %>

<form  id="f2" name="f2" runat="server">
    <table style="width: 100%;" cellpadding="2" cellspacing="1" class="border">
        <tr>
            <td class="tdbg">
                
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right">
		当前客户热度
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtClientHeat" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		销售机会Id
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtSalesOpportunitiesId" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		销售机会阶段 状态
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtSalesOpportunitiesSate" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		回访备注
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtBackNotes" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		回访状态 未回访
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtSate" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		是否下步跟进 1-是,0-否
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtIsFollowUp" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		不跟进原因 (关闭原因)
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtCloseNotes" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		回访人
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtReturnAccountId" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		实际回访时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox ID="txtActualTime" runat="server" Width="70px"  onfocus="setday(this)"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		计划回访时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox ID="txtPlanTime" runat="server" Width="70px"  onfocus="setday(this)"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		添加者id
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtCreateAccountId" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		更新者id
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtRefreshAccountId" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		创建时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox ID="txtCreateTime" runat="server" Width="70px"  onfocus="setday(this)"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right">
		更新时间 最近一次编辑的人员I
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox ID="txtRefreshTime" runat="server" Width="70px"  onfocus="setday(this)"></asp:TextBox>
	</td></tr>
</table>
<script src="/js/calendar1.js" type="text/javascript"></script>

            </td>
        </tr>
        <tr>
            <td class="tdbg" align="center" valign="bottom">
                <asp:Button ID="btnSave" runat="server" Text="保存"
                    OnClick="btnSave_Click" class="inputbutton" onmouseover="this.className='inputbutton_hover'"
                    onmouseout="this.className='inputbutton'"></asp:Button>
                <asp:Button ID="btnCancle" runat="server" Text="取消"
                    OnClick="btnCancle_Click" class="inputbutton" onmouseover="this.className='inputbutton_hover'"
                    onmouseout="this.className='inputbutton'"></asp:Button>
            </td>
        </tr>
    </table>
    <br />
    </form>