﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Client.ClientInfoExpandValue
{
    public partial class Modify : BasicPage
    {       

        		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
				{
					int Id=(Convert.ToInt32(Request.Params["id"]));
					ShowInfo(Id);
				}
			}
		}
			
	private void ShowInfo(int Id)
	{
		XinYiOffice.BLL.ClientInfoExpandValue bll=new XinYiOffice.BLL.ClientInfoExpandValue();
		XinYiOffice.Model.ClientInfoExpandValue model=bll.GetModel(Id);
		this.lblId.Text=model.Id.ToString();
		this.txtClientInfoExpandId.Text=model.ClientInfoExpandId.ToString();
		this.txtValue.Text=model.Value;
		this.txtClientInfoId.Text=model.ClientInfoId.ToString();
		this.txtCreateAccountId.Text=model.CreateAccountId.ToString();
		this.txtRefreshAccountId.Text=model.RefreshAccountId.ToString();
		this.txtCreateTime.Text=model.CreateTime.ToString();
		this.txtRefreshTime.Text=model.RefreshTime.ToString();

	}

		public void btnSave_Click(object sender, EventArgs e)
		{
			
            //string strErr="";
            //if(!PageValidate.IsNumber(txtClientInfoExpandId.Text))
            //{
            //    strErr+="扩展id格式错误！\\n";	
            //}
            //if(this.txtValue.Text.Trim().Length==0)
            //{
            //    strErr+="扩展值不能为空！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtClientInfoId.Text))
            //{
            //    strErr+="关联客户id格式错误！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtCreateAccountId.Text))
            //{
            //    strErr+="添加者id格式错误！\\n";	
            //}
            //if(!PageValidate.IsNumber(txtRefreshAccountId.Text))
            //{
            //    strErr+="更新者id格式错误！\\n";	
            //}
            //if(!PageValidate.IsDateTime(txtCreateTime.Text))
            //{
            //    strErr+="创建时间格式错误！\\n";	
            //}
            //if(!PageValidate.IsDateTime(txtRefreshTime.Text))
            //{
            //    strErr+="更新时间 最近一次编辑的人员I格式错误！\\n";	
            //}

            //if(strErr!="")
            //{
            //    MessageBox.Show(this,strErr);
            //    return;
            //}
			int Id=int.Parse(this.lblId.Text);
			int ClientInfoExpandId=int.Parse(this.txtClientInfoExpandId.Text);
			string Value=this.txtValue.Text;
			int ClientInfoId=int.Parse(this.txtClientInfoId.Text);
			int CreateAccountId=int.Parse(this.txtCreateAccountId.Text);
			int RefreshAccountId=int.Parse(this.txtRefreshAccountId.Text);
			DateTime CreateTime=DateTime.Parse(this.txtCreateTime.Text);
			DateTime RefreshTime=DateTime.Parse(this.txtRefreshTime.Text);


			XinYiOffice.Model.ClientInfoExpandValue model=new XinYiOffice.Model.ClientInfoExpandValue();
			model.Id=Id;
			model.ClientInfoExpandId=ClientInfoExpandId;
			model.Value=Value;
			model.ClientInfoId=ClientInfoId;
			model.CreateAccountId=CreateAccountId;
			model.RefreshAccountId=RefreshAccountId;
			model.CreateTime=CreateTime;
			model.RefreshTime=RefreshTime;
            model.TenantId = CurrentTenantId;
			XinYiOffice.BLL.ClientInfoExpandValue bll=new XinYiOffice.BLL.ClientInfoExpandValue();
			bll.Update(model);
			xytools.web_alert("保存成功！","list.aspx");

		}


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }
    }
}
