﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Client.ClientLiaisons
{
    public partial class Modify : BasicPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }
        }


        protected void InitData()
        {
            txtPersonNumber.Text = string.Empty;
            DropDownList_Province.DataSource = AppDataCacheServer.Province;
            DropDownList_Province.DataTextField = "Name";
            DropDownList_Province.DataValueField = "Id";
            DropDownList_Province.DataBind();

            txtPersonNumber.Text = "CLL" + DateTime.Now.ToString("yyyyMMddmmss");
            SetDropDownList("Power", ref DropDownList_Power);
            SetDropDownList("Source", ref DropDownList_Source);
            SetDropDownList("IfContact", ref DropDownList_IfContact);
            SetDropDownList("ClassiFication", ref DropDownList_ClassiFication);
            SetDropDownList("PrimaryContact", ref DropDownList_PrimaryContact);
            SetDropDownList("Sex", ref DropDownList_Sex);

        }

        protected void SetDropDownList(string fieldName, ref DropDownList ddl)
        {
            DataRow[] dr = AppDataCacheServer.ClientLiaisonsDictionaryTable().Select(string.Format("FieldName='{0}'", fieldName));
            foreach (DataRow _dr in dr)
            {
                ddl.Items.Add(new ListItem(SafeConvert.ToString(_dr["DisplayName"]), SafeConvert.ToString(_dr["Value"])));
            }
        }

        private void ShowInfo(int Id)
        {
            InitData();
            XinYiOffice.BLL.ClientLiaisons bll = new XinYiOffice.BLL.ClientLiaisons();
            XinYiOffice.Model.ClientLiaisons model = bll.GetModel(Id);

            this.lblId.Text = model.Id.ToString();

            try
            {
                this.txtByClientInfoId.Text = new BLL.ClientInfo().GetModel(SafeConvert.ToInt(model.ClientInfoId)).Name;
                HiddenField_ByClientInfoId.Value = model.ClientInfoId.ToString();
            }
            catch(Exception ex)
            {
                EasyLog.WriteLog(ex);
            }

            this.txtPersonNumber.Text = model.PersonNumber;
            this.txtFullName.Text = model.FullName;
            DropDownList_Power.SelectedValue = model.Power.ToString();
            this.txtPhone.Text = model.Phone;
            this.txtOtherPhone.Text = model.OtherPhone;
            this.txtFax.Text = model.Fax;
            this.txtEmail.Text = model.Email;
            this.txtQQ.Text = model.QQ;
            this.txtBirthday.Text = model.Birthday;


            try
            {
                this.txtByAccountId.Text = AccountsServer.GetFullNameByAccountId(SafeConvert.ToInt(model.ByAccountId));
                HiddenField_ByAccountId.Value = model.ByAccountId.ToString();
            }
            catch(Exception ex)
            {
                EasyLog.WriteLog(ex);
            }

            this.txtDepartment.Text = model.Department;
            this.txtFUNCTION.Text = model.FunctionName;
            this.txtDirectSuperior.Text = SafeConvert.ToString(model.DirectSuperior);
            this.DropDownList_Source.SelectedValue = model.Source.ToString();

            DropDownList_IfContact.SelectedValue = model.IfContact.ToString();
            DropDownList_ClassiFication.SelectedValue = model.ClassiFication.ToString();
            this.txtBusiness.Text = model.Business;
            DropDownList_PrimaryContact.SelectedValue = model.PrimaryContact.ToString();
            this.txtShortPhone.Text = model.ShortPhone;
            DropDownList_Sex.SelectedValue = model.Sex.ToString();
            this.txtCountriesRegions.Text = model.CountriesRegions;
            this.txtZip.Text = model.Zip;
            this.txtAddress.Text = model.Address;

            DropDownList_Province.SelectedValue = model.Province.ToString();

            DropDownList_County.DataSource = AppDataCacheServer.GetCityByPid(SafeConvert.ToInt(model.Province));
            DropDownList_County.DataTextField = "Name";
            DropDownList_County.DataValueField = "Id";
            DropDownList_County.DataBind();

            DropDownList_County.SelectedValue = model.County.ToString();
            HiddenField_county.Value = model.County.ToString();

            this.txtRemarks.Text = model.Remarks;


        }



        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {

            int Id = int.Parse(this.lblId.Text);
            int ClientInfoId = int.Parse(this.HiddenField_ByClientInfoId.Value);
            string PersonNumber = this.txtPersonNumber.Text;
            string FullName = this.txtFullName.Text;
            int Power = SafeConvert.ToInt(DropDownList_Power.SelectedValue);
            string Phone = this.txtPhone.Text;
            string OtherPhone = this.txtOtherPhone.Text;
            string Fax = this.txtFax.Text;
            string Email = this.txtEmail.Text;
            string QQ = this.txtQQ.Text;
            string Birthday = this.txtBirthday.Text;
            int ByAccountId = SafeConvert.ToInt(HiddenField_ByAccountId.Value);
            string Department = this.txtDepartment.Text;
            string FUNCTION = this.txtFUNCTION.Text;
            string DirectSuperior = this.txtDirectSuperior.Text;
            int Source = SafeConvert.ToInt(DropDownList_Source.SelectedValue);
            int IfContact = SafeConvert.ToInt(DropDownList_IfContact.SelectedValue);
            int ClassiFication = SafeConvert.ToInt(DropDownList_ClassiFication.SelectedValue);
            string Business = this.txtBusiness.Text;
            int PrimaryContact = SafeConvert.ToInt(DropDownList_PrimaryContact.SelectedValue);
            string ShortPhone = this.txtShortPhone.Text;
            int Sex = SafeConvert.ToInt(DropDownList_Sex.SelectedValue);
            string CountriesRegions = this.txtCountriesRegions.Text;
            string Zip = this.txtZip.Text;
            string Address = this.txtAddress.Text;
            int Province = SafeConvert.ToInt(DropDownList_Province.SelectedValue);
            int County = SafeConvert.ToInt(DropDownList_County.SelectedValue);
            string Remarks = this.txtRemarks.Text;
            int CreateAccountId = CurrentAccountId;
            int RefreshAccountId = CurrentAccountId;

            DateTime RefreshTime = DateTime.Now;


            XinYiOffice.Model.ClientLiaisons model = new XinYiOffice.Model.ClientLiaisons();
            model.Id = Id;
            model.ClientInfoId = ClientInfoId;
            model.PersonNumber = PersonNumber;
            model.FullName = FullName;
            model.Power = Power;
            model.Phone = Phone;
            model.OtherPhone = OtherPhone;
            model.Fax = Fax;
            model.Email = Email;
            model.QQ = QQ;
            model.Birthday = Birthday;
            model.ByAccountId = ByAccountId;
            model.Department = Department;
            model.FunctionName = FUNCTION;
            model.DirectSuperior =  SafeConvert.ToInt(DirectSuperior);
            model.Source = Source;
            model.IfContact = IfContact;
            model.ClassiFication = ClassiFication;
            model.Business = Business;
            model.PrimaryContact = PrimaryContact;
            model.ShortPhone = ShortPhone;
            model.Sex = Sex;
            model.CountriesRegions = CountriesRegions;
            model.Zip = Zip;
            model.Address = Address;
            model.Province = Province;
            model.County = County;
            model.Remarks = Remarks;

            model.RefreshAccountId = RefreshAccountId;

            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;
            XinYiOffice.BLL.ClientLiaisons bll = new XinYiOffice.BLL.ClientLiaisons();
            bll.Update(model);
            xytools.web_alert("保存成功！", "list.aspx");

        }
    }
}
