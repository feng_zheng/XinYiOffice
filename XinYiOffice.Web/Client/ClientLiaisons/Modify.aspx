﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="Modify.aspx.cs" Inherits="XinYiOffice.Web.Client.ClientLiaisons.Modify" Title="修改页" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="/css/ui-lightness/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<script src="/js/jquery-ui-1.8.17.custom.min.js"></script>
<script src="/js/jquery-ui-widget-combobox.js"></script>


<script>
    $(function () {

        var autoClientInfoId = $("#<%=txtByClientInfoId.ClientID%>");
        var autoHiddenField_ClientInfoId = $("#<%=HiddenField_ByClientInfoId.ClientID%>");
        var url_getclient = "/Call/Ajax.aspx?action=getclientinfo&sj=" + Math.random();

        //客户
        autoClientInfoId.autocomplete({
            source: url_getclient,
            select: function (event, ui) {

                autoClientInfoId.val(ui.item.label);
                autoHiddenField_ClientInfoId.val(ui.item.value);
                return false;
            },
            focus: function (event, ui) {
                autoClientInfoId.val(ui.item.label);
                return false;
            }
        });

        //城市
        var DropDownList_Province = $("#<%=DropDownList_Province.ClientID%>");
        var DropDownList_City = $("#<%=DropDownList_County.ClientID%>");
        var HiddenField_City = $("#<%=HiddenField_county.ClientID%>");

        DropDownList_Province.click(function () {
            DropDownList_City.html("");
            $.getJSON("/Call/Ajax.aspx?action=getcity&pid=" + DropDownList_Province.val() + "&sj=" + Math.random(), function (json) {

                $.each(json.Data, function (i) {
                    DropDownList_City.append("<option value='" + json.Data[i].Id + "'>" + json.Data[i].Name + "</option>");
                });
            });

        });


        DropDownList_City.change(function () {
            HiddenField_City.val(DropDownList_City.val());
        });

        var autoAccountId = $("#<%=txtByAccountId.ClientID%>");
        var hidautoAccountId = $("#<%=HiddenField_ByAccountId.ClientID%>");
        var url_workaccount = "/Call/Ajax.aspx?action=getofficeworkeraccount&sj=" + Math.random();
        autoAccountId.autocomplete({
            source: url_workaccount,
            select: function (event, ui) {

                autoAccountId.val(ui.item.label);
                hidautoAccountId.val(ui.item.value);
                return false;
            },
            focus: function (event, ui) {
                autoAccountId.val(ui.item.label);
                return false;
            }
        });

    });
</script>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form id="f2" name="f2" runat="server">
    <div class="setup_box" style=" margin-bottom:0">
     	<div class="h_title"><h4>联系人修改</h4></div>
        
<table width="100%" border="0" cellPadding="0" cellSpacing="0" class="plan_table">
	<tr>
	<td width="17%"  align="right" class="table_left">
		编号
	：</td>
	<td width="30%" align="left">
		<asp:label id="lblId" runat="server"></asp:label>
	</td>
	<td width="22%" align="left" class="table_left">联系人编号
	：</td>
	<td width="31%" align="left"><asp:TextBox id="txtPersonNumber" runat="server" Width="200px"></asp:TextBox></td>
	</tr>
	<tr>
	<td  align="right" class="table_left">联系人姓名
	：</td>
	<td align="left"><asp:TextBox id="txtFullName" runat="server" Width="200px"></asp:TextBox></td>
	<td align="left" class="table_left">权限
	：</td>
	<td align="left"><asp:DropDownList ID="DropDownList_Power" runat="server"> </asp:DropDownList></td>
	</tr>
	<tr>
	<td  align="right" class="table_left">手机
	：</td>
	<td align="left"><asp:TextBox id="txtPhone" runat="server" Width="200px"></asp:TextBox></td>
	<td align="left" class="table_left">其他电话
	：</td>
	<td align="left"><asp:TextBox id="txtOtherPhone" runat="server" Width="200px"></asp:TextBox></td>
	</tr>
	<tr>
	<td  align="right" class="table_left">传真
	：</td>
	<td align="left"><asp:TextBox id="txtFax" runat="server" Width="200px"></asp:TextBox></td>
	<td align="left" class="table_left">电子邮件
	：</td>
	<td align="left"><asp:TextBox id="txtEmail" runat="server" Width="200px"></asp:TextBox></td>
	</tr>
	<tr>
	<td  align="right" class="table_left">QQ
	：</td>
	<td align="left"><asp:TextBox id="txtQQ" runat="server" Width="200px"></asp:TextBox></td>
	<td align="left" class="table_left">所属职员
	：</td>
	<td align="left"><asp:TextBox id="txtByAccountId" 
            runat="server" Width="200px"></asp:TextBox>
        <asp:HiddenField ID="HiddenField_ByAccountId" runat="server" />
        </td>
	</tr>
	<tr>
	<td  align="right" class="table_left">生日
	：</td>
	<td align="left"><asp:TextBox id="txtBirthday" runat="server" Width="200px"></asp:TextBox></td>
	<td align="left" class="table_left">职务
	：</td>
	<td align="left"><asp:TextBox id="txtFUNCTION" runat="server" Width="200px"></asp:TextBox></td>
	</tr>
	<tr>
	<td  align="right" class="table_left">所属客户
	：</td>
	<td align="left"><asp:TextBox id="txtByClientInfoId" runat="server" Width="200px"></asp:TextBox>
        <asp:HiddenField ID="HiddenField_ByClientInfoId" runat="server" />
        </td>
	<td align="left" class="table_left">来源
	：</td>
	<td align="left"><asp:DropDownList ID="DropDownList_Source" runat="server"> </asp:DropDownList></td>
	</tr>
	<tr>
	<td  align="right" class="table_left">部门
	：</td>
	<td align="left"><asp:TextBox id="txtDepartment" runat="server" Width="200px"></asp:TextBox></td>
	<td align="left" class="table_left">联系人分类
	：</td>
	<td align="left"><asp:DropDownList ID="DropDownList_ClassiFication" runat="server"> </asp:DropDownList></td>
	</tr>
	<tr>
	<td  align="right" class="table_left">直属上级
	：</td>
	<td align="left"><asp:TextBox id="txtDirectSuperior" runat="server" Width="200px"></asp:TextBox></td>
	<td align="left" class="table_left"><span class="style1">主要联系人
	：</span></td>
	<td align="left"><span class="style1">
	  <asp:DropDownList ID="DropDownList_PrimaryContact" runat="server"> </asp:DropDownList>
	</span></td>
	</tr>
	<tr>
	<td  align="right" class="table_left">是否可以联系
	：</td>
	<td colspan="3" align="left"><asp:DropDownList ID="DropDownList_IfContact" runat="server" style=" width:100px;"></asp:DropDownList></td>
	</tr>
	<tr>
	<td height="20"  align="right" class="table_left">负责业务
	：</td>
	<td colspan="3" align="left"><asp:TextBox id="txtBusiness" runat="server" Width="200px"></asp:TextBox></td>
	</tr>
	</table>
    
    <div class="h_title"><h4>联系信息</h4></div>
    
<table width="100%" border="0" cellPadding="0" cellSpacing="0" class="plan_table">
  <tr>
    <td  align="right" class="table_left">接受短信手机
      ：</td>
    <td width="0" align="left" class="style1"><asp:TextBox id="txtShortPhone" runat="server" Width="200px"></asp:TextBox></td>
    <td width="0" align="left" class="table_left">省份
      ：</td>
    <td width="0" align="left" class="style1"><asp:DropDownList ID="DropDownList_Province" runat="server"> </asp:DropDownList></td>
  </tr>
  <tr>
    <td height="25"  align="right" class="table_left">性别
      ：</td>
    <td width="0" height="25" align="left"><asp:DropDownList ID="DropDownList_Sex" runat="server"> </asp:DropDownList></td>
    <td width="0" height="25" align="left" class="table_left">区县
      ：</td>
    <td width="0" height="25" align="left"><asp:DropDownList ID="DropDownList_County" runat="server"> </asp:DropDownList>
        <asp:HiddenField ID="HiddenField_county" runat="server" />
      </td>
  </tr>
  <tr>
    <td height="25"  align="right" class="table_left"> 国家地区
      ：</td>
    <td width="0" height="25" colspan="3" align="left"><asp:TextBox id="txtCountriesRegions" runat="server" Width="200px"></asp:TextBox></td>
  </tr>
  <tr>
    <td height="25"  align="right" class="table_left"> 邮编
      ：</td>
    <td width="*" height="25" colspan="3" align="left"><asp:TextBox id="txtZip" runat="server" Width="200px"></asp:TextBox></td>
  </tr>
  <tr>
    <td height="25"  align="right" class="table_left"> 地址
      ：</td>
    <td width="*" height="25" colspan="3" align="left"><asp:TextBox id="txtAddress" runat="server" Width="200px"></asp:TextBox></td>
  </tr>
  <tr>
    <td height="25"  align="right" class="table_left">备注:</td>
    <td width="*" height="25" colspan="3" align="left"><asp:TextBox ID="txtRemarks" runat="server"></asp:TextBox></td>
  </tr>
</table>

 </div>
 
          <div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
         <a href="javascript:history.go(-1);" class="cancel">取消</a>
        
    </div>   
</form>
</asp:Content>



