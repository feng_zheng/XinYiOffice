﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"   AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="XinYiOffice.Web.Client.SalesOpportunities.Show" Title="显示页" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
<link href="/css/marketingplan.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form id="f2" name="f2" runat="server">

<div class="setup_box">

<div class="h_title"><h4>机会名称:<asp:Label id="lblOppName" runat="server"></asp:Label></h4></div>

<table cellSpacing="0" cellPadding="0" width="100%" border="0" class="plan_table">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		编号
	：</td>
	<td width="0" height="25" colspan="3" align="left">
		<asp:Label id="lblId" runat="server"></asp:Label>
	</td></tr>

	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		所属客户
	：</td>
	<td height="25" width="0" align="left">
		<asp:Label id="lblCustomerService" runat="server"></asp:Label>
	</td>
	<td width="0" align="left" class="table_left"><span class="table_left">所属人
	：</span></td>
	<td width="*" align="left"><asp:Label id="lblPersonalAccountId" runat="server"></asp:Label></td>
	</tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">发生时间
      ：</td>
	<td width="0" height="25" align="left"><asp:Label id="lblOccTime" runat="server"></asp:Label>&nbsp;</td>
	<td width="0" height="25" align="left" class="table_left"><span class="table_left">来源
	：</span></td>
	<td width="0" height="25" align="left"><asp:Label id="lblSource" runat="server"></asp:Label></td>
	</tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		营销计划
	：</td>
	<td width="*" height="25" colspan="3" align="left">
		<asp:Label id="lblMarketingPlanId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">客户需求描述
	：</td>
	<td width="*" height="25" colspan="3" align="left"><asp:Label id="lblRequirement" runat="server"></asp:Label></td></tr>
</table>

<div class="h_title"><h4>预期</h4></div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="plan_table">
  <tr>
    <td height="25" width="30%" align="right" class="table_left"> 预计金额
      ：</td>
    <td height="25" colspan="3" align="left"><asp:Label id="lblEstAmount" runat="server"></asp:Label></td>
  </tr>
  <tr>
    <td height="25" align="right" class="table_left">预计确定日期
      ：</td>
    <td width="32%" height="25" align="left"><asp:Label id="lblEstDate" runat="server"></asp:Label>&nbsp;</td>
    <td width="11%" align="left" class="table_left"><span>外币备注
      ：</span></td>
    <td width="27%" align="left"><asp:Label id="lblCurrencyNotes" runat="server"></asp:Label>&nbsp;</td>
  </tr>
</table>
<div class="h_title"><h4>当前状态</h4></div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="plan_table">
  <tr>
    <td height="25" width="30%" align="right" class="table_left"> 目前阶段
      ：</td>
    <td height="25" width="0" align="left"><asp:Label id="lblPresentStage" runat="server"></asp:Label></td>
    <td width="0" align="left" class="table_left"><span>可能性
      ：</span></td>
    <td width="*" align="left"><asp:Label id="lblFeasibility" runat="server"></asp:Label></td>
  </tr>
  <tr>
    <td height="25" width="30%" align="right" class="table_left"> 下一步
      ：</td>
    <td width="0" height="25" colspan="3" align="left"><asp:Label id="lblNextStep" runat="server"></asp:Label></td>
  </tr>
  <tr>
    <td height="25" width="30%" align="right" class="table_left"> 状态
      ：</td>
    <td width="*" height="25" colspan="3" align="left"><asp:Label id="lblSate" runat="server"></asp:Label></td>
  </tr>
  <tr>
    <td height="25" width="30%" align="right" class="table_left"> 阶段备注
      ：</td>
    <td width="*" height="25" colspan="3" align="left"><asp:Label id="lblStageRemarks" runat="server"></asp:Label></td>
  </tr>
</table>

<div class="h_title"><h4>当前状态</h4></div>
<table width="100%" border="0" cellPadding="0" cellSpacing="0" class="plan_table">
  <tr>
    <td height="25" width="30%" align="right" class="table_left">添加者id
      ：</td>
    <td height="25" width="0" align="left"><asp:Label id="lblCreateAccountId" runat="server"></asp:Label></td>
    <td width="0" align="left" class="table_left"><span>更新者id
      ：</span></td>
    <td width="*" align="left"><asp:Label id="lblRefreshAccountId" runat="server"></asp:Label></td>
  </tr>
  <tr>
    <td height="25" width="30%" align="right" class="table_left">创建时间
      ：</td>
    <td width="0" height="25" align="left"><asp:Label id="lblCreateTime" runat="server"></asp:Label></td>
    <td width="0" height="25" align="left" class="table_left"><span>更新时间 ：</span></td>
    <td width="0" height="25" align="left"><asp:Label id="lblRefreshTime" runat="server"></asp:Label></td>
  </tr>
</table>

</div>

      <div class="clear btn_box">
      <a href="javascript:history.go(-1);;" class="cancel">返回</a>
      </div>

            </form>

</asp:Content>





