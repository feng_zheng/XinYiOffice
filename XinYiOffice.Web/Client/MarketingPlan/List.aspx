﻿<%@ Page Title="客户_营销计划" MasterPageFile="~/BasicContent.Master" Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" EnableViewState="false" Inherits="XinYiOffice.Web.Client.MarketingPlan.List" %>

<%@ Register src="../../InitControl/SearchApp.ascx" tagname="SearchApp" tagprefix="uc1" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
<link rel="stylesheet" href="/css/pagecss.css">
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<div class="marketing_box">
    	<div class="btn_search_box">
        	<div class="clear cc" style=" padding-left:15px;">
                <div class="btn clear">
                
<%if (ValidatePermission("MARKETINGPLAN_LIST"))
              { %>
            <a href="javascript:;" id="search_a"  class="sj">条件查询</a>
            <%} %>
            <%if (ValidatePermission("MARKETINGPLAN_LIST_ADD"))
              { %>
            <a href="add.aspx">添加</a>
            <%} %>
            <%if (ValidatePermission("MARKETINGPLAN_LIST_EDIT"))
              { %>
            <a href="javascript:;" class="edit">修改</a>
            <%} %>
             <%if (ValidatePermission("MARKETINGPLAN_LIST_DEL"))
              { %>
            <a href="javascript:;" class="tdel">删除</a>
            <%} %>

                </div>
                <div class="del clear"></div>
            </div>
            <form id="f2" >
            <div class="show_search_box" style="display:none;">
        		<div class="tjss clear">


                    <uc1:SearchApp ID="SearchApp1" runat="server" TableName="vMarketingPlan"/>

                </div>
                <div class="clear btn_box btn_box_p">
<a href="javascript:;" class="save btn_search">查询</a>
<a href="javascript:;" class="cancel res_search">取消</a>
              
                </div>
        	</div>
            </form>
        </div>
        <div class="marketing_table">
        	<table cellpadding="0" cellspacing="0" id="MarketingPlan_table">
            	
                
                <asp:Repeater ID="repMarketingPlan" runat="server" >
                <HeaderTemplate>
                <tr><th><input type="checkbox" id="cb_all"></th><th>计划名称</th><th>状态</th><th>开始日期</th><th>结束日期</th><th>类型</th><th>预期收入</th><th>预算成本</th><th>计划所有人</th></tr>
                </HeaderTemplate>
                
                <ItemTemplate>
                <tr><td class="center"><input type="checkbox" name="chk_list" va="<%#DataBinder.Eval(Container.DataItem, "Id")%>"></td><td><a href="Show.aspx?Id=<%#DataBinder.Eval(Container.DataItem, "Id")%>"> <%#Eval("ProgramName").ToString().Length > 40 ? Eval("ProgramName").ToString().Substring(0, 40) + "..." : Eval("ProgramName")%> </a></td><td><%#DataBinder.Eval(Container.DataItem, "SateName")%></td><td><%#DataBinder.Eval(Container.DataItem, "StartTime","{0:yyyy-MM-dd}")%></td><td><%#DataBinder.Eval(Container.DataItem, "EndTime", "{0:yyyy-MM-dd}")%></td><td><%#DataBinder.Eval(Container.DataItem, "TypeName")%></td><td><span style="color:Red;"><%#DataBinder.Eval(Container.DataItem, "AnticipatedRevenue")%></span></td><td><span style="color:Green;"><%#DataBinder.Eval(Container.DataItem, "BudgetCost")%></span></td><td><%#DataBinder.Eval(Container.DataItem, "SchemerName")%></td></tr>    
                </ItemTemplate>
                </asp:Repeater>

                

            </table>
        </div>
    </div>
<%=pagehtml%>
<script>
        $('#MarketingPlan_table td').hover(function () {
            $(this).parent().find('td').css('background', '#f0f8fc')
        }, function () {
            $(this).parent().find('td').css('background', 'none')
        });
</script>
</asp:Content>