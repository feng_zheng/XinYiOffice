﻿<%@ Page Language="C#"  AutoEventWireup="true" MasterPageFile="~/BasicContent.Master"  CodeBehind="Add.aspx.cs" Inherits="XinYiOffice.Web.Client.MarketingPlan.Add" Title="增加页" %>

<asp:Content ID="he" ContentPlaceHolderID="head" runat="server">
<link href="/css/marketingplan.css" rel="stylesheet" type="text/css" />

<link href="/css/ui-lightness/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<script src="/js/jquery-ui-1.8.17.custom.min.js"></script>
<script src="/js/jquery-ui-widget-combobox.js"></script>

<script type="text/javascript">
    $(function () {

        var SchemerAccountId = $("#<%=txtSchemerAccountId.ClientID%>");
        var hidSchemerAccountId = $("#<%=HiddenField_SchemerAccountId.ClientID%>");

        var ExecuteAccountId = $("#<%=txtExecuteAccountId.ClientID%>");
        var hidExecuteAccountId = $("#<%=HiddenField_ExecuteAccountId.ClientID%>");

        var datas1;
        //objects为json数据源对象
        var url = "/Call/Ajax.aspx?action=getofficeworkeraccount&sj=" + Math.random();

        SchemerAccountId.autocomplete({
            source: url,
            select: function (event, ui) {

                SchemerAccountId.val(ui.item.label);
                hidSchemerAccountId.val(ui.item.value);
                return false;
            },
            focus: function (event, ui) {
                SchemerAccountId.val(ui.item.label);
                return false;
            }
        });



        ExecuteAccountId.autocomplete({
            source: url,
            select: function (event, ui) {

                ExecuteAccountId.val(ui.item.label);
                hidExecuteAccountId.val(ui.item.value);
                return false;
            },
            focus: function (event, ui) {
                ExecuteAccountId.val(ui.item.label);
                return false;
            }
        });




    });
</script>
</asp:Content>

<asp:Content ID="cph" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <form  id="f2" name="f2" runat="server">

    <div class="setup_box" style=" margin-bottom:0">
    	<div class="h_title"><h4>营销计划信息</h4></div>
        <table cellpadding="0" cellspacing="0" class="plan_table">
        	<tr><td class="table_left">计划所有人：</td><td><asp:TextBox id="txtSchemerAccountId" runat="server" Width="200px"></asp:TextBox>
	    (默认为当前登录用户)<asp:HiddenField ID="HiddenField_SchemerAccountId" runat="server" /></td><td class="table_left">计划执行人：</td><td><asp:TextBox id="txtExecuteAccountId" runat="server" Width="200px"></asp:TextBox>
	    (默认为当前登录用户)<asp:HiddenField ID="HiddenField_ExecuteAccountId" runat="server" /></td></tr>
            <tr><td class="table_left">计划名称：</td><td><asp:TextBox id="txtProgramName" runat="server" Width="200px"></asp:TextBox></td><td class="table_left">状态：</td><td><asp:DropDownList ID="DropDownList_Sate" runat="server">
        </asp:DropDownList></td></tr>
            <tr><td class="table_left">类型：</td><td><asp:DropDownList ID="DropDownList_TYPE" runat="server">
        </asp:DropDownList></td><td class="table_left">&nbsp;</td><td>&nbsp;</td></tr> 
            <tr><td class="table_left">开始日期：</td><td><asp:TextBox ID="txtStartTime" runat="server" Width="143px"  
            class="Wdate"  onClick="WdatePicker()"></asp:TextBox></td><td class="table_left">结束日期：</td><td><asp:TextBox ID="txtEndTime" runat="server" Width="143px"  
            class="Wdate"  onClick="WdatePicker()"></asp:TextBox></td></tr> 
        </table>
        <div class="h_title"><h4>预期</h4></div>
        <table cellpadding="0" cellspacing="0" class="plan_table">
        	<tr><td class="table_left">预期收入：</td><td><asp:TextBox id="txtAnticipatedRevenue" runat="server" Width="200px">10000</asp:TextBox></td><td class="table_left">预算成本：</td><td><asp:TextBox id="txtBudgetCost" runat="server" Width="200px">10000</asp:TextBox></td></tr>
        </table>
        <div class="h_title"><h4>描述信息</h4></div>
        <table cellpadding="0" cellspacing="0" class="plan_table">
        	<tr><td class="table_left">描述：</td><td style=" width:auto">
                <asp:TextBox id="txtProgramDescription" runat="server" Width="539px" 
            Height="95px" TextMode="MultiLine"></asp:TextBox><br />
            </td></tr>
            </table>
        
    </div>
    <div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
         <a href="javascript:history.go(-1);" class="cancel">取消</a>
        
    </div>

    </form>
</asp:Content>