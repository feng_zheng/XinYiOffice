﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="XinYiOffice.Web.Sys.Accounts.Add" Title="增加页" %>


<asp:Content ContentPlaceHolderID="head" runat="server">
<script src="/js/ui/jquery.ui.core.js"></script>
	<script src="/js/ui/jquery.ui.widget.js"></script>
	<script src="/js/ui/jquery.ui.position.js"></script>
	<script src="/js/ui/jquery.ui.autocomplete.js"></script>
	<link rel="stylesheet" href="/js/themes/base/jquery.ui.all.css">
    <link rel="stylesheet" href="/js/themes/base/jquery.ui.autocomplete.css">

    <style type="text/css">
        .style1
        {
            height: 36px;
        }
    </style>

    <script>
        var txtKeyId = $("#<%=txtKeyId.ClientID%>");
        var url_get = "/Call/Ajax.aspx?action=getofficeworker&sj=" + Math.random();

        $(function () {


            var HiddenField_KeyId = $("#<%=HiddenField_KeyId.ClientID%>");

            var ddlAccountType = $("#<%=DropDownList_AccountType.ClientID%>");
            var DropDownList_HeadPortrait = $("#<%=DropDownList_HeadPortrait.ClientID%>");
            var Image1 = $("#<%=Image1.ClientID%>");

            ddlAccountType.change(function () {
                var _va = ddlAccountType.val();
                //alert(_va);
                if (_va == "1") {
                    url_get = "/Call/Ajax.aspx?action=getofficeworker&sj=" + Math.random(); //公司内部职员
                }
                else if (_va == "2") {
                    url_get = "/Call/Ajax.aspx?action=getclientinfo&sj=" + Math.random(); //客户列表
                }
                else if (_va == "3") {
                    url_get = "/Call/Ajax.aspx?action=getotherofficeworker&sj=" + Math.random(); //合作伙伴
                }

                InitInput();
            });

            DropDownList_HeadPortrait.change(function () {
                var _va = DropDownList_HeadPortrait.val();
                Image1.attr("src","/img/face/"+_va+".png");
            });

        });

        function InitInput () {

            //客户
            txtKeyId.autocomplete({
                source: url_get,
                select: function (event, ui) {
                    //alert(ui.item.value);
                    txtKeyId.val(ui.item.label);
                    HiddenField_KeyId.val(ui.item.value);
                    return false;
                },
                focus: function (event, ui) {
                    txtKeyId.val(ui.item.label);
                    return false;
                }
            });

        }
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<form  id="f2" name="f2" runat="server">
    <div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>新建帐号</h4></div>
 
    <table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		用户名：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtAccountName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		密码：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtAccountPassword" runat="server" Width="200px">888</asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		状态：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_Sate" runat="server">
            <asp:ListItem Value="1">正常</asp:ListItem>
            <asp:ListItem Value="2">未激活</asp:ListItem>
            <asp:ListItem Value="3">异常</asp:ListItem>
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		昵称：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtNiceName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		姓名：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtFullName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		英文名：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtEnName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		时区：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_TimeZone" runat="server">
            <asp:ListItem Value="1">中国</asp:ListItem>
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		Email：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtEmail" runat="server" Width="200px">@</asp:TextBox>
	</td></tr>
	<tr>
	<td width="30%" align="right" class="table_left" style="height: 36px">
		头像选择：</td>
	<td width="*" align="left" class="style1">
		<asp:DropDownList ID="DropDownList_HeadPortrait" runat="server">
            <asp:ListItem>1</asp:ListItem>
            <asp:ListItem>2</asp:ListItem>
            <asp:ListItem>3</asp:ListItem>
            <asp:ListItem>4</asp:ListItem>
            <asp:ListItem>5</asp:ListItem>
            <asp:ListItem>6</asp:ListItem>
            <asp:ListItem>7</asp:ListItem>
            <asp:ListItem>8</asp:ListItem>
            <asp:ListItem>9</asp:ListItem>
            <asp:ListItem>10</asp:ListItem>
            <asp:ListItem>11</asp:ListItem>
            <asp:ListItem>12</asp:ListItem>
        </asp:DropDownList>
        <asp:Image ID="Image1" runat="server" ImageUrl="/img/face/1.png" />
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		手机号码
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtPhone" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		帐号类型 ：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_AccountType" runat="server">
            <asp:ListItem Value="1">公司职员</asp:ListItem>
            <asp:ListItem Value="2">客户方面</asp:ListItem>
            <asp:ListItem Value="3">合作伙伴</asp:ListItem>
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		帐号关联：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtKeyId" runat="server" Width="200px"></asp:TextBox>
	    <asp:HiddenField ID="HiddenField_KeyId" runat="server" />
        (打.可选择客户，职员，或其他组织)</td></tr>
	</table>
    </div>

     <div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
         <%--<a href="javascript:history.go(-1);" class="cancel">取消</a>--%>
        
    </div>

    </form>
</asp:Content>
