﻿<%@ Page Language="C#"  MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="Show.aspx.cs" Inherits="XinYiOffice.Web.Sys.Accounts.Show" Title="显示页" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<form id="f2" name="f2" runat="server">
<div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>帐号信息</h4></div>
 
   <table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		编号
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		用户名
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblAccountName" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		密码
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblAccountPassword" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		状态
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblSate" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		昵称
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblNiceName" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		名字
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblRealName" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		姓氏
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblSurName" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		英文名
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblEnName" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		时区
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblTimeZone" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		Email
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblEmail" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		头像：</td>
	<td height="25" width="*" align="left">
		<asp:Image ID="Image1" runat="server" />
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		手机号码
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblPhone" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		所属帐号类型：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblAccountType" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		帐号关联：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblKeyId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		添加者id
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCreateAccountId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		更新者id
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblRefreshAccountId" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		创建时间
	：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblCreateTime" runat="server"></asp:Label>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		更新时间：</td>
	<td height="25" width="*" align="left">
		<asp:Label id="lblRefreshTime" runat="server"></asp:Label>
	</td></tr>
</table>
</div>
            </form>


</asp:Content>



