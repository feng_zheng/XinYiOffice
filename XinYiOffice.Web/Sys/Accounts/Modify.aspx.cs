﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Sys.Accounts
{
    public partial class Modify : BasicPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(ValidatePermission("ACCOUNT_MANAGE") && ValidatePermission(" ACCOUNT_LIST_EDIT")))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.Accounts bll = new XinYiOffice.BLL.Accounts();
            XinYiOffice.Model.Accounts model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.txtAccountName.Text = model.AccountName;
            this.txtAccountPassword.Text = model.AccountPassword;
            DropDownList_Sate .SelectedValue= model.Sate.ToString();
            this.txtNiceName.Text = model.NiceName;
            this.txtFullName.Text = model.FullName;
          
            this.txtEnName.Text = model.EnName;
            DropDownList_TimeZone.SelectedValue = model.TimeZone.ToString();
            this.txtEmail.Text = model.Email;

            DropDownList_HeadPortrait.SelectedValue = model.HeadPortrait;
            this.txtPhone.Text = model.Phone;
            DropDownList_AccountType.SelectedValue = model.AccountType.ToString();
            this.txtKeyId.Text = model.KeyId.ToString();

            HiddenField_KeyId.Value = model.KeyId.ToString();

            Image1.ImageUrl = string.Format("/img/face/{0}.png",model.HeadPortrait);
        }


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = int.Parse(this.lblId.Text);
            string AccountName = this.txtAccountName.Text;
            string AccountPassword = this.txtAccountPassword.Text;
            int Sate = SafeConvert.ToInt(DropDownList_Sate.SelectedValue);
            string NiceName = this.txtNiceName.Text;

            string EnName = this.txtEnName.Text;
            int TimeZone = SafeConvert.ToInt(DropDownList_TimeZone.SelectedValue);
            string Email = this.txtEmail.Text;
            string HeadPortrait = SafeConvert.ToString(DropDownList_HeadPortrait.SelectedValue);
            string Phone = this.txtPhone.Text;
            int AccountType = SafeConvert.ToInt(DropDownList_AccountType.SelectedValue,0);
            int KeyId = SafeConvert.ToInt(HiddenField_KeyId.Value,0);
            int CreateAccountId = CurrentAccountId;
            int RefreshAccountId = CurrentAccountId;
            DateTime CreateTime = DateTime.Now;
            DateTime RefreshTime = DateTime.Now;

            XinYiOffice.Model.Accounts model = new XinYiOffice.Model.Accounts();
            model.Id = Id;
            model.AccountName = AccountName;
            model.AccountPassword = AccountPassword;
            model.Sate = Sate;
            model.NiceName = NiceName;
            model.FullName = txtFullName.Text;

            model.EnName = EnName;
            model.TimeZone = TimeZone;
            model.Email = Email;
            model.HeadPortrait = HeadPortrait;
            model.Phone = Phone;
            model.AccountType = AccountType;
            model.KeyId = KeyId;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.Accounts bll = new XinYiOffice.BLL.Accounts();
            bll.Update(model);
            xytools.web_alert_new_url("保存成功！", "list.aspx");

        }
    }
}
