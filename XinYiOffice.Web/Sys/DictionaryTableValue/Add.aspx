﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="XinYiOffice.Web.Sys.DictionaryTableValue.Add" Title="增加页" %>


<asp:Content ContentPlaceHolderID="head" runat="server"></asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form  id="f2" name="f2" runat="server">
 <div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>新建项</h4></div>
 
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		所属组名 ：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtDictionaryTableId" runat="server" Width="200px"></asp:TextBox>
	    <asp:HiddenField ID="hidDictionaryTableId" runat="server" />
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		显示名称
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtDisplayName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		对应的值
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtValue" runat="server" Width="200px"></asp:TextBox>
	    (建议不做更改)</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		排序
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtSort" runat="server" Width="200px">1</asp:TextBox>
	</td></tr>
	</table>
    </div>

     <div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
         <a href="list.aspx" class="cancel">取消</a>
         </div>
    </form>
</asp:Content>

