﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Sys.SystemJournal
{
    public partial class Show : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int id = SafeConvert.ToInt(xytools.url_get("id"));

            if (!IsPostBack)
            {
                ShowBind(id);
            }
        }

        protected void ShowBind(int id)
        {
            Model.SystemJournal sj = new Model.SystemJournal();
            sj = new BLL.SystemJournal().GetModel(id);

            lblId.Text = sj.Id.ToString();
            lblType.Text = sj.TYPE.ToString();
            lblOperationContent.Text = sj.OperationContent;
            lblCurrentAccountId.Text = sj.CurrentAccountId.ToString();
            lblIPAddress.Text = sj.IPAddress;
            lblSystemInfo.Text = sj.SystemInfo;
            lblCreateTime.Text = sj.CreateTime.ToString();


        }
    }
}