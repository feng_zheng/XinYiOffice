﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master" AutoEventWireup="true"
    CodeBehind="Modify.aspx.cs" Inherits="XinYiOffice.Web.Sys.Roles.Modify" Title="修改页" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <style>
        h1, h2, h3, dt, ul, li
        {
            margin: 0px;
            padding: 0px;
        }
        .peritem
        {
            font-size: 12px;
            margin: 0px;
            padding: 0px;
            width: 800px;
            clear: both;
            height:auto;
			padding-left:15px;
			padding-top:10px;
        }
        .peritem h1
        {
            font-size: 16px;
            margin: 0px;
            padding: 0px;
            color: #333;
            font-weight: bold;
        }
        .peritem dt
        {
            float: left;
            list-style: none;
            width: 800px;
			padding-left:15px;
			padding-top:5px;
           
        }
        .peritem dt h2
        {
            font-size: 14px;
            font-weight: normal;
            margin: 0px;
            padding: 0px;
            width: 80px;
            clear: both;
        }
        .peritem dt ul
        {
            height: 30px;
            line-height: 30px;
			padding-left:15px;
        }
        .peritem dt ul h3
        {
            font-size: 12px;
            font-weight: normal;
            margin: 0px;
            padding: 0px;
            width: 85px;
            float: left;
        }
        .peritem dt ul li
        {
            float: left;
            list-style: none;
            margin-left: 10px;
            display: inline;
            height: 30px;
            line-height: 30px;
        }
		.peritem dt label
        {
			margin-left: 2px;

        }
		
		div:after {
    content:".";
    display:block;
    height:0;
    clear:both;
    visibility:hidden;
}
/*针对IE*/
div{zoom:1;}
#example-advanced tr td{ line-height:25px; height:25px; margin:0px; padding:0px;}
    </style>


<link rel="stylesheet" href="/css/jquery.treetable.theme.default.css" />
<link rel="stylesheet" href="/css/jquery.treetable.css" />
<script src="/js/vendor/jquery-ui.js"></script>
<script src="/js/jquery.treetable.js"></script>
 <script language="javascript">
     $(function () {
         //内容
         $(".peritem h1 span input[type=checkbox]").click(function () {

             var che = $(this).attr("checked");
             $(this).parents(".peritem").find("input[type=checkbox]").attr("checked", che);

         });

         //栏目
         $(".peritem dt h2 span input[type=checkbox]").click(function () {

             var che = $(this).attr("checked");
             $(this).parents("dt").find("input[type=checkbox]").attr("checked", che);

         });

         //栏目列表
         $(".peritem dt ul h3 span input[type=checkbox]").click(function () {

             var che = $(this).attr("checked");
             $(this).parents("ul").find("input[type=checkbox]").attr("checked", che);

         });

     })
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <form id="f2" name="f2" runat="server">
    <div class="setup_box" style="margin-bottom: 0">
        <div class="h_title">
            <h4>
                编辑角色</h4>
        </div>
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td height="25" width="30%" align="right" class="table_left">
                    角色id ：
                </td>
                <td height="25" width="*" align="left">
                    <asp:Label ID="lblId" runat="server"></asp:Label>
                    <asp:HiddenField ID="HiddenField_id" runat="server" />
                </td>
            </tr>
            <tr>
                <td height="25" width="30%" align="right" class="table_left">
                    角色名 ：
                </td>
                <td height="25" width="*" align="left">
                    <asp:TextBox ID="txtName" runat="server" Width="200px"></asp:TextBox>
                    <asp:HiddenField ID="txtCreateAccountId" runat="server" />
                    <asp:HiddenField ID="txtRefreshAccountId" runat="server" />
                    <asp:HiddenField ID="txtCreateTime" runat="server" />
                    <asp:HiddenField ID="txtRefreshTime" runat="server" />
                </td>
            </tr>
        </table>
        
        <div class="h_title"><h4>菜单权限</h4></div>
        <asp:Panel runat="server" ID="panMenuTree">
        <table id="example-advanced">
        <caption>
          <a href="#" onClick="jQuery('#example-advanced').treetable('expandAll'); return false;">展开</a>
          <a href="#" onClick="jQuery('#example-advanced').treetable('collapseAll'); return false;">收起</a>
        </caption>
        <thead>
        <tr>
          <th>菜单名称</th>
          </tr>
        </thead>
        <tbody runat="server" id="tbody_cell">

        </tbody>
      </table>

      <script>
          $("#example-advanced").treetable({ expandable: true });

          // Highlight selected row
          $("#example-advanced tbody tr").mousedown(function () {
              $("tr.selected").removeClass("selected");
              $(this).addClass("selected");
          });

          // Drag & Drop Example Code
          $("#example-advanced .file, #example-advanced .folder").draggable({
              helper: "clone",
              opacity: .75,
              refreshPositions: true, // Performance?
              revert: "invalid",
              revertDuration: 300,
              scroll: true
          });

          $("#example-advanced .folder").each(function () {
              $(this).parents("tr").droppable({
                  accept: ".file, .folder",
                  drop: function (e, ui) {
                      var droppedEl = ui.draggable.parents("tr");
                      $("#example-advanced").treetable("move", droppedEl.data("ttId"), $(this).data("ttId"));
                  },
                  hoverClass: "accept",
                  over: function (e, ui) {
                      var droppedEl = ui.draggable.parents("tr");
                      if (this != droppedEl[0] && !$(this).is(".expanded")) {
                          $("#example-advanced").treetable("expandNode", $(this).data("ttId"));
                      }
                  }
              });
          });

          jQuery('#example-advanced').treetable('collapseAll');
                
    </script>
        </asp:Panel>
        

        <div class="h_title">
            <h4>具体权限节点设置</h4>
        </div>
        <asp:Panel runat="server" ID="panPermissions">
        <div class="peritem">
            <h1>
                <asp:CheckBox ID="CheckBsdfs67" runat="server" Text="信息" ToolTip="INTERNALLETTER_BASIC" /></h1>
            <dt>
                <h2>
                    <asp:CheckBox ID="Che343ox68" runat="server" Text="站内信" ToolTip="INTERNALLETTER_ZHANNEI" /></h2>
                <ul>
                    <h3>
                        <asp:CheckBox ID="C343ckBox69" runat="server" Text="列表" ToolTip="INTERNALLETTER_ZHANNEI_LIST" /></h3>
                    <li>
                        <asp:CheckBox ID="CdfwkBeox70" runat="server" Text="查看" ToolTip="INTERNALLETTER_ZHANNEI_LIST_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="Check23423xf70" runat="server" Text="增加" ToolTip="INTERNALLETTER_ZHANNEI_LIST_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="Checksdfox70" runat="server" Text="修改" ToolTip="INTERNALLETTER_ZHANNEI_LIST_EDIT" /></li>
                    <li>
                        <asp:CheckBox ID="Checsdfexox70" runat="server" Text="删除" ToolTip="INTERNALLETTER_ZHANNEI_LIST_DEL" /></li>

                </ul>
                
            </dt>
            
            
        </div>
        
        <div class="peritem">
            <h1>
                <asp:CheckBox ID="CheckBox1" runat="server" Text="客户" ToolTip="CLIENT_BASIC"/></h1>
            <dt>
                <h2>
                    <asp:CheckBox ID="CheckBox2" runat="server" Text="客户管理" ToolTip="CLIENT_MANAGE"/></h2>
                <ul>
                    <h3>
                        <asp:CheckBox ID="CheckBox3" runat="server" Text="联系人管理" ToolTip="CLIENTLIAISONS_MANAGE"/></h3>
                    <li>
                        <asp:CheckBox ID="CheckBox4" runat="server" Text="查看" ToolTip="CLIENTLIAISONS_MANAGE_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox5" runat="server" Text="增加" ToolTip="CLIENTLIAISONS_MANAGE_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox7" runat="server" Text="修改"  ToolTip="CLIENTLIAISONS_MANAGE_EDIT"/></li>
                    <li>
                        <asp:CheckBox ID="CheckBox6" runat="server" Text="删除" ToolTip="CLIENTLIAISONS_MANAGE_DEL"/></li>
                </ul>
                <ul>
                    <h3>
                        <asp:CheckBox ID="CheckBox8" runat="server" Text="客户列表" ToolTip="CLIENTINFO_LIST" /></h3>
                    <li>
                        <asp:CheckBox ID="CheckBox9" runat="server" Text="查看" ToolTip="CLIENTINFO_LIST_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox10" runat="server" Text="增加" ToolTip="CLIENTINFO_LIST_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox11" runat="server" Text="修改" ToolTip="CLIENTINFO_LIST_EDIT" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox12" runat="server" Text="删除" ToolTip="CLIENTINFO_LIST_DEL" /></li>
                </ul>
            </dt>
            <dt>
                <h2>
                    <asp:CheckBox ID="CheckBox13" runat="server" 
                        Text="市场营销" ToolTip="MARKETINGPLAN_MANAGE" /></h2>
                <ul>
                    <h3>
                        <asp:CheckBox ID="CheckBox14" runat="server" 
                            Text="营销计划" ToolTip="MARKETINGPLAN_LIST" /></h3>
                    <li>
                        <asp:CheckBox ID="CheckBox15" runat="server" Text="查看" ToolTip="MARKETINGPLAN_LIST_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox16" runat="server" Text="增加" ToolTip="MARKETINGPLAN_LIST_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox17" runat="server" Text="修改" ToolTip="MARKETINGPLAN_LIST_EDIT" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox18" runat="server" Text="删除" ToolTip="MARKETINGPLAN_LIST_DEL" /></li>
                </ul>
                <ul>
                    <h3>
                        <asp:CheckBox ID="CheckBox19" runat="server"
                            Text="商业机会" ToolTip="SALESOPPORTUNITIES_LIST" />
                    </h3>
                    <li>
                        <asp:CheckBox ID="CheckBox20" runat="server" Text="查看" ToolTip="SALESOPPORTUNITIES_LIST_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox21" runat="server" Text="增加" ToolTip="SALESOPPORTUNITIES_LIST_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox22" runat="server" Text="修改" ToolTip="SALESOPPORTUNITIES_LIST_EDIT" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox23" runat="server" Text="删除" ToolTip="SALESOPPORTUNITIES_LIST_DEL" /></li>
                </ul>
                <ul>
                    <h3>
                        <asp:CheckBox ID="CheckBox24" runat="server" 
                            Text="客户跟进" ToolTip="CLIENTTRACKING_LIST" /></h3>
                    <li>
                        <asp:CheckBox ID="CheckBox25" runat="server" Text="查看" ToolTip="CLIENTTRACKING_LIST_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox26" runat="server" Text="增加" ToolTip="CLIENTTRACKING_LIST_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox27" runat="server" Text="修改" ToolTip="CLIENTTRACKING_LIST_EDIT" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox28" runat="server" Text="删除" ToolTip="CLIENTTRACKING_LIST_DEL" /></li>
                </ul>
            </dt>
        </div>
        
        <div class="peritem">
            <h1>
                <asp:CheckBox ID="CheckBox29" runat="server" Text="项目" ToolTip="PROJECTINFO_BASIC"/></h1>
            <dt>
                <h2>
                    <asp:CheckBox ID="CheckBox30" runat="server" Text="项目管理" ToolTip="PROJECTINFO_MANAGE" /></h2>
                <ul>
                    <h3>
                        <asp:CheckBox ID="CheckBox31" runat="server" Text="我的工作" ToolTip="PROJECTINFO_MYLIST" /></h3>
                    <li>
                        <asp:CheckBox ID="CheckBox32" runat="server" Text="查看" ToolTip="PROJECTINFO_MYLIST_VIEW"/></li>
                    <li>
                        <asp:CheckBox ID="CheckBox33" runat="server" Text="增加" ToolTip="PROJECTINFO_MYLIST_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox34" runat="server" Text="修改" ToolTip="PROJECTINFO_MYLIST_EIDT" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox35" runat="server" Text="删除" ToolTip="PROJECTINFO_MYLIST_DEL"/></li>
                    <li>
                        <asp:CheckBox ID="CheckBox42" runat="server" Text="时间查看" ToolTip="PROJECTROLES_LIST_TIMEMANAGE"/></li>
                    <li>
                        <asp:CheckBox ID="CheckBox43" runat="server" Text="资金查看" ToolTip="PROJECTROLES_LIST_ESTMANAGE"/></li>
                </ul>
                <ul>
                    <h3>
                        <asp:CheckBox ID="CheckBox36" runat="server" Text="全部列表" ToolTip="PROJECTINFO_LIST" /></h3>
                    <li>
                        <asp:CheckBox ID="CheckBox37" runat="server" Text="查看" ToolTip="PROJECTINFO_LIST_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox38" runat="server" Text="增加" ToolTip="PROJECTINFO_LIST_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox39" runat="server" Text="修改" ToolTip="PROJECTINFO_LIST_EDIT"/></li>
                    <li>
                        <asp:CheckBox ID="CheckBox40" runat="server" Text="删除" ToolTip="PROJECTINFO_LIST_DEL"/></li>
                    <li>
                        <asp:CheckBox ID="CheckBox44" runat="server" Text="时间查看" ToolTip="PROJECTROLES_LIST_TIMEMANAGE" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox45" runat="server" Text="资金查看" ToolTip="PROJECTROLES_LIST_ESTMANAGE"/></li>
                </ul>
            </dt>
          <dt>
                <h2>
                    <asp:CheckBox ID="CheckBox41" runat="server" Text="议题追踪" ToolTip="ISSUETRACKER_BASIC" /></h2>
                <ul>
                    <h3>
                        <asp:CheckBox ID="CheckBox46" runat="server" Text="我的议题" ToolTip="ISSUETRACKER_MYLIST"/></h3>
                    <li>
                        <asp:CheckBox ID="CheckBox47" runat="server" Text="查看" ToolTip="ISSUETRACKER_MYLIST_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox48" runat="server" Text="增加" ToolTip="ISSUETRACKER_MYLIST_ADD"  /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox49" runat="server" Text="修改" ToolTip="ISSUETRACKER_MYLIST_EDIT"  /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox50" runat="server" Text="删除" ToolTip="ISSUETRACKER_MYLIST_DEL" /></li>
                </ul>
                <ul>
                    <h3>
                        <asp:CheckBox ID="CheckBox51" runat="server" Text="全部追踪" ToolTip="ISSUETRACKER_LIST"  /></h3>
                    <li>
                        <asp:CheckBox ID="CheckBox52" runat="server" Text="查看" ToolTip="ISSUETRACKER_LIST_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox5222" runat="server" Text="增加" ToolTip="ISSUETRACKER_LIST_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox522" runat="server" Text="修改" ToolTip="ISSUETRACKER_LIST_EDIT" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox52a2" runat="server" Text="删除" ToolTip="ISSUETRACKER_LIST_DEL" /></li>
                </ul>
            </dt>
          <dt>
                <h2>
                    <asp:CheckBox ID="CheckBox53" runat="server" Text="讨论区" ToolTip="DISCUSS_BASIC" /></h2>
                <ul>
                    <h3>
                        <asp:CheckBox ID="CheckBox54" runat="server" Text="讨论列表" ToolTip="DISCUSS_LIST"/></h3>
                    <li>
                        <asp:CheckBox ID="CheckBox55" runat="server" Text="查看" ToolTip="DISCUSS_LIST_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox59" runat="server" Text="增加" ToolTip="DISCUSS_LIST_ADD"  /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox60" runat="server" Text="修改" ToolTip="DISCUSS_LIST_EDIT"  /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox61" runat="server" Text="删除" ToolTip="DISCUSS_LIST_DEL" /></li>
                </ul>
                <ul>
                    <h3>
                        <asp:CheckBox ID="CheckBox63" runat="server" Text="其他功能" ToolTip="DISCUSS_TOOL"  /></h3>
                    <li>
                        <asp:CheckBox ID="CheckBox64" runat="server" Text="快捷回复" ToolTip="DISCUSS_TOOL_KJHF" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox65" runat="server" Text="上传附件" ToolTip="DISCUSS_TOOL_UPRES" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox66" runat="server" Text="下载附件" ToolTip="DISCUSS_TOOL_DOWNRES" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox70" runat="server" Text="发送信息" ToolTip="DISCUSS_TOOL_SENDINFO" /></li>
                </ul>
            </dt>
            
            
            
            
            
            <dt>
                <h2>
                    <asp:CheckBox ID="CheckBox56" runat="server" Text="参数设置" ToolTip="PROJECTTYPES_BASIC" /></h2>
                <ul>
                    <h3>
                        <asp:CheckBox ID="CheckBox57" runat="server" Text="项目类型" ToolTip="PROJECTTYPES_LIST" /></h3>
                    <li>
                        <asp:CheckBox ID="CheckBox58" runat="server" Text="查看" ToolTip="PROJECTTYPES_LIST_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox258" runat="server" Text="增加" ToolTip="PROJECTTYPES_LIST_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox558" runat="server" Text="修改" ToolTip="PROJECTTYPES_LIST_EDIT" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBox358" runat="server" Text="删除" ToolTip="PROJECTTYPES_LIST_DEL" /></li>
                </ul>
                <ul>
                    <h3>
                        <asp:CheckBox ID="CheckBox62" runat="server" Text="项目角色" ToolTip="PROJECTROLES_LIST" /></h3>
                    <li>
                        <asp:CheckBox ID="CheckB3ox63" runat="server" Text="查看" ToolTip="PROJECTROLES_LIST_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="CheackBox63" runat="server" Text="增加" ToolTip="PROJECTROLES_LIST_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBfox63" runat="server" Text="修改" ToolTip="PROJECTROLES_LIST_EDIT"  /></li>
                    <li>
                        <asp:CheckBox ID="CheacksBox63" runat="server" Text="删除" ToolTip="PROJECTROLES_LIST_DEL" /></li>
                </ul>
            </dt>
        </div>
        
       <div class="peritem">
            <h1>
                <asp:CheckBox ID="CheckBox2ssf9" runat="server" Text="日程" ToolTip="PROGRAMME_MANAGE"/></h1>
        </div>
        
       <div class="peritem">
            <h1>
                <asp:CheckBox ID="CheckBoxsf67" runat="server" Text="财务" ToolTip="FINANCE_MANAGE" /></h1>
            <dt>
                <h2>
                    <asp:CheckBox ID="ChesdackBox68" runat="server" Text="收款管理" ToolTip="MAKECOLLECTIONS_MANAGE" /></h2>
                <ul>
                    <h3>
                        <asp:CheckBox ID="CheasfsckBox69" runat="server" Text="应收款" ToolTip="MAKECOLLECTIONS_MANAGE_LIST" /></h3>
                    <li>
                        <asp:CheckBox ID="ChesfsckBeox70" runat="server" Text="查看" ToolTip="MAKECOLLECTIONS_MANAGE_LIST_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="ChefefeckBoxf70" runat="server" Text="增加" ToolTip="MAKECOLLECTIONS_MANAGE_LIST_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="Checasf23ksBox70" runat="server" Text="修改" ToolTip="MAKECOLLECTIONS_MANAGE_LIST_EDIT" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBxox70" runat="server" Text="删除" ToolTip="MAKECOLLECTIONS_MANAGE_LIST_DEL" /></li>

                </ul>
                <ul>
                    <h3>
                        <asp:CheckBox ID="CheckBf232ox74" runat="server" Text="已收款" ToolTip="MAKECOLLECTIONS_MANAGE_LIST_SATE1" /></h3>
                    <li>
                        <asp:CheckBox ID="ChecfsakdBox75" runat="server" Text="查看" ToolTip="MAKECOLLECTIONS_MANAGE_LIST_SATE1_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="Checa24kBox75" runat="server" Text="增加" ToolTip="MAKECOLLECTIONS_MANAGE_LIST_SATE1_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="Chefck2342Box75" runat="server" Text="修改" ToolTip="MAKECOLLECTIONS_MANAGE_LIST_SATE1_EDIT" /></li>
                    <li>
                        <asp:CheckBox ID="CheccksfaBox75" runat="server" Text="删除" ToolTip="MAKECOLLECTIONS_MANAGE_LIST_SATE1_DEL" /></li>
                </ul>
            </dt>
            <dt>
                <h2>
                    <asp:CheckBox ID="ChecadfkBox74" runat="server" Text="付款管理" ToolTip="ADVICEPAYMENT_MANAGE" />
                </h2>
                <ul>
                    <h3>
                        <asp:CheckBox ID="Checer3kBox80" runat="server" Text="已付款" ToolTip="ADVICEPAYMENT_MANAGE_LIST_SATE1" /></h3>
                    <li>
                        <asp:CheckBox ID="CheckBoasfas2x81" runat="server" Text="查看" ToolTip="ADVICEPAYMENT_MANAGE_LIST_SATE1_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="CheckB5242o3x81" runat="server" Text="增加" ToolTip="ADVICEPAYMENT_MANAGE_LIST_SATE1_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="CheckB234sdf3ox81" runat="server" Text="修改" ToolTip="ADVICEPAYMENT_MANAGE_LIST_SATE1_EDIT" /></li>
                    <li>
                        <asp:CheckBox ID="CheckB4o234asfx81" runat="server" Text="删除" ToolTip="ADVICEPAYMENT_MANAGE_LIST_SATE1_DEL" /></li>
                </ul>
                
                <ul>
                    <h3>
                        <asp:CheckBox ID="Check234Box80" runat="server" Text="应付款" ToolTip="ADVICEPAYMENT_MANAGE_LIST_SATE0" /></h3>
                    <li>
                        <asp:CheckBox ID="CheckBasfos2x81" runat="server" Text="查看" ToolTip="ADVICEPAYMENT_MANAGE_LIST_SATE0_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="CheckB5afaso3x81" runat="server" Text="增加" ToolTip="ADVICEPAYMENT_MANAGE_LIST_SATE0_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="CheckB3safasfox81" runat="server" Text="修改" ToolTip="ADVICEPAYMENT_MANAGE_LIST_SATE0_EDIT" /></li>
                    <li>
                        <asp:CheckBox ID="CheckB4adffox81" runat="server" Text="删除" ToolTip="ADVICEPAYMENT_MANAGE_LIST_SATE0_DEL" /></li>
                </ul>
                
            </dt>
            <dt>
                <h2>
                    <asp:CheckBox ID="Chec3kBoaaex85" runat="server" Text="工资管理" ToolTip="PAYROLLCONTROL_MANAGE" /></h2>
                <ul>
                    <h3>
                        <asp:CheckBox ID="Check5Bxefox86" runat="server" Text="工资列表" ToolTip="PAYROLLCONTROL_MANAGE_LIST" /></h3>
                    <li>
                        <asp:CheckBox ID="Chec6kBsdfasox87" runat="server" Text="查看" ToolTip="PAYROLLCONTROL_MANAGE_LIST_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="Csfd7Box87" runat="server" Text="增加" ToolTip="PAYROLLCONTROL_MANAGE_LIST_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="CxafckBocxx87" runat="server" Text="修改" ToolTip="PAYROLLCONTROL_MANAGE_LIST_EDIT" /></li>
                    <li>
                        <asp:CheckBox ID="CsdfsBaox87" runat="server" Text="删除" ToolTip="PAYROLLCONTROL_MANAGE_LIST_DEL"  /></li>
                </ul>
            </dt>
            <dt>
                <h2>
                    <asp:CheckBox ID="ChfefBox91" runat="server" Text="银行帐号" ToolTip="BANKACCOUNT_MANAGE" /></h2>
                <ul>
                    <h3>
                        <asp:CheckBox ID="Czdfox92" runat="server" Text="银行帐号" ToolTip="BANKACCOUNT_MANAGE_LIST" /></h3>
                    <li>
                        <asp:CheckBox ID="CfefBocx93" runat="server" Text="查看" ToolTip="BANKACCOUNT_MANAGE_LIST_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="ChzdfBox93" runat="server" Text="增加" ToolTip="BANKACCOUNT_MANAGE_LIST_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="Csdfeox93" runat="server" Text="修改" ToolTip="BANKACCOUNT_MANAGE_LIST_EDIT" /></li>
                    <li>
                        <asp:CheckBox ID="Chsdfx93" runat="server" Text="删除" ToolTip="BANKACCOUNT_MANAGE_LIST_DEL" /></li>
                </ul>
               
            </dt>
            <dt>
                <h2>
                    <asp:CheckBox ID="Chdfe7" runat="server" Text="费用报销" ToolTip="REIMBURSEMENTEXPENSES_MANAGE"/></h2>
                <ul>
                    <h3>
                        <asp:CheckBox ID="Cdfdx108" runat="server" Text="列表" ToolTip="REIMBURSEMENTEXPENSES_MANAGE_LIST" /></h3>
                    <li>
                        <asp:CheckBox ID="Checzdff109" runat="server" Text="查看" ToolTip="REIMBURSEMENTEXPENSES_MANAGE_LIST_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="Cefezxa109" runat="server" Text="增加" ToolTip="REIMBURSEMENTEXPENSES_MANAGE_LIST_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="Chesdfew09" runat="server" Text="修改" ToolTip="REIMBURSEMENTEXPENSES_MANAGE_LIST_EDIT"  /></li>
                    <li>
                        <asp:CheckBox ID="Checefea109" runat="server" Text="删除" ToolTip="REIMBURSEMENTEXPENSES_MANAGE_LIST_DEL" /></li>
                </ul>
            </dt>
            
        </div>
        
        
       <div class="peritem">
            <h1>
                <asp:CheckBox ID="sdfwzdf" runat="server" Text="组织" ToolTip="INSTITUTION_BASIC" /></h1>
            <dt>
                <h2>
                    <asp:CheckBox ID="ffsafa" runat="server" Text="机构管理" ToolTip="INSTITUTION_MANAGE" /></h2>
                <ul>
                    <h3>
                        <asp:CheckBox ID="Ch234asdBox69" runat="server" Text="机构列表" ToolTip="INSTITUTION_MANAGE_LIST" /></h3>
                    <li>
                        <asp:CheckBox ID="Che3saox70" runat="server" Text="查看" ToolTip="INSTITUTION_MANAGE_LIST_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="Chec343" runat="server" Text="增加" ToolTip="INSTITUTION_MANAGE_LIST_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="Checksf70" runat="server" Text="修改" ToolTip="INSTITUTION_MANAGE_LIST_EDIT" /></li>
                    <li>
                        <asp:CheckBox ID="Cheefe70" runat="server" Text="删除" ToolTip="INSTITUTION_MANAGE_LIST_DEL" /></li>

                </ul>
                
            </dt>
            <dt>
                <h2>
                    <asp:CheckBox ID="Checkdfs4" runat="server" Text="部门管理" ToolTip="DEPARTMENT_MANAGE" />
                </h2>
                <ul>
                    <h3>
                        <asp:CheckBox ID="Chsdfx80" runat="server" Text="列表" ToolTip="DEPARTMENT_MANAGE_LIST" /></h3>
                    <li>
                        <asp:CheckBox ID="CsdfkBos2x81" runat="server" Text="查看" ToolTip="DEPARTMENT_MANAGE_LIST_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="Cdfa5o3x81" runat="server" Text="增加" ToolTip="DEPARTMENT_MANAGE_LIST_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="C343ox81" runat="server" Text="修改" ToolTip="DEPARTMENT_MANAGE_LIST_EDIT" /></li>
                    <li>
                        <asp:CheckBox ID="Chezdfx81" runat="server" Text="删除" ToolTip="DEPARTMENT_MANAGE_LIST_DEL" /></li>
                </ul>
                
                
                
            </dt>
            <dt>
                <h2>
                    <asp:CheckBox ID="C34ox85" runat="server" Text="人员管理" ToolTip="OFFICEWORKER_MANAGE" /></h2>
                <ul>
                    <h3>
                        <asp:CheckBox ID="Chesdfa6" runat="server" Text="列表" ToolTip="OFFICEWORKER_MANAGE_LIST" /></h3>
                    <li>
                        <asp:CheckBox ID="Ch33ox87" runat="server" Text="查看" ToolTip="OFFICEWORKER_MANAGE_LIST_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="Csafx87" runat="server" Text="增加" ToolTip="OFFICEWORKER_MANAGE_LIST_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="Chwerocxx87" runat="server" Text="修改" ToolTip="OFFICEWORKER_MANAGE_LIST_EDIT" /></li>
                    <li>
                        <asp:CheckBox ID="Cheasfox87" runat="server" Text="删除" ToolTip="OFFICEWORKER_MANAGE_LIST_DEL"  /></li>
                </ul>
            </dt>
            
            
            
        </div>
        
       <div class="peritem">
            <h1>
                <asp:CheckBox ID="CheckBox67" runat="server" Text="系统" ToolTip="SYSTEM_BASIC" /></h1>
            <dt>
                <h2>
                    <asp:CheckBox ID="CheckBox68" runat="server" Text="帐号" ToolTip="ACCOUNT_MANAGE" /></h2>
                <ul>
                    <h3>
                        <asp:CheckBox ID="ChecksfsBox69" runat="server" Text="帐号列表" ToolTip="ACCOUNT_LIST" /></h3>
                    <li>
                        <asp:CheckBox ID="CheckBezdfox70" runat="server" Text="查看" ToolTip="ACCOUNT_LIST_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBo234xf70" runat="server" Text="增加" ToolTip="ACCOUNT_LIST_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="ChecksBosdfx70" runat="server" Text="修改" ToolTip="ACCOUNT_LIST_EDIT" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBxoxsdf70" runat="server" Text="删除" ToolTip="ACCOUNT_LIST_DEL" /></li>

                    <li>
                        <asp:CheckBox ID="CheckBox71" runat="server" Text="分配权限" ToolTip="ACCOUNT_LIST_ALLOT" /></li>
                </ul>
                <ul>
                    <h3>
                        <asp:CheckBox ID="CheckBfox74" runat="server" Text="系统日志" ToolTip="JOURNAL_MANGAGE" /></h3>
                    <li>
                        <asp:CheckBox ID="CheckdBodfdx75" runat="server" Text="查看" ToolTip="JOURNAL_MANGAGE_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="ChecakBoasdfx75" runat="server" Text="增加" ToolTip="JOURNAL_MANGAGE_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="ChefckBosdfax75" runat="server" Text="修改" ToolTip="JOURNAL_MANGAGE_EDIT" /></li>
                    <li>
                        <asp:CheckBox ID="ChecckBosfx75" runat="server" Text="删除" ToolTip="JOURNAL_MANGAGE_DEL" /></li>
                </ul>
            </dt>
            <dt>
                <h2>
                    <asp:CheckBox ID="CheckBox74" runat="server" Text="全局字典" ToolTip="DICTIONARYTABLE_MANAGE" />
                </h2>
                <ul>
                    <h3>
                        <asp:CheckBox ID="CheckBox80" runat="server" Text="字典列表" ToolTip="DICTIONARYTABLE_LIST" /></h3>
                    <li>
                        <asp:CheckBox ID="CheckBos2x81" runat="server" Text="查看" ToolTip="DICTIONARYTABLE_LIST_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="CheckB5o3x81" runat="server" Text="增加" ToolTip="DICTIONARYTABLE_LIST_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="CheckB3ox81" runat="server" Text="修改" ToolTip="DICTIONARYTABLE_LIST_EDIT" /></li>
                    <li>
                        <asp:CheckBox ID="CheckB4ox81" runat="server" Text="删除" ToolTip="DICTIONARYTABLE_LIST_DEL" /></li>
                </ul>
            </dt>
            <dt>
                <h2>
                    <asp:CheckBox ID="Chec3kBox85" runat="server" Text="搜索设置" ToolTip="SEARCHCONFIG_BASIC" /></h2>
                <ul>
                    <h3>
                        <asp:CheckBox ID="Check5Box86" runat="server" Text="搜索列表" ToolTip="SEARCHCONFIG_LIST" /></h3>
                    <li>
                        <asp:CheckBox ID="Chec6kBox87" runat="server" Text="查看" ToolTip="SEARCHCONFIG_LIST_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="Check7Box87" runat="server" Text="增加" ToolTip="SEARCHCONFIG_LIST_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBocxx87" runat="server" Text="修改" ToolTip="SEARCHCONFIG_LIST_EDIT" /></li>
                    <li>
                        <asp:CheckBox ID="CheackBaox87" runat="server" Text="删除" ToolTip="SEARCHCONFIG_LIST_DEL"  /></li>
                </ul>
            </dt>
            <dt>
                <h2>
                    <asp:CheckBox ID="CheckBox91" runat="server" Text="权限" ToolTip="PERMISSIONS_BASIC" /></h2>
                <ul>
                    <h3>
                        <asp:CheckBox ID="CheckBox92" runat="server" Text="角色" ToolTip="ROLES_LIST" /></h3>
                    <li>
                        <asp:CheckBox ID="CheckBocx93" runat="server" Text="查看" ToolTip="ROLES_LIST_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="ChecfkBox93" runat="server" Text="增加" ToolTip="ROLES_LIST_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="ChecakBcox93" runat="server" Text="修改" ToolTip="ROLES_LIST_EDIT" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBaox93" runat="server" Text="删除" ToolTip="ROLES_LIST_DEL" /></li>
                </ul>
                <ul>
                    <h3>
                        <asp:CheckBox ID="CheckBox97" runat="server" Text="权限节点" ToolTip="PERMISSIONS_LIST"/></h3>
                    <li>
                        <asp:CheckBox ID="CheczkBox98" runat="server" Text="查看" ToolTip="PERMISSIONS_LIST_VIEW"/></li>
                    <li>
                        <asp:CheckBox ID="CheckaeBox98" runat="server" Text="增加" ToolTip="PERMISSIONS_LIST_ADD"/></li>
                    <li>
                        <asp:CheckBox ID="CheckfBox98" runat="server" Text="修改" ToolTip="PERMISSIONS_LIST_EDIT" /></li>
                    <li>
                        <asp:CheckBox ID="CheckfffBox98" runat="server" Text="删除" ToolTip="PERMISSIONS_LIST_DEL"/></li>
                </ul>
                <ul>
                    <h3>
                        <asp:CheckBox ID="CheckBox102" runat="server" Text="权限分类" ToolTip="PERMISSIONCATEGORIES_LIST"/></h3>
                    <li>
                        <asp:CheckBox ID="CheckBozx103" runat="server" Text="查看" ToolTip="PERMISSIONCATEGORIES_LIST_VIEW"/></li>
                    <li>
                        <asp:CheckBox ID="CheckBcsox103" runat="server" Text="增加" ToolTip="PERMISSIONCATEGORIES_LIST_ADD"/></li>
                    <li>
                        <asp:CheckBox ID="CheckBcox103" runat="server" Text="修改" ToolTip="PERMISSIONCATEGORIES_LIST_EDIT"/></li>
                    <li>
                        <asp:CheckBox ID="CheckBfox103" runat="server" Text="删除" ToolTip="PERMISSIONCATEGORIES_LIST_DEL"/></li>
                </ul>
            </dt>
            <dt>
                <h2>
                    <asp:CheckBox ID="CheckBox107" runat="server" Text="菜单" ToolTip="MENUTREE_MANAGE"/></h2>
                <ul>
                    <h3>
                        <asp:CheckBox ID="CheckBox108" runat="server" Text="菜单列表" ToolTip="MENUTREE_LIST" /></h3>
                    <li>
                        <asp:CheckBox ID="CheckBoxf109" runat="server" Text="查看" ToolTip="MENUTREE_LIST_VIEW" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBoxa109" runat="server" Text="增加" ToolTip="MENUTREE_LIST_ADD" /></li>
                    <li>
                        <asp:CheckBox ID="CheckBoxzx109" runat="server" Text="修改" ToolTip="MENUTREE_LIST_EDIT"  /></li>
                    <li>
                        <asp:CheckBox ID="CheckBoxx109" runat="server" Text="删除" ToolTip="MENUTREE_LIST_DEL" /></li>
                </ul>
            </dt>
            <dt>
            <h2><asp:CheckBox ID="CheckBox7sz" runat="server" Text="组件插件" ToolTip="PLUG_BASIC"/></h2>
              <ul><h3><asp:CheckBox ID="CheckBosf72" runat="server" Text="桌面插件" ToolTip="DESKTOPPLUG_BASIC"/></h3>
                   <li>
                        <asp:CheckBox ID="Checkasx73" runat="server" Text="邮件" ToolTip="DESKTOPPLUG_1" /></li>
                  
                     <li>
                        <asp:CheckBox ID="Checfx72" runat="server" Text="我的日程" ToolTip="DESKTOPPLUG_2" />
                    </li>
                    <li>
                        <asp:CheckBox ID="CheckBo73s" runat="server" Text="我的项目" ToolTip="DESKTOPPLUG_3" />
                    </li>
                    <li>
                        <asp:CheckBox ID="Cszf2" runat="server" Text="快捷方式" ToolTip="DESKTOPPLUG_4" />
                    </li>
                    <li>
                        <asp:CheckBox ID="cbffz" runat="server" Text="内部信件" ToolTip="DESKTOPPLUG_5" />
                    </li>
                    <li>
                        <asp:CheckBox ID="safe2" runat="server" Text="近期回访" ToolTip="DESKTOPPLUG_6" />
                    </li>
                     

              </ul>
            </dt>
        </div>


          <div class="peritem">
            <h1>
                <asp:CheckBox ID="Cheds9" runat="server" Text="网盘" ToolTip="SKYDRIVE_BASIC" /></h1>
            <dt>
                <h2>
                    <asp:CheckBox ID="CherkBox72" runat="server" Text="控制权限" ToolTip="SKYCONTROL_AUTHORITY" /></h2>
                <ul>
                    <h3>
                        <asp:CheckBox ID="CwsckBox73" runat="server" Text="新建目录" ToolTip="SKYCREATE_NEW_DIRECTORY" /></h3>
                    <li>
                        <asp:CheckBox ID="rdff" runat="server" Text="新建文本" ToolTip="THE_NEW_TEXT" /></li>
                    <li>
                        <asp:CheckBox ID="Chx7s6" runat="server" Text="删除" ToolTip="SKY_DELETE" /></li>
                    <li>
                        <asp:CheckBox ID="Che77" runat="server" Text="粘贴" ToolTip="SKY_PASTE" /></li>
                    <li>
                        <asp:CheckBox ID="ChxzBox78" runat="server" Text="复制" ToolTip="SKY_REPLICATION" /></li>

                    <li>
                        <asp:CheckBox ID="Czx79" runat="server" Text="剪贴" ToolTip="SKY_CLIP" /></li>
                </ul>
                <ul>
                    <h3>
                        <asp:CheckBox ID="sdfcc" runat="server" Text="压缩" ToolTip="SKY_COMPRESSION" /></h3>
                    <li>
                        <asp:CheckBox ID="cczc82" runat="server" Text="解压" ToolTip="SKY_DECOMPRESSION" /></li>
                    <li>
                        <asp:CheckBox ID="fszc83" runat="server" Text="上传" ToolTip="SKY_UPLOAD" /></li>
                    <li>
                        <asp:CheckBox ID="acsf84" runat="server" Text="下载" ToolTip="SKY_DOWNLOAD" /></li>
                    
                </ul>
            </dt>
           
        </div>

        </asp:Panel>
        
        
        
        
    </div>
    <!--div-->
    <div class="clear"></div>
    <div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" OnClick="LinkButton1_Click">保存</asp:LinkButton>
        <a href="javascript:history.go(-1);" class="cancel">取消</a>
    </div>
    </form>
</asp:Content>
