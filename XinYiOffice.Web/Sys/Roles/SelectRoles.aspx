﻿<%@ Page Language="C#" MasterPageFile="~/MinLayer.Master" AutoEventWireup="true" CodeBehind="SelectRoles.aspx.cs" Inherits="XinYiOffice.Web.Sys.Roles.SelectRoles" %>

<asp:Content ContentPlaceHolderID="head_layer" runat="server">
<script>


    $(function () {

        var ins = $("#ins");
        var rem = $("#rem");

        var all_list = $("#<%=ListBox_Roles.ClientID%>");
        var my_list = $("#<%=ListBox_SelectRoles.ClientID%>");
        var hid_sel = $("#<%=HiddenField_SelectRoles.ClientID%>");


        ins.click(function () {

            move(all_list.attr("id"), my_list.attr("id"));
        });

        rem.click(function () {

            move(my_list.attr("id"), all_list.attr("id"));
        });





        //setname:要移出数据的列表名称 getname:要移入数据的列表名称
        function move(setname, getname) {
            var size = $("#" + setname + " option").size();
            var selsize = $("#" + setname + " option:selected").size();
            if (size > 0 && selsize > 0) {
                $.each($("#" + setname + " option:selected"), function (id, own) {
                    var text = $(own).text();
                    var tag = $(own).attr("value");
                    $("#" + getname).prepend("<option value=\"" + tag + "\">" + text + "</option>");
                    $(own).remove();
                    $("#" + setname + "").children("option:first").attr("selected", true);
                });
            }
            //重新排序
            $.each($("#" + getname + " option"), function (id, own) {
                orderrole(getname);
            });
        }

        //按首字母排序角色列表
        function orderrole(listname) {
            var size = $("#" + listname + " option").size();
            var one = $("#" + listname + " option:first-child");
            if (size > 0) {
                var text = $(one).text();
                var tag = parseInt($(one).attr("value"));
                //循环列表中第一项值下所有元素
                $.each($(one).nextAll(), function (id, own) {
                    var nextag = parseInt($(own).attr("value"));
                    if (tag > nextag) {
                        $(one).remove();
                        $(own).after("<option value=\"" + tag + "\">" + text + "</option>");
                        one = $(own).next();
                    }
                });
            }
        }



        function vf() {
            var _val = '';

            my_list.each(function () {

                $(this).find("option").each(function () {
                    _val += $(this).val() + ',';
                });

            });

            hid_sel.attr("value", _val);

        }

        var frm = $('#<%=f2.ClientID%>');

        frm.submit(function () {
            vf();

            // 提交表单
            frm.ajaxSubmit();
            // 为了防止普通浏览器进行表单提交和产生页面导航（防止页面刷新？）返回false

            window.setTimeout(function () {
                var i = parent.layer.getFrameIndex();
                parent.layer.close(i);
            }, 300);



            return false;
        });


    })





</script>
</asp:Content>

<asp:Content ContentPlaceHolderID="PlaceBody" runat="server">
<form  runat="server" id="f2" action="?action=add">

<table width="600" border="0">
  <tr>
    <td width="138">目前可选角色</td>
    <td rowspan="2" class="style1"><p>
        <input  type="button" value="&gt;" id="ins"/><br/>
        <input type="button" value="&lt;" id="rem"/>
        </p>
      </td>
    <td width="268">已拥有的角色<asp:HiddenField ID="HiddenField_AccountId" runat="server" />
      </td>
  </tr>
  <tr>
    <td>
        <asp:ListBox ID="ListBox_Roles" runat="server" Height="200px" Width="150px">
        </asp:ListBox>
      </td>
    <td>
        <asp:ListBox ID="ListBox_SelectRoles" runat="server" Height="200px" Width="150px"></asp:ListBox>
      </td>
      <asp:HiddenField ID="HiddenField_SelectRoles" runat="server" />
  </tr>
</table>

<input type="submit" value="确定" name="btn1" id="btn1" />
</form>

</asp:Content>
