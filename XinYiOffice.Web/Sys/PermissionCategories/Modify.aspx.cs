﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Sys.PermissionCategories
{
    public partial class Modify : BasicPage
    {       

        		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
				{
					int Id=(Convert.ToInt32(Request.Params["id"]));
					ShowInfo(Id);
				}
			}
		}
			
	private void ShowInfo(int Id)
	{
		XinYiOffice.BLL.PermissionCategories bll=new XinYiOffice.BLL.PermissionCategories();
		XinYiOffice.Model.PermissionCategories model=bll.GetModel(Id);
		this.lblId.Text=model.Id.ToString();
		this.txtName.Text=model.Name;
		this.txtCreateAccountId.Value=model.CreateAccountId.ToString();
		this.txtRefreshAccountId.Value=model.RefreshAccountId.ToString();
		this.txtCreateTime.Value=model.CreateTime.ToString();
		this.txtRefreshTime.Value=model.RefreshTime.ToString();

	}


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = int.Parse(this.lblId.Text);
            string Name = this.txtName.Text;
            int CreateAccountId = SafeConvert.ToInt(txtCreateAccountId.Value,0);
            int RefreshAccountId = SafeConvert.ToInt(txtRefreshAccountId.Value,0);
            DateTime CreateTime = SafeConvert.ToDateTime(txtCreateTime.Value);
            DateTime RefreshTime = SafeConvert.ToDateTime(txtRefreshTime.Value);


            XinYiOffice.Model.PermissionCategories model = new XinYiOffice.Model.PermissionCategories();
            model.Id = Id;
            model.Name = Name;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.PermissionCategories bll = new XinYiOffice.BLL.PermissionCategories();
            bll.Update(model);
            xytools.web_alert("保存成功！", "list.aspx");

        }
    }
}
