﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;
using XinYiOffice.Basic;

namespace XinYiOffice.Web.Sys.MenuTree
{
    public partial class Modify : BasicPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("MENUTREE_LIST_EDIT"))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            AppDataCacheServer.GetMenuTreeList(0, ref DropDownList_MenuTreeId);
            DropDownList_MenuTreeId.Items.Insert(0, new ListItem("无上级菜单", "0"));

            XinYiOffice.BLL.MenuTree bll = new XinYiOffice.BLL.MenuTree();
            XinYiOffice.Model.MenuTree model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.txtGuid.Text = model.Guid;
            this.txtName.Text = model.Name;
            //this.txtMenuTreeId.Text = model.MenuTreeId.ToString();

            DropDownList_MenuTreeId.SelectedValue = SafeConvert.ToString(model.MenuTreeId);

            this.txtChainedAddress.Text = model.ChainedAddress;
            this.txtIsShow.Text = model.IsShow.ToString();
            this.txtSort.Text = model.Sort.ToString();
            this.txtIcon.Text = model.Icon;
            this.txtIconMax.Text = model.IconMax;
            this.txtDescription.Text = model.Description;

        }


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {

            int Id = int.Parse(this.lblId.Text);
            string Guid = this.txtGuid.Text;
            string Name = this.txtName.Text;
            int MenuTreeId = SafeConvert.ToInt(DropDownList_MenuTreeId.SelectedValue);
            string ChainedAddress = this.txtChainedAddress.Text;
            int IsShow = int.Parse(this.txtIsShow.Text);
            int Sort = int.Parse(this.txtSort.Text);
            string Icon = this.txtIcon.Text;
            string IconMax = this.txtIconMax.Text;
            string Description = this.txtDescription.Text;

            int RefreshAccountId = CurrentAccountId;

            DateTime RefreshTime = DateTime.Now;


            XinYiOffice.Model.MenuTree model = new XinYiOffice.Model.MenuTree();
            model.Id = Id;
            model.Guid = Guid;
            model.Name = Name;
            model.MenuTreeId = MenuTreeId;
            model.ChainedAddress = ChainedAddress;
            model.IsShow = IsShow;
            model.Sort = Sort;
            model.Icon = Icon;
            model.IconMax = IconMax;
            model.Description = Description;

            model.RefreshAccountId = RefreshAccountId;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.MenuTree bll = new XinYiOffice.BLL.MenuTree();
            bll.Update(model);

            //CreateWebSiteMap.UpWebSiteMap();


            AppDataCacheServer.ClearDataCacheByName("MenuTreeList");
            xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
