﻿<%@ Page Language="C#"  MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="XinYiOffice.Web.Sys.SearchConfig.Add" Title="增加页" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
<script>
    $(function () {
        var dll_seltablename = $("#<%=DropDownList_TableName.ClientID%>");
        var hid_seltablename = $("#<%=HiddenField_DropDownList_TableName.ClientID%>");
        dll_seltablename.change(function () {
            hid_seltablename.val(dll_seltablename.val());
        });
    })
</script>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">

 <div class="setup_box" style=" margin-bottom:0">
 <div class="h_title"><h4>新建搜索配置</h4></div>
 
<form  id="f2" name="f2" runat="server">
   <table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		唯一序列号 方便程序调用
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtGuid" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		搜索配置名
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		针对的表名或视图名
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_TableName" runat="server">
        </asp:DropDownList>
        <asp:HiddenField ID="HiddenField_DropDownList_TableName" runat="server" />
	</td></tr>
	</table>
    </div>

         <div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" onclick="LinkButton1_Click">保存</asp:LinkButton>
        <a href="javascript:history.go(-1);" class="cancel">取消</a>
        
    </div>

    </form>
</asp:Content>

