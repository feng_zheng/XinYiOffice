﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using XinYiOffice.Common;
using System.Drawing;
using XinYiOffice.Web.UI;
using XinYiOffice.Common.Web;

namespace XinYiOffice.Web.Sys.DictionaryTable
{
    public partial class List : BasicPage
    {
        XinYiOffice.BLL.DictionaryTable bll = new XinYiOffice.BLL.DictionaryTable();

        public int _page = 1;
        public int _pagezs = 0;
        public int _datazs = 0;
        string benye_url;//当前url
        public string pagehtml = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(ValidatePermission("DICTIONARYTABLE_MANAGE") && ValidatePermission("DICTIONARYTABLE_LIST")))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                BindData();
            }            string action = xytools.url_get("action");
            if (!string.IsNullOrEmpty(action))
            {
                switch (action)
                {
                    case "search":
                        SearchApp1.SetSqlWhere();
                        BindData();
                        break;
                }
            }


        }


        #region gridView
        public void BindData()
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select Id,Name,Con,TableName,FieldName,CreateAccountId,RefreshAccountId,CreateTime,RefreshTime,TenantId ");
            strSql.Append(" FROM DictionaryTable ");
            strSql.AppendFormat("where TenantId={0} ", CurrentTenantId);
            if (!string.IsNullOrEmpty(SearchApp1.SqlWhere))
            {
                strSql.Append(SearchApp1.SqlWhere);
            }

            #region 分页
            if (!string.IsNullOrEmpty(xytools.url_get("page")))
            {
                _page = SafeConvert.ToInt(xytools.url_get("page"), 1);
            }
            PageDataSet pd = new PageDataSet();
            DataTable dt = pd.GetListPageBySql(strSql.ToString(), _page, 10, ref _pagezs, ref _datazs);
            PageAttribute pa = new PageAttribute();
            pa.PagCur = _page.ToString();
            pa.PageLinage = "10";
            pa.PageShowFL = "1";
            pa.PageShowGo = "1";

            string urlpas = xyurl.GetQueryRemovePage(Request.QueryString);
            benye_url = "list.aspx?" + urlpas;//获取当前url字符串
            pa.PageUrl = benye_url;
            pagehtml = ParseHTML.PageLabel(pa, _pagezs);
            #endregion

            repDictionaryTable.DataSource = dt;
            repDictionaryTable.DataBind();

        }
        #endregion

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("add.aspx");
        }





    }
}
