﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Sys.DictionaryTable
{
    public partial class Modify : BasicPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ValidatePermission("DICTIONARYTABLE_LIST_EDIT"))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.DictionaryTable bll = new XinYiOffice.BLL.DictionaryTable();
            XinYiOffice.Model.DictionaryTable model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.txtName.Text = model.Name;
            this.txtCon.Text = model.Con;
            this.txtTableName.Text = model.TableName;
            this.txtFieldName.Text = model.FieldName;

            HiddenField_CreateAccountId.Value = model.CreateAccountId.ToString();
            HiddenField_RefreshAccountId.Value = model.RefreshAccountId.ToString();
            HiddenField_CreateTime.Value = model.CreateTime.ToString();
            HiddenField_RefreshTime.Value = model.RefreshTime.ToString();

        }



        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = int.Parse(this.lblId.Text);
            string Name = this.txtName.Text;
            string Con = this.txtCon.Text;
            string TableName = this.txtTableName.Text;
            string FieldName = this.txtFieldName.Text;
            int CreateAccountId = SafeConvert.ToInt(HiddenField_CreateAccountId.Value);
            int RefreshAccountId = SafeConvert.ToInt(HiddenField_RefreshAccountId.Value);
            DateTime CreateTime = SafeConvert.ToDateTime(HiddenField_CreateTime.Value);
            DateTime RefreshTime = SafeConvert.ToDateTime(HiddenField_RefreshTime.Value);


            XinYiOffice.Model.DictionaryTable model = new XinYiOffice.Model.DictionaryTable();
            model.Id = Id;
            model.Name = Name;
            model.Con = Con;
            model.TableName = TableName;
            model.FieldName = FieldName;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.DictionaryTable bll = new XinYiOffice.BLL.DictionaryTable();
            bll.Update(model);
            xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
