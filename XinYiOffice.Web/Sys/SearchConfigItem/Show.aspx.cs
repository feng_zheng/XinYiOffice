﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Sys.SearchConfigItem
{
    public partial class Show : BasicPage
    {
        public string strid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    strid = Request.Params["id"];
                    int Id = (Convert.ToInt32(strid));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.SearchConfigItem bll = new XinYiOffice.BLL.SearchConfigItem();
            XinYiOffice.Model.SearchConfigItem model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.lblDisplayName.Text = model.DisplayName;
            this.lblFieldName.Text = model.FieldName;
            this.lblSearchDataSoureId.Text = model.SearchDataSoureId.ToString();
            this.lblSort.Text = model.Sort.ToString();
            this.lblValueType.Text = model.ValueType.ToString();
            this.lblValueRelChar.Text = model.ValueRelChar;
            this.lblPrefixChar.Text = model.PrefixChar;
            this.lblPrefixRelChar.Text = model.PrefixRelChar;
            this.lblSuffixChar.Text = model.SuffixChar;
            this.lblSuffixRelChar.Text = model.SuffixRelChar;
            this.lblSearchConfigId.Text = model.SearchConfigId.ToString();
            this.lblUseValueType.Text = model.UseValueType.ToString();
            this.lblDefaultValue.Text = model.DefaultValue;
            this.lblCreateAccountId.Text = model.CreateAccountId.ToString();
            this.lblRefreshAccountId.Text = model.RefreshAccountId.ToString();
            this.lblCreateTime.Text = model.CreateTime.ToString();
            this.lblRefreshTime.Text = model.RefreshTime.ToString();

        }


    }
}
