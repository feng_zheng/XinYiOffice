﻿<%@ Page Title="搜索项"  MasterPageFile="~/BasicContent.Master"  Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" EnableViewState="false" Inherits="XinYiOffice.Web.Sys.SearchConfigItem.List" %>
<%@ Register src="../../InitControl/SearchApp.ascx" tagname="SearchApp" tagprefix="uc1" %>

<asp:Content ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form id="f2" name="f2" >
<div class="marketing_box">
<div class="btn_search_box">
        	<div class="clear cc" style=" padding-left:15px;">
            <div class="btn clear"><a href="javascript:;" id="search_a"  class="sj">条件查询</a><a href="add.aspx?SearchConfigId=<%=SearchConfigId%>">添加</a><a href="javascript:;" class="edit" style=" display:;">修改</a><a href="javascript:;" class="tdel">删除</a></div>
                <div class="del clear"></div>
            </div>
          
            <div class="show_search_box" style="display:none;">
        		<div class="tjss clear">


                    <uc1:searchapp ID="SearchApp1" runat="server" TableName="vIssueTracker"/>

                </div>
                <div class="clear btn_box btn_box_p">
                <a href="javascript:;" class="save btn_search">查询</a>
                <a href="javascript:;" class="cancel res_search">取消</a>
                
              
                </div>
        	</div>
    
        </div>
        
        <div class="marketing_table">
        	<table cellpadding="0" cellspacing="0" id="ClientTracking_table">

                <asp:Repeater ID="repSearchConfigItem" runat="server" >
                <HeaderTemplate>
                <tr>
                <th class="center"><input type="checkbox" id="cb_all"></th>
                <th>显示字段名称</th>
                <th>数据库字段名</th>
               <th>表单数据来源</th>
               <th>排序</th>
               <th>值类型</th>
               <th>与值的关系</th>
                </tr>
                </HeaderTemplate>
                
                <ItemTemplate>
                <tr><td class="center"><input type="checkbox" name="chk_list" va="<%#DataBinder.Eval(Container.DataItem, "Id")%>"></td>
                <td><a href="Show.aspx?Id=<%#DataBinder.Eval(Container.DataItem, "Id")%>" ><%#DataBinder.Eval(Container.DataItem, "DisplayName")%></a></td>
 
                <td><%#DataBinder.Eval(Container.DataItem, "FieldName")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "SearchDataSoureId")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "Sort")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "ValueType")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "ValueRelChar")%></td>

                </tr>    
                </ItemTemplate>
                </asp:Repeater>


            </table>
        </div>
        
</div>


</form>

<script>
    $('#ClientTracking_table td').hover(function () {
        $(this).parent().find('td').css('background', '#f0f8fc')
    }, function () {
        $(this).parent().find('td').css('background', 'none')
    });
</script>

</asp:Content>


