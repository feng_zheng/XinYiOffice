﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="XinYiOffice.Web.ChangePassword" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form id="f2" name="f2" runat="server">
    <div class="setup_box">
    	<div class="h_title"><h4>修改密码</h4></div>
        
        <table cellpadding="0" cellspacing="0" id="change_password">
        	<tr><td class="table_left">密码：</td><td>
                <asp:TextBox ID="TextBox_oldpas" runat="server" 
    TextMode="Password" MaxLength="15"></asp:TextBox></td></tr>
            <tr><td class="table_left">新密码：</td><td><asp:TextBox ID="TextBox_newpassword" 
                    runat="server" TextMode="Password" MaxLength="15"></asp:TextBox><span>（至少6个字符）</span></td></tr>
            <tr><td class="table_left">再次输入新密码：</td><td>
                <asp:TextBox ID="TextBox_newpassword2" runat="server" 
    TextMode="Password" MaxLength="15"></asp:TextBox></td></tr> 
        </table>
        

    </div>
    <div class="clear btn_box">
    <asp:LinkButton ID="alinkSave" CssClass="save" runat="server" onclick="alinkSave_Click">确认更改</asp:LinkButton>
    	
        <asp:LinkButton ID="alinkCancel" runat="server" CssClass="cancel" Type="reset">重置</asp:LinkButton>
        

    </div>

</form>
</asp:Content>
