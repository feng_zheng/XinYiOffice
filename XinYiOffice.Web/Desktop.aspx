﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Desktop.aspx.cs" Inherits="XinYiOffice.Web.Desktop" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>桌面</title>
<link href="/css/init.css" rel="stylesheet" type="text/css" />
<link href="/css/index_content.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/jquery-1.8.2.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script>

<script type="text/javascript">
    $(function () {
        $('.hover_tr').each(function () {
            $(this).find('tr:last').find('td').css('border', 'none')
        })
        $('#richeng table tr:last').find('td:last').css('border', 'none')
        $('.hover_tr td').hover(function () {
            $(this).parent().find('td').css('background', '#f0f8fc')
        }, function () {
            $(this).parent().find('td').css('background', 'none')
        });
        var oDate = new Date();
        var oW = ['日', '一', '二', '三', '四', '五', '六']
        strTime = '今天是：' + oDate.getFullYear() + '年' + (oDate.getMonth() + 1) + '月' + oDate.getDate() + '日　星期' + oW[oDate.getDay()]
        $('#time').html(strTime);
        var wk = oDate.getDay() == 0 ? 7 : oDate.getDay();
        var InitTd = $('#richeng table tr').eq(wk - 1).find('td');
        InitTd.eq(0).addClass('actweek');
        InitTd.eq(1).addClass('actcontent');

    })
</script>
</head>

<body>
<div class="min clear">

<asp:Panel ID="panDesktop" runat="server"></asp:Panel>


	<%--
    
    <div id="email" class="list_box">
    	<div class="clear box_title">
        	<p class="left_t">邮件<span class="d">未读<font color="993333">56</font>封</span></p>
        </div>
        <div class="box_min">
        	<table cellpadding="0" cellspacing="0" class="hover_tr">
            	<tr><th class="left">标题</th><th class="right">时间</th></tr>
                <tr><td class="list"><a href="javascaript:;"><strong>李明博- </strong><span class="color_blue">回复：关于消费信息应该匹配内部内形象</span></a></td><td><span class="color_blue">1小时前</span></td></tr>
                <tr><td class="list"><a href="javascaript:;"><strong>SKPE  - </strong><span class="color_blue">创富志之破局之道19：日化行业的本质（4）</span></a></td><td>03-19</td></tr>
                <tr><td class="list"><a href="javascaript:;"><strong>SKPE  - </strong><span class="color_blue">创富志之破局之道19：日化行业的本质（4）</span></a></td><td>03-19</td></tr>
                <tr><td class="list"><a href="javascaript:;"><strong>SKPE  - </strong><span class="color_blue">创富志之破局之道19：日化行业的本质（4）</span></a></td><td>03-19</td></tr>
                <tr><td class="list"><a href="javascaript:;"><strong>SKPE  - </strong><span class="color_blue">创富志之破局之道19：日化行业的本质（4）</span></a></td><td>03-19</td></tr>
                <tr><td class="list"><a href="javascaript:;"><strong>SKPE  - </strong><span class="color_blue">创富志之破局之道19：日化行业的本质（4）</span></a></td><td>03-19</td></tr>
            </table>
        </div>
    </div>
    <div id="richeng" class="list_box">
    	<div class="clear box_title">
        	<p class="left_t">我的日程</p>
            <p class="right_t" id="time"></p>
        </div>
        <div class="box_min">
        	<table cellpadding="0" cellspacing="0">
            	<tr><td class="week">星期一</td><td><a href="javascript:;">逃过一劫，如果你喜欢喝酒，热闹，你会喜欢上我现在的节奏的。</a></td></tr>
            	<tr><td class="week">星期二</td><td><a href="javascript:;">逃过一劫，如果你喜欢喝酒，热闹，你会喜欢上我现在的节奏的。</a></td></tr>
            	<tr><td class="week">星期三</td><td><a href="javascript:;">逃过一劫，如果你喜欢喝酒，热闹，你会喜欢上我现在的节奏的。</a></td></tr>
            	<tr><td class="week">星期四</td><td><a href="javascript:;">逃过一劫，如果你喜欢喝酒，热闹，你会喜欢上我现在的节奏的。</a></td></tr>
            	<tr><td class="week">星期五</td><td><a href="javascript:;">逃过一劫，如果你喜欢喝酒，热闹，你会喜欢上我现在的节奏的。</a></td></tr>
            	<tr><td class="week">星期六</td><td><a href="javascript:;">逃过一劫，如果你喜欢喝酒，热闹，你会喜欢上我现在的节奏的。</a></td></tr>
            	<tr><td class="week">星期日</td><td><a href="javascript:;">逃过一劫，如果你喜欢喝酒，热闹，你会喜欢上我现在的节奏的。</a></td></tr>
            </table>
        </div>
    </div>
    <div id="email" class="list_box">
    	<div class="clear box_title">
        	<p class="left_t">邮件<span class="d">未读<font color="993333">56</font>封</span></p>
        </div>
        <div class="box_min">
        	<table cellpadding="0" cellspacing="0" class="hover_tr">
            	<tr><th class="left">标题</th><th class="center">进度</th><th class="right">时间</th></tr>
                <tr><td class="list"><a href="javascaript:;"><strong>李明博- </strong>回复：关于消费信息应该匹配内部内形象</a></td><td class="center"><p><span style=" width:50%;"></span></p></td><td><span class="color_blue">1小时前</span></td></tr>
                <tr><td class="list"><a href="javascaript:;"><strong>SKPE  - </strong>创富志之破局之道19：日化行业的本质（4）</a></td><td class="center"><p><span style=" width:20%;"></span></p></td><td>03-19</td></tr>
                <tr><td class="list"><a href="javascaript:;"><strong>SKPE  - </strong>创富志之破局之道19：日化行业的本质（4）</a></td><td class="center"><p><span style=" width:80%;"></span></p></td><td>03-19</td></tr>
                <tr><td class="list"><a href="javascaript:;"><strong>SKPE  - </strong>创富志之破局之道19：日化行业的本质（4）</a></td><td class="center"><p><span style=" width:60%;"></span></p></td><td>03-19</td></tr>
                <tr><td class="list"><a href="javascaript:;"><strong>SKPE  - </strong>创富志之破局之道19：日化行业的本质（4）</a></td><td class="center"><p><span style=" width:45%;"></span></p></td><td>03-19</td></tr>
                <tr><td class="list"><a href="javascaript:;"><strong>SKPE  - </strong>创富志之破局之道19：日化行业的本质（4）</a></td><td class="center"><p><span style=" width:56%;"></span></p></td><td>03-19</td></tr>
            </table>
        </div>
    </div>
    <div id="kjfs" class="list_box">
    	<div class="clear box_title">
        	<p class="left_t">快捷方式</p>
        </div>
        <div class="box_min">
        	<ul class="clear">
            	<li>
                	<div><a href="javascript:;"><img src="img/ico1.jpg" alt="图标"></a></div>
                    <p><a href="javascript:;">登陆日志</a></p>
                </li>
                <li>
                	<div><a href="javascript:;"><img src="img/ico2.jpg" alt="图标"></a></div>
                    <p><a href="javascript:;">我的日程</a></p>
                </li>
                <li>
                	<div><a href="javascript:;"><img src="img/ico3.jpg" alt="图标"></a></div>
                    <p><a href="javascript:;">登陆日志</a></p>
                </li>
                <li>
                	<div><a href="javascript:;"><img src="img/ico4.jpg" alt="图标"></a></div>
                    <p><a href="javascript:;">登陆日志</a></p>
                </li>
                <li>
                	<div><a href="javascript:;"><img src="img/ico5.jpg" alt="图标"></a></div>
                    <p><a href="javascript:;">我的日程</a></p>
                </li>
                <li>
                	<div><a href="javascript:;"><img src="img/ico6.jpg" alt="图标"></a></div>
                    <p><a href="javascript:;">联系人</a></p>
                </li>
            </ul>
         </div>
    </div>
    <div id="" class="list_box">
    	<div class="clear box_title">
        	<p class="left_t">内部信函<span class="d">未读<font color="993333">56</font>封</span></p>
        </div>
        <div class="box_min">
        	<table cellpadding="0" cellspacing="0" class="hover_tr">
            	<tr><th class="left">标题</th><th class="right">时间</th></tr>
                <tr><td class="list"><a href="javascaript:;"><strong>李明博- </strong><span class="color_blue">回复：关于消费信息应该匹配内部内形象</span></a></td><td><span class="color_blue">1小时前</span></td></tr>
                <tr><td class="list"><a href="javascaript:;"><strong>SKPE  - </strong><span class="color_blue">创富志之破局之道19：日化行业的本质（4）</span></a></td><td>03-19</td></tr>
                <tr><td class="list"><a href="javascaript:;"><strong>SKPE  - </strong><span class="color_blue">创富志之破局之道19：日化行业的本质（4）</span></a></td><td>03-19</td></tr>
                <tr><td class="list"><a href="javascaript:;"><strong>SKPE  - </strong><span class="color_blue">创富志之破局之道19：日化行业的本质（4）</span></a></td><td>03-19</td></tr>
                <tr><td class="list"><a href="javascaript:;"><strong>SKPE  - </strong><span class="color_blue">创富志之破局之道19：日化行业的本质（4）</span></a></td><td>03-19</td></tr>
                <tr><td class="list"><a href="javascaript:;"><strong>SKPE  - </strong><span class="color_blue">创富志之破局之道19：日化行业的本质（4）</span></a></td><td>03-19</td></tr>
            </table>
        </div>
    </div>--%>
</div>
<p id="min_foot">
	授权给:<%=CurrentTenantNameCn%> &nbsp;&nbsp;技术支持：新亿乐天
    <span style=" display:none;"><script language="javascript" type="text/javascript" src="http://js.users.51.la/16839766.js"></script>
<noscript><a href="http://www.51.la/?16839766" target="_blank"><img alt="&#x6211;&#x8981;&#x5566;&#x514D;&#x8D39;&#x7EDF;&#x8BA1;" src="http://img.users.51.la/16839766.asp" style="border:none" /></a></noscript></span> 

</p>
</body>
</html>
