﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Internal.InternalReceiver
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(ValidatePermission("INTERNALLETTER_ZHANNEI") && ValidatePermission("INTERNALLETTER_ZHANNEI_LIST_ADD")))
            {
                base.NoPermissionPage();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int InternalLetterId = int.Parse(this.txtInternalLetterId.Text);
            int RecipientAccountId = int.Parse(this.txtRecipientAccountId.Text);
            int RecipientType = int.Parse(this.txtRecipientType.Text);
            int IsView = int.Parse(this.txtIsView.Text);
            int IsDelete = int.Parse(this.txtIsDelete.Text);
            int IsReply = int.Parse(this.txtIsReply.Text);
            int LetterPosition = int.Parse(this.txtLetterPosition.Text);
            string PositionFoler = this.txtPositionFoler.Text;
            int CreateAccountId = int.Parse(this.txtCreateAccountId.Text);
            int RefreshAccountId = int.Parse(this.txtRefreshAccountId.Text);
            DateTime CheckDate = DateTime.Parse(this.txtCheckDate.Text);
            DateTime CreateTime = DateTime.Parse(this.txtCreateTime.Text);
            DateTime RefreshTime = DateTime.Parse(this.txtRefreshTime.Text);

            XinYiOffice.Model.InternalReceiver model = new XinYiOffice.Model.InternalReceiver();
            model.InternalLetterId = InternalLetterId;
            model.RecipientAccountId = RecipientAccountId;
            model.RecipientType = RecipientType;
            model.IsView = IsView;
            model.IsDelete = IsDelete;
            model.IsReply = IsReply;
            model.LetterPosition = LetterPosition;
            model.PositionFoler = PositionFoler;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CheckDate = CheckDate;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.InternalReceiver bll = new XinYiOffice.BLL.InternalReceiver();
            bll.Add(model);
            xytools.web_alert("保存成功！", "add.aspx");

        }


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }
    }
}
