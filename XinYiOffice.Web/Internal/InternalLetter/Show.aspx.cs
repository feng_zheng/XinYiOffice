﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Basic;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Internal.InternalLetter
{
    public partial class Show : BasicPage
    {
        public string strid = "";
        public string strImpDegree = string.Empty;
        public string strImpDegreeTitle = string.Empty;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    strid = Request.Params["id"];
                    int Id = (Convert.ToInt32(strid));
                    HiddenField_InternalReceiverId.Value = Id.ToString();

                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.InternalLetter bll = new XinYiOffice.BLL.InternalLetter();
            //XinYiOffice.Model.InternalLetter model = bll.GetModel(Id);

            DataTable _dt = InternalLetterServer.GetvInternalReceiver(Id);
            if (_dt == null || _dt.Rows.Count <= 0)
            {
                return;
                Page.Visible = false;
            }
            else
            {
                DataRow dr = _dt.Rows[0];

                this.lblTitle.Text = SafeConvert.ToString(dr["Title"]);
                this.lblCon.Text = SafeConvert.ToString(dr["Con"]);
                this.HyperLink_AuthorAccountId.Text = string.Format("{0}({1})", SafeConvert.ToString(dr["FullName"]), SafeConvert.ToString(dr["AccountName"]));
                HyperLink_RecipientAccount.Text = string.Format("{0}({1})", SafeConvert.ToString(dr["RecipientAccountFullName"]), SafeConvert.ToString(dr["RecipientAccountName"]));

                strImpDegree = SafeConvert.ToString(dr["ImpDegree"]);

                switch (strImpDegree)
                {
                    case "1":
                        strImpDegreeTitle = "普通";
                        break;

                    case "2":
                        strImpDegreeTitle = "高";
                        break;

                    case "3":
                        strImpDegreeTitle = "重要";
                        break;

                    case "4":
                        strImpDegreeTitle = "非常重要";
                        break;
                }

                #region //更新已读字段
                if (SafeConvert.ToInt(dr["IsView"],0)==0)
                {
                    Model.InternalReceiver ir = new Model.InternalReceiver();
                    ir = new BLL.InternalReceiver().GetModel(Id);
                    ir.IsView = 1;
                    ir.CheckDate = DateTime.Now;
                    new BLL.InternalReceiver().Update(ir);
                }
                #endregion
            }



        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 回复信件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LinkButton_rep_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("RepInternalReceiver.aspx?intrid={0}", HiddenField_InternalReceiverId.Value));
        }

        /// <summary>
        /// 删除信件关联数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LinkButton_del_Click(object sender, EventArgs e)
        {

        }


    }
}
