﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Common;
using System.Collections;
using XinYiOffice.Basic;

namespace XinYiOffice.Web
{
    public partial class OutLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            OutLoginAction.OutAndClearAll();

            if (Config.StaticSettings.TenantModel == "all")
            {
                xytools.href_url("http://www.xinyioffice.com/Cloud/Login.aspx");
            }
            else
            {
                string tid = AppDataCacheServer.CurrentTenantId().ToString();
                xytools.web_alert_new_url("您已退出系统,期待您下次使用!", "login.aspx?tid=" + Common.DESEncrypt.Encrypt(tid));
            }

           
        }
    }
}