﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="_ClientTracking.ascx.cs" Inherits="XinYiOffice.Web.DesktopPlug._ClientTracking" %>
<div id="email222" class="list_box">
    	<div class="clear box_title">
        	<p class="left_t">近期回访客户<span class="d">还有<font style=" color:#C00; font-weight:bold;"><%=ClientTrackingCount%></font>个未回访</span></p>
        </div>
        <div class="box_min">
        	<table cellpadding="0" cellspacing="0" class="hover_tr">
            	<tr><th class="left">客户名称</th><th class="right">需回访时间</th><th class="right">操作</th></tr>

                <asp:Repeater ID="repClientTracking" runat="server">
                <ItemTemplate>
                <tr><td class="list"><a href="/Client/ClientTracking/Show.aspx?Id=<%#DataBinder.Eval(Container.DataItem, "Id")%>"><span class="color_blue"><%#DataBinder.Eval(Container.DataItem, "ClientInfoName")%></span></a></td><td><%#DataBinder.Eval(Container.DataItem, "PlanTime","{0:yyyy-MM-dd}")%></td><td><a href="/Client/ClientTracking/Show.aspx?Id=<%#DataBinder.Eval(Container.DataItem, "Id")%>">开始回访</a></td></tr>
                </ItemTemplate>
                </asp:Repeater>

            </table>
        </div>
    </div>
