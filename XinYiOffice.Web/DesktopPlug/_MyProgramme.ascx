﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="_MyProgramme.ascx.cs" Inherits="XinYiOffice.Web.DesktopPlug._MyProgramme" %>
<script>
    $(function () {
        var qiandao = $(".qiandao");
        var qian_span = $(".qian_span");
        qiandao.click(function () {

            var i_qiandao = $.layer({
                shade: [0.5, '#000', true],
                area: ['300px', 'auto'],
                border: [5, 0.3, '#000', true],
                offset: ['200px', '50%'],
                title: '开始签到',
                closeBtn: [0, true],
                dialog: {
                    btns: 2,
                    btn: ['确定', '取消'],
                    type: 9,
                    msg: '新的一天，新的开始!点击确定签到',
                    yes: function (index) {

                        $.get("/Call/Ajax.aspx?action=setqiandao&sj="+Math.random(), function (dat) {
                            if (dat != '') {
                                if (dat == '1') {
                                    $.layer({ area: ['auto', 'auto'], border: [5, 0.3, '#000', true], title: false, dialog: { msg: '签到成功!', type: 1} });
                                    qiandao.hide();
                                    qian_span.show();
                                }
                                else if (dat == '-99') {
                                    $.layer({ area: ['auto', 'auto'], border: [5, 0.3, '#000', true], title: false, dialog: { msg: '服务器忙,请稍候再试!', type: 8} });
                                }
                            }
                        });
                        layer.close(i_qiandao);
                    },
                    no: function (index) { layer.close(i_qiandao); }
                }

            });

        }); //qiandao


        var qiantui = $(".qiantui");
        qiantui.click(function () {

            var i_qiantui = $.layer({
                shade: [0.5, '#000', true],
                area: ['300px', 'auto'],
                border: [5, 0.3, '#000', true],
                offset: ['200px', '50%'],
                title: '签退',
                closeBtn: [0, true],
                dialog: {
                    btns: 2,
                    btn: ['确定', '取消'],
                    type: 9,
                    msg: '感谢一天的辛苦!点击确定签退',
                    yes: function (index) {

                        $.get("/Call/Ajax.aspx?action=setqiantui", function (dat) {
                            if (dat != '') {
                                if (dat == '1') {
                                    $.layer({ area: ['auto', 'auto'], border: [5, 0.3, '#000', true], title: false, dialog: { msg: '签退成功!', type: 1} });
                                    qiantui.hide();
                                    qian_span.text('已签退');
                                    qian_span.show();
                                }
                                else if (dat == '-99') {
                                    $.layer({ area: ['auto', 'auto'], border: [5, 0.3, '#000', true], title: false, dialog: { msg: '服务器忙,请稍候再试!', type: 8} });

                                }
                            }
                        });
                        layer.close(i_qiantui);
                    },
                    no: function (index) { layer.close(i_qiantui); }
                }
            });

        }); //qiantui

    })
</script>
<div id="richeng" class="list_box">
    	<div class="clear box_title">
        	<p class="left_t">我的日程</p>
            <p class="right_t" id="time"></p>
            <p>[<a class="qiandao" id="qiandao" runat="server" href="javascript:;">签到</a><span class="qian_span" id="qian_span" runat="server" >已签到</span><a class="qiantui" id="qiantui"  runat="server" href="javascript:;">签退</a>]</p>
        </div>
        <div class="box_min">
        	<table cellpadding="0" cellspacing="0">
            	<tr><td class="week" id="dtWeek1" runat="server">星期一</td><td id="dtWeek1Con" runat="server" style=" text-align:left;"></td></tr>
            	<tr><td class="week" id="dtWeek2" runat="server">星期二</td><td id="dtWeek2Con" runat="server" style=" text-align:left;"></td></tr>
            	<tr><td class="week" id="dtWeek3" runat="server">星期三</td><td id="dtWeek3Con" runat="server" style=" text-align:left;"></td></tr>
            	<tr><td class="week" id="dtWeek4" runat="server">星期四</td><td id="dtWeek4Con" runat="server" style=" text-align:left;"></td></tr>
            	<tr><td class="week" id="dtWeek5" runat="server">星期五</td><td id="dtWeek5Con" runat="server" style=" text-align:left;"></td></tr>
            	<tr><td class="week" id="dtWeek6" runat="server">星期六</td><td id="dtWeek6Con" runat="server" style=" text-align:left;"></td></tr>
            	<tr><td class="week" id="dtWeek7" runat="server">星期日</td><td id="dtWeek7Con" runat="server" style=" text-align:left;"></td></tr>
            </table>
        </div>
    </div>