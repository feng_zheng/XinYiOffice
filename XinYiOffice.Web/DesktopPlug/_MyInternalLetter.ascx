﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="_MyInternalLetter.ascx.cs" Inherits="XinYiOffice.Web.DesktopPlug._MyInternalLetter" %>
<div id="email" class="list_box">
    	<div class="clear box_title">
        	<p class="left_t">站内信<span class="d">未读<font color="993333"><%=InternalLetterCount%></font>封</span></p>
        </div>
        <div class="box_min">
        	<table cellpadding="0" cellspacing="0" class="hover_tr">
            	<tr><th class="left">标题</th><th width="140">时间</th></tr>

                <asp:Repeater ID="repInternalLetter" runat="server">
                <ItemTemplate>
                <tr><td class="list"><a href="/Internal/InternalLetter/show.aspx?Id=<%#DataBinder.Eval(Container.DataItem, "Id")%>"><span class="color_blue"><%#DataBinder.Eval(Container.DataItem, "Title")%></span>-<strong><%#DataBinder.Eval(Container.DataItem, "FullName")%></strong></a></td><td> <%#DataBinder.Eval(Container.DataItem, "CreateTime")%></td></tr>
                </ItemTemplate>
                </asp:Repeater>

            </table>
        </div>
    </div>
