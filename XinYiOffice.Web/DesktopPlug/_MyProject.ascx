﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="_MyProject.ascx.cs" Inherits="XinYiOffice.Web.DesktopPlug._MyProject" %>

<div id="email" class="list_box">
    	<div class="clear box_title">
        	<p class="left_t">项目<span class="d">待完成<font color="993333"><%=MyProjectListCount%></font></span></p>
        </div>
        <div class="box_min">
        	<table cellpadding="0" cellspacing="0" class="hover_tr">
            	<tr><th class="left">标题</th><th class="center">进度</th><th class="right">时间</th></tr>

                <asp:Repeater ID="repProject" runat="server">

<ItemTemplate>
                <tr><td class="list"><a href="/Project/ProjectInfo/Show.aspx?Id=<%#DataBinder.Eval(Container.DataItem, "Id")%>"><strong><%#DataBinder.Eval(Container.DataItem, "Title")%></strong></a></td><td class="center"><p><span style=" width:50%;"></span></p></td><td><%#DataBinder.Eval(Container.DataItem, "ActualStartTime", "{0:yyyy-MM-dd}")%></td></tr>
                
                </ItemTemplate>
</asp:Repeater>

            </table>
        </div>
    </div>