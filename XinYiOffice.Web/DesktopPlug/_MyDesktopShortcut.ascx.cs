﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Basic;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.DesktopPlug
{
    public partial class _MyDesktopShortcut : BasicUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            InitData();
        }

        protected void InitData()
        {
            repDesktopShortcut.DataSource = DesktopShortcutServer.GetMyDesktopShortcut(CurrentAccountId,8,CurrentTenantId);
            repDesktopShortcut.DataBind();
        }
    }
}