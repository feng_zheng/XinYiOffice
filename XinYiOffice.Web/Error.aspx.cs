﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace XinYiOffice.Web
{
    public partial class Error : System.Web.UI.Page
    {
        public string sErrorUrl = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            
            sErrorUrl=Request.RawUrl.ToString();
        }
    }
}