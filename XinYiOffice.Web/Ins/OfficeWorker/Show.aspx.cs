﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Basic;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Ins.OfficeWorker
{
    public partial class Show : BasicPage
    {
        public string strid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(ValidatePermission("OFFICEWORKER_MANAGE") && ValidatePermission("OFFICEWORKER_MANAGE_LIST_VIEW")))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    strid = Request.Params["id"];
                    int Id = (Convert.ToInt32(strid));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.OfficeWorker bll = new XinYiOffice.BLL.OfficeWorker();
            //XinYiOffice.Model.OfficeWorker model = bll.GetModel(Id);

            DataTable dt = OfficeWorkerServer.GetOfficeWorkerList(string.Format("Id={0}",Id),CurrentTenantId);
            if(dt!=null&&dt.Rows.Count>0)
            {
                DataRow dr = dt.Rows[0];
                this.lblId.Text = SafeConvert.ToString(dr["Id"]);


                this.lblFullName.Text = SafeConvert.ToString(dr["FullName"]);
                this.lblUsedName.Text = SafeConvert.ToString(dr["UsedName"]);
                this.lblSex.Text =  SafeConvert.ToString(dr["Sex"]);
                this.lblEmail.Text =  SafeConvert.ToString(dr["Email"]);
                this.lblTel.Text =  SafeConvert.ToString(dr["Tel"]);
                this.lblPhone.Text =  SafeConvert.ToString(dr["Phone"]);
                this.lblIntNumber.Text = SafeConvert.ToString(dr["IntNumber"]);
                this.lblInstitutionId.Text = SafeConvert.ToString(dr["InstitutionName"]);
                this.lblDepartmentId.Text = SafeConvert.ToString(dr["DepartmentName"]);
                this.lblPosition.Text = SafeConvert.ToString(dr["PositionName"]);
                this.lblProvince.Text = SafeConvert.ToString(dr["Province"]);
                this.lblCity.Text = SafeConvert.ToString(dr["City"]);
                this.lblCounty.Text = SafeConvert.ToString(dr["County"]);
                this.lblStreet.Text = SafeConvert.ToString(dr["Street"]);
                this.lblZipCode.Text = SafeConvert.ToString(dr["ZipCode"]);
                this.lblBirthDate.Text = SafeConvert.ToString(dr["BirthDate"]);
                this.lblChinaID.Text = SafeConvert.ToString(dr["ChinaID"]);
                this.lblNationality.Text = SafeConvert.ToString(dr["Nationality"]);
                this.lblNativePlace.Text = SafeConvert.ToString(dr["NativePlace"]);
                this.lblPhone1.Text = SafeConvert.ToString(dr["Phone1"]);
                this.lblPhone2.Text = SafeConvert.ToString(dr["Phone2"]);
                this.lblPoliticalStatus.Text =SafeConvert.ToString(dr["PoliticalStatus"]);
                this.lblEntryTime.Text = SafeConvert.ToString(dr["EntryTime"]);
                this.lblEntranceMode.Text = SafeConvert.ToString(dr["EntranceMode"]);
                this.lblPostGrades.Text = SafeConvert.ToString(dr["PostGradesName"]);
                this.lblWageLevel.Text = SafeConvert.ToString(dr["WageLevelName"]);
                this.lblInsuranceWelfare.Text = SafeConvert.ToString(dr["InsuranceWelfare"]);
                this.lblGraduateSchool.Text = SafeConvert.ToString(dr["GraduateSchool"]);
                this.lblFormalSchooling.Text = SafeConvert.ToString(dr["FormalSchooling"]);
                this.lblMajor.Text = SafeConvert.ToString(dr["Major"]);
                this.lblEnglishLevel.Text = SafeConvert.ToString(dr["EnglishLevel"]);
                this.lblPreWork.Text = SafeConvert.ToString(dr["PreWork"]);
                this.lblPrePosition.Text = SafeConvert.ToString(dr["PrePosition"]);
                this.lblPreStartTime.Text = SafeConvert.ToString(dr["PreStartTime"]);
                this.lblPreEndTime.Text =SafeConvert.ToString(dr["PreEndTime"]);
                this.lblPreEpartment.Text = SafeConvert.ToString(dr["PreEpartment"]);
                this.lblTurnoverTime.Text =SafeConvert.ToString(dr["TurnoverTime"]);
                this.lblSate.Text = SafeConvert.ToString(dr["SateName"]);
                this.lblRemarks.Text = SafeConvert.ToString(dr["Remarks"]);
                this.lblCreateAccountId.Text = SafeConvert.ToString(dr["CreateAccountId"]);
                this.lblRefreshAccountId.Text = SafeConvert.ToString(dr["RefreshAccountId"]);
                this.lblCreateTime.Text = SafeConvert.ToString(dr["CreateTime"]);
                this.lblRefreshTime.Text = SafeConvert.ToString(dr["RefreshTime"]);
            }

            

        }


    }
}
