﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Web.UI;
using XinYiOffice.Common;
using System.Data;

namespace XinYiOffice.Web.Ins.OfficeWorker
{
    public partial class QianDaoList :BasicPage
    {
        public int _page = 1;
        public int _pagezs = 0;
        public int _datazs = 0;
        string benye_url;//当前url
        public string pagehtml = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (base.ValidatePermission("CLIENT_BASIC") && base.ValidatePermission("CLIENT_MANAGE") && base.ValidatePermission("CLIENTINFO_LIST"))
            {

            }
            else
            {
                base.NoPermissionPage();
            }

            string action = xytools.url_get("action");
            if (!string.IsNullOrEmpty(action))
            {
                switch (action)
                {
                    case "search":
                        SearchApp1.SetSqlWhere();
                        BindData();
                        break;
                }
            }
            else if (!Page.IsPostBack)
            {
                BindData();
            }
        }

        public void BindData()
        {
            DataTable dt = DbHelperSQL.Query(string.Format(" select * from vProgramme where ScheduleType=-99 and TenantId={0}",CurrentTenantId)).Tables[0];

            repQianDao.DataSource = dt;
            repQianDao.DataBind();
        }

        public string GetDateTime(string st, string et)
        {
            DateTime _st = SafeConvert.ToDateTime(st);
            DateTime _et = SafeConvert.ToDateTime(et);

            TimeSpan ts = _et - _st;
            return ts.Hours.ToString();
        }

    }
}