﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Ins.Institution
{
    public partial class Modify : BasicPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(ValidatePermission("INSTITUTION_MANAGE") && ValidatePermission("INSTITUTION_MANAGE_LIST_EDIT")))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    int Id = (Convert.ToInt32(Request.Params["id"]));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.Institution bll = new XinYiOffice.BLL.Institution();
            XinYiOffice.Model.Institution model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.txtOrganizationName.Text = model.OrganizationName;
            this.txtIntroduction.Text = model.Introduction;
            this.txtLogo.Text = model.Logo;
            this.txtURL.Text = model.URL;

            this.HiddenField_txtCreateAccountId.Value= model.CreateAccountId.ToString();
            this.HiddenField_txtRefreshAccountId.Value = model.RefreshAccountId.ToString();
            this.HiddenField_txtCreateTime.Value = model.CreateTime.ToString();
            this.HiddenField_txtRefreshTime.Value = model.RefreshTime.ToString();

        }


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            int Id = int.Parse(this.lblId.Text);
            string OrganizationName = this.txtOrganizationName.Text;
            string Introduction = this.txtIntroduction.Text;
            string Logo = this.txtLogo.Text;
            string URL = this.txtURL.Text;
            int CreateAccountId = SafeConvert.ToInt(HiddenField_txtCreateAccountId.Value);
            int RefreshAccountId = SafeConvert.ToInt(HiddenField_txtRefreshAccountId.Value);
            DateTime CreateTime = SafeConvert.ToDateTime(HiddenField_txtCreateTime.Value);
            DateTime RefreshTime = SafeConvert.ToDateTime(HiddenField_txtRefreshTime.Value);


            XinYiOffice.Model.Institution model = new XinYiOffice.Model.Institution();
            model.Id = Id;
            model.OrganizationName = OrganizationName;
            model.Introduction = Introduction;
            model.Logo = Logo;
            model.URL = URL;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.Institution bll = new XinYiOffice.BLL.Institution();
            bll.Update(model);

            xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
