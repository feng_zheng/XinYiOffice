﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Ins.Institution
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(ValidatePermission("INSTITUTION_MANAGE") && ValidatePermission("INSTITUTION_MANAGE_LIST_ADD")))
            {
                base.NoPermissionPage();
            }
        }
 


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            string OrganizationName = this.txtOrganizationName.Text;
            string Introduction = this.txtIntroduction.Text;
            string Logo = WebUpLoad.PostActionUpLoad(FileUpload1.ClientID, "Institution");

            string URL = this.txtURL.Text;

            int CreateAccountId = CurrentAccountId;
            int RefreshAccountId = CurrentAccountId;
            DateTime CreateTime = DateTime.Now;
            DateTime RefreshTime = DateTime.Now;

            XinYiOffice.Model.Institution model = new XinYiOffice.Model.Institution();
            model.OrganizationName = OrganizationName;
            model.Introduction = Introduction;
            model.Logo = Logo;
            model.URL = URL;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.Institution bll = new XinYiOffice.BLL.Institution();
            bll.Add(model);
            xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
