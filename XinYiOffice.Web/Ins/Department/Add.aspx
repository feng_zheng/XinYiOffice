﻿<%@ Page Language="C#" MasterPageFile="~/BasicContent.Master"  AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="XinYiOffice.Web.Ins.Department.Add" Title="增加页" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script src="/js/ui/jquery.ui.core.js"></script>
	<script src="/js/ui/jquery.ui.widget.js"></script>
	<script src="/js/ui/jquery.ui.position.js"></script>
	<script src="/js/ui/jquery.ui.autocomplete.js"></script>
	<link rel="stylesheet" href="/js/themes/base/jquery.ui.all.css">
    <link rel="stylesheet" href="/js/themes/base/jquery.ui.autocomplete.css">


<script>
    $(function () {

        var url_getwork = "/Call/Ajax.aspx?action=getofficeworkeraccount&sj=" + Math.random();
        var autoPMAccountId = $("#<%=txtTorchbearerAccountId.ClientID%>");
        var auto_hid_pm_account = $("#<%=HiddenField_TorchbearerAccountId.ClientID%>");

        autoPMAccountId.autocomplete({
            source: url_getwork,
            select: function (event, ui) {
                //alert(ui.item.value);
                autoPMAccountId.val(ui.item.label);
                auto_hid_pm_account.val(ui.item.value);
                return false;
            },
            focus: function (event, ui) {
                autoPMAccountId.val(ui.item.label);
                return false;
            }
        });

    });
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<form  id="f2" name="f2" runat="server">
 <div class="setup_box" style="margin-bottom: 0">
 <div class="h_title"><h4>部门信息</h4></div>
                
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		部门名称
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtDepartmentName" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		部门介绍
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtDepartmentCon" runat="server" Width="446px" Height="111px" 
            TextMode="MultiLine"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		所属机构
	：</td>
	<td height="25" width="*" align="left">
		<asp:DropDownList ID="DropDownList_InstitutionId" runat="server">
        </asp:DropDownList>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		负责人
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtTorchbearerAccountId" runat="server" Width="200px"></asp:TextBox>
	    <asp:HiddenField ID="HiddenField_TorchbearerAccountId" runat="server" />
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		电话号码
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtPhoneCode" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		电话号码
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtPhoneCode2" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		传真
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtFax" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		邮编
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtZipCode" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	<tr>
	<td height="25" width="30%" align="right" class="table_left">
		地址
	：</td>
	<td height="25" width="*" align="left">
		<asp:TextBox id="txtAddress" runat="server" Width="200px"></asp:TextBox>
	</td></tr>
	</table>
        <div class="clear btn_box">
        <asp:LinkButton ID="LinkButton1" class="save" runat="server" OnClick="LinkButton1_Click">保存</asp:LinkButton>
        <a href="javascript:history.go(-1);" class="cancel">取消</a>
    </div></div>
    <br />
    </form></asp:Content>