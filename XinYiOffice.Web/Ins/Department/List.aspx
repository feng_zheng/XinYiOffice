﻿<%@ Page Title="部门" MasterPageFile="~/BasicContent.Master"  Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" EnableViewState="false" Inherits="XinYiOffice.Web.Ins.Department.List" %>
<%@ Register src="../../InitControl/SearchApp.ascx" tagname="SearchApp" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<form id="f2" name="f2" >
<div class="marketing_box">
<div class="btn_search_box">
        	<div class="clear cc" style=" padding-left:15px;">
            <div class="btn clear">
            <%if (ValidatePermission("CLIENTINFO_LIST"))
              { %>
            <a href="javascript:;" id="search_a"  class="sj">条件查询</a>
            <%} %>
            <%if (ValidatePermission("CLIENTINFO_LIST_ADD"))
              { %>
            <a href="add.aspx">添加</a>
            <%} %>
            <%if (ValidatePermission("CLIENTINFO_LIST_EDIT"))
              { %>
            <a href="javascript:;" class="edit">修改</a>
            <%} %>
             <%if (ValidatePermission("CLIENTINFO_LIST_DEL"))
              { %>
            <a href="javascript:;" class="tdel">删除</a>
            <%} %>

            </div>
                <div class="del clear"></div>
            </div>
          
            <div class="show_search_box" style="display:none;">
        		<div class="tjss clear">
                <uc1:searchapp ID="SearchApp1" runat="server" TableName="vClientInfo"/>

                </div>
                <div class="clear btn_box btn_box_p">
<a href="javascript:;" class="save btn_search">查询</a>
<a href="javascript:;" class="cancel res_search">取消</a>
                </div>
        	</div>
    
        </div>
        
   
   <div class="marketing_table">
        <table cellpadding="0" cellspacing="0" id="_table">

<asp:Repeater ID="repDepartment" runat="server" >
                <HeaderTemplate>
                <tr><th class="center"><input type="checkbox" id="cb_all"></th>
                <th>编号</th>
                <th>部门名称</th>
                <th>部门负责人</th>
                <th>所属机构</th>
                <th>创建时间</th></tr>
                </HeaderTemplate>
                
                <ItemTemplate>
                <tr><td class="center"><input type="checkbox" name="chk_list" va="<%#DataBinder.Eval(Container.DataItem, "Id")%>"></td>
                 <td><%#DataBinder.Eval(Container.DataItem, "Id")%></td>
                <td><a href="Show.aspx?Id=<%#DataBinder.Eval(Container.DataItem, "Id")%>"><%#DataBinder.Eval(Container.DataItem, "DepartmentName")%></a></td>

                <td><%#DataBinder.Eval(Container.DataItem, "TorchbearerAccountName")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "InstitutionName")%></td>
                <td><%#DataBinder.Eval(Container.DataItem, "CreateTime")%></td>

                </tr>    
                </ItemTemplate>
                </asp:Repeater>

                

            </table>
        </div>
        

</div>
</form>
<script>
    $('#_table td').hover(function () {
        $(this).parent().find('td').css('background', '#f0f8fc')
    }, function () {
        $(this).parent().find('td').css('background', 'none')
    });
</script>
</asp:Content>
