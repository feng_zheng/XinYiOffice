﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Common;
using XinYiOffice.Common.Web;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Ins.Department
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(ValidatePermission("DEPARTMENT_MANAGE") && ValidatePermission("DEPARTMENT_MANAGE_LIST_ADD")))
            {
                base.NoPermissionPage();
            }

            if(!IsPostBack)
            {
                InitData();
            }
        }

        public void InitData()
        {
            DropDownList_InstitutionId.DataSource= new BLL.Institution().GetList(string.Format(" TenantId={0} ",CurrentTenantId));
            DropDownList_InstitutionId.DataValueField = "Id";
            DropDownList_InstitutionId.DataTextField = "OrganizationName";
            DropDownList_InstitutionId.DataBind();

            DropDownList_InstitutionId.Items.Insert(0, new ListItem("请选择所属机构","0"));
        }


        public void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("list.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            string DepartmentName = this.txtDepartmentName.Text;
            string DepartmentCon = this.txtDepartmentCon.Text;
            int InstitutionId = SafeConvert.ToInt(DropDownList_InstitutionId.SelectedValue);
            int TorchbearerAccountId = SafeConvert.ToInt(HiddenField_TorchbearerAccountId.Value);

            string PhoneCode = this.txtPhoneCode.Text;
            string PhoneCode2 = this.txtPhoneCode2.Text;
            string Fax = this.txtFax.Text;
            string ZipCode = this.txtZipCode.Text;
            string Address = this.txtAddress.Text;
        
            int CreateAccountId = CurrentAccountId;
            int RefreshAccountId = CreateAccountId;
            DateTime CreateTime = DateTime.Now;
            DateTime RefreshTime = DateTime.Now;

            XinYiOffice.Model.Department model = new XinYiOffice.Model.Department();
            model.DepartmentName = DepartmentName;
            model.DepartmentCon = DepartmentCon;
            model.InstitutionId = InstitutionId;
            model.TorchbearerAccountId = TorchbearerAccountId;
            model.PhoneCode = PhoneCode;
            model.PhoneCode2 = PhoneCode2;
            model.Fax = Fax;
            model.ZipCode = ZipCode;
            model.Address = Address;
            model.CreateAccountId = CreateAccountId;
            model.RefreshAccountId = RefreshAccountId;
            model.CreateTime = CreateTime;
            model.RefreshTime = RefreshTime;
            model.TenantId = CurrentTenantId;

            XinYiOffice.BLL.Department bll = new XinYiOffice.BLL.Department();
            bll.Add(model);
            xytools.web_alert("保存成功！", "list.aspx");
        }
    }
}
