﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using XinYiOffice.Web.UI;

namespace XinYiOffice.Web.Ins.Department
{
    public partial class Show : BasicPage
    {
        public string strid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(ValidatePermission("DEPARTMENT_MANAGE") && ValidatePermission("DEPARTMENT_MANAGE_LIST_VIEW")))
            {
                base.NoPermissionPage();
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["id"] != null && Request.Params["id"].Trim() != "")
                {
                    strid = Request.Params["id"];
                    int Id = (Convert.ToInt32(strid));
                    ShowInfo(Id);
                }
            }
        }

        private void ShowInfo(int Id)
        {
            XinYiOffice.BLL.Department bll = new XinYiOffice.BLL.Department();
            XinYiOffice.Model.Department model = bll.GetModel(Id);
            this.lblId.Text = model.Id.ToString();
            this.lblDepartmentName.Text = model.DepartmentName;
            this.lblDepartmentCon.Text = model.DepartmentCon;
            this.lblInstitutionId.Text = model.InstitutionId.ToString();
            this.lblTorchbearerAccountId.Text = model.TorchbearerAccountId.ToString();
            this.lblPhoneCode.Text = model.PhoneCode;
            this.lblPhoneCode2.Text = model.PhoneCode2;
            this.lblFax.Text = model.Fax;
            this.lblZipCode.Text = model.ZipCode;
            this.lblAddress.Text = model.Address;
            this.lblCreateAccountId.Text = model.CreateAccountId.ToString();
            this.lblRefreshAccountId.Text = model.RefreshAccountId.ToString();
            this.lblCreateTime.Text = model.CreateTime.ToString();
            this.lblRefreshTime.Text = model.RefreshTime.ToString();

        }


    }
}
