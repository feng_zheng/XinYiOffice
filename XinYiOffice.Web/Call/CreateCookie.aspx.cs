﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Basic;
using XinYiOffice.Common;

namespace XinYiOffice.Web.Call
{
    public partial class CreateCookie : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int cookieday = 1;
            string username = Common.DESEncrypt.Decrypt(xytools.url_get("u"));
            string pas = Common.DESEncrypt.Decrypt(xytools.url_get("pass"));
            string miTenantId = xytools.url_get("t");
            string ym = Common.DESEncrypt.Decrypt(xytools.url_get("ym"));

            DateTime now = SafeConvert.ToDateTime(DateTime.Now.ToString("yyyy-MM-DD hh:mm:ss"));

            DateTime dt = SafeConvert.ToDateTime(ym);
            if ((dt.Year == now.Year) &&
                (dt.Month == now.Month) &&
                (dt.Day == now.Day) &&
                (dt.Hour == now.Hour)&&
                (dt.Minute==dt.Minute)
                )
            {
                bool state = AccountsServer.LoginValidation(username, pas, miTenantId, cookieday);
                if (state)
                {
                    if (AccountsServer.State == 1)
                    {
                        xytools.href_url("/index.aspx");
                    }
                    else
                    {
                        xytools.web_alert("对不起,用户密码错误!", @"http://www.xinyioffice.com/Cloud/Login.aspx");
                    }
                }
                else
                {
                    xytools.web_alert("对不起,用户密码错误!", @"http://www.xinyioffice.com/Cloud/Login.aspx");
                }

            }
            else
            {
                xytools.web_alert("请求失效",@"http://www.xinyioffice.com/Cloud/Login.aspx");
            }

          

        }
    }
}