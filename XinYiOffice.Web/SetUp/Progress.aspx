﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Progress.aspx.cs" Inherits="XinYiOffice.Web.SetUp.Progress" %>

<script language="javascript" src="/js/jquery-1.8.2.js"></script>
<script>

    function ThisProgress(currnt, itemcount, msg) {

        var fs = (currnt / itemcount) * 100;
        if (fs < 50) {
            $("#spanFs").css("color", "#000");

        }
        else {
            $("#spanFs").css("color", "#5A9CBF");
        }

        $("#progress_ing").css("width", fs + "%");
        $("#progress_ing").css("background-color", "#5A9CBF");

        $("#spanFs").text(parseInt(fs) + "%");
        $("#spanCurrnt").text(currnt);
        $("#spanItemCount").text(itemcount);
        if (fs == 100) {
            $("#divSucceed").show();
        }
        $("#span_info").css("color", "#5A9CBF");
        $("#span_info").text(msg);

    }

    function SetMsg(msg) {
        $("#span_info").css("color", "red");
        $("#span_info").text(msg);
    }
</script>

<style>
    body{ background-color:#fff; width:auto; height:auto;}
div{ margin:0px; padding:0px; font-size:12px; line-height:150%;}
#progress{ background-color#efefef; border: 1px solid #A0D1F0; height:12px;}
#spanItemCount{}
#progress_ing{ height:12px;}
#divSucceed{ display:none;}
</style>

<body>
<div id="build_ui">
初始化:<br/>

<div id="progress">
  <div id="progress_ing"></div>
</div>
<span id="span_info"></span><br/>
步骤 <span id="spanCurrnt"></span>/<span id="spanItemCount"></span> 已完成 <span id="spanFs"></span>
</div>
<div id="divSucceed">已全部完成,可以关闭此窗口</div>
<iframe width="0" height="0" scrolling="no" style=" display:none;" id="ifr" name="ifr" src="Action.aspx?action=init&TenantId=<%=TenantId%>"></iframe>
</body>
