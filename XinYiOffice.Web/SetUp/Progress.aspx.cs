﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XinYiOffice.Common;

namespace XinYiOffice.Web.SetUp
{
    public partial class Progress : System.Web.UI.Page
    {
        public int TenantId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            TenantId = SafeConvert.ToInt(xytools.url_get("TenantId"));
        }
    }
}