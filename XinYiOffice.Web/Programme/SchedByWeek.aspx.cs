﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using XinYiOffice.Web.UI;
using XinYiOffice.Common;
using System.Web.UI.HtmlControls;
using System.Text;

namespace XinYiOffice.Web.Programme
{
    public partial class SchedByWeek : BasicPage
    {
        private static DateTime date = new DateTime();
        private static DataTable dtProgrammeAll = new DataTable();
        protected int cutAccountId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!ValidatePermission("PROGRAMME_MANAGE"))
                {
                    base.NoPermissionPage();
                }

                date = DateTime.Now;
                cutAccountId =CurrentAccountId;
                DisplayDate(date);
            }

            
        }

        public int day(string date)
        {
            return Int32.Parse(DateTime.Parse(date).Day.ToString());
        }

        public string ssubject(int i)
        {

            DateTime Date = new DateTime();
            Date = DateTime.Parse(sDate(i));
            StringBuilder sb = new StringBuilder();

            DataRow[] dr = dtProgrammeAll.Select(string.Format("StartTime<='{0}' and EndTime>='{0}' and AccountId={1}", Date, cutAccountId));
            if (dr.Length > 0)
            {
                foreach (DataRow _dr in dr)
                {
                    sb.AppendFormat("<p>{0}<span></span></p>", SafeConvert.ToString(_dr["Title"]));
                }
            }

            return sb.ToString();
        }

        public void GetProgrammeAll()
        {
            
            string mysql = " SELECT * FROM vProgrammeMembers WHERE (DATEPART(month, StartTime) >= {0}) AND (DATEPART(year, StartTime) >= {1})";
            mysql = string.Format(mysql, date.Month, date.Year);
            dtProgrammeAll = Common.DbHelperSQL.Query(mysql).Tables[0];
        }


        public string sDate(int i)
        {
            int j = num(date);
            DateTime Date = new DateTime();
            Date = date;
            System.TimeSpan duration1 = new System.TimeSpan(j, 0, 0, 0);
            Date = Date.Subtract(duration1);
            System.TimeSpan duration2 = new System.TimeSpan(i, 0, 0, 0);
            Date = Date.Add(duration2);
            return Date.ToShortDateString();
        }

        public int num(DateTime date)
        {
            int j = 0;
            switch (date.DayOfWeek.ToString())
            {
                case "Sunday":
                    j = 0;
                    break;
                case "Monday":
                    j = 1;
                    break;
                case "Tuesday":
                    j = 2;
                    break;
                case "Wednesday":
                    j = 3;
                    break;
                case "Thursday":
                    j = 4;
                    break;
                case "Friday":
                    j = 5;
                    break;
                case "Saturday":
                    j = 6;
                    break;
            }
            return j;
        }

        protected void cmdLastDay_Click(object sender, EventArgs e)
        {
            System.TimeSpan duration = new System.TimeSpan(7, 0, 0, 0);
            date = date.Subtract(duration);
            DisplayDate(date);
        }


        protected void cmdNextDay_Click(object sender, EventArgs e)
        {
            System.TimeSpan duration = new System.TimeSpan(7, 0, 0, 0);
            date = date.Add(duration);
            DisplayDate(date);
        }

        void DisplayDate(DateTime date)
        {
            if (cutAccountId == 0)
            {
                cutAccountId = CurrentAccountId;
            }

            GetProgrammeAll();

            string Day = "";
            switch (date.DayOfWeek.ToString())
            {
                case "Sunday":
                    Day = "星期日";
                    break;
                case "Monday":
                    Day = "星期一";
                    break;
                case "Tuesday":
                    Day = "星期二";
                    break;
                case "Wednesday":
                    Day = "星期三";
                    break;
                case "Thursday":
                    Day = "星期四";
                    break;
                case "Friday":
                    Day = "星期五";
                    break;
                case "Saturday":
                    Day = "星期六";
                    break;
            }

            lblHeader.InnerText = date.Year + "年" + date.Month + "月" + date.Day + "日" + Day;
            //txtYear.Text = date.Year.ToString();
            //dropMonth.SelectedIndex = date.Month - 1;
            //dropDay.SelectedIndex = date.Day - 1;

            
        }


    }
}